// WL_DRAW.C 
#include "wl_def.h"
#pragma hdrstop

#include "wl_cloudsky.h"
#include "wl_atmos.h"
#include "wl_shade.h"
#include "fixedptc.h"

byte *DefaultPatchSource(void);

/*
=============================================================================

                               LOCAL CONSTANTS

=============================================================================
*/

int doorlock_to_doorpage[dr_max][dr_face_max] =
{
    // dr_normal
    { 0, 1 },
    // dr_lock1
    { 3, 1 },
    // dr_lock2
    { 3, 1 },
    // dr_lock3
    { 3, 1 },
    // dr_lock4
    { 3, 1 },
    // dr_elevator
    { 2, 1 },
};

#ifdef MIRWALL
#define TRUCKTILE1 61 
#define TRUCKTILE2 62 
#define TRUCKTILE3 63 
#endif

/*
=============================================================================

                              GLOBAL VARIABLES

=============================================================================
*/

VBuf_t vbuf = { NULL };

int32_t    wolfticrate = WOLFTICRATE_DEFAULT;
int32_t    lasttimecount;
int32_t    frameon;
boolean fpscounter;
#ifndef REMDEBUG
uint32_t   lastfpsticks;
boolean    lastfpsinit = 0;
#endif


int fps_frames=0, fps_time=0, fps=0;

int *wallheight;
int min_wallheight;

//
// math tables
//
short *pixelangle;
int32_t finetangent[FINEANGLES/4];
fixed sintable[ANGLES+ANGLES/4];
fixed *costable = sintable+(ANGLES/4);

//
// refresh variables
//
fixed   viewx,viewy;                    // the focal point
short   viewangle;
fixed   viewsin,viewcos;

void    TransformActor (objtype *ob);
void    BuildTables (void);
void    ClearScreen (void);
int     CalcRotate (objtype *ob);
void    DrawScaleds (void);
void    CalcTics (void);
void    ThreeDRefresh (void);



//
// wall optimization variables
//

//
// ray tracing variables
//
short    focaltx,focalty,viewtx,viewty;
longword xpartialup,xpartialdown,ypartialup,ypartialdown;

short   midangle,angle;

word    tilehit;
int     pixx;

short   xtile,ytile;
short   xtilestep,ytilestep;
int32_t    xintercept,yintercept;
word    xstep,ystep;
word    xspot,yspot;
short   xlighttile,ylighttile;
int     texdelta;

word horizwall[MAXWALLTILES],vertwall[MAXWALLTILES];

WR_LightInfo_t *curLightInfo = NULL;


/*
============================================================================

                           3 - D  DEFINITIONS

============================================================================
*/

/*
========================
=
= TransformActor
=
= Takes paramaters:
=   gx,gy               : globalx/globaly of point
=
= globals:
=   viewx,viewy         : point of view
=   viewcos,viewsin     : sin/cos of viewangle
=   scale               : conversion from global value to screen value
=
= sets:
=   screenx,transx,transy,screenheight: projected edge location and size
=
========================
*/


//
// transform actor
//
void TransformActor (objtype *ob)
{
    fixed gx,gy,gxt,gyt,nx,ny;
    fixed actorsize;

//
// translate point to view centered coordinates
//
    gx = ob->x-viewx;
    gy = ob->y-viewy;

//
// calculate newx
//
    gxt = FixedMul(gx,viewcos);
    gyt = FixedMul(gy,viewsin);
    actorsize = ob->actorsize;
    if (actorsize == 0)
    {
        actorsize = ACTORSIZE;
    }
    nx = gxt-gyt-actorsize;         // fudge the shape forward a bit, because
                                    // the midpoint could put parts of the shape
                                    // into an adjacent wall

//
// calculate newy
//
    gxt = FixedMul(gx,viewsin);
    gyt = FixedMul(gy,viewcos);
    ny = gyt+gxt;

//
// calculate perspective ratio
//
    ob->transx = nx;
    ob->transy = ny;

    if (nx<MINDIST)                 // too close, don't overflow the divide
    {
        ob->viewheight = 0;
        return;
    }

    ob->viewx = (word)(centerx + ny*scale/nx);

//
// calculate height (heightnumerator/(nx>>8))
//
    ob->viewheight = (word)(heightnumerator/(nx>>8));
}

//==========================================================================

/*
========================
=
= TransformTile
=
= Takes paramaters:
=   tx,ty               : tile the object is centered in
=
= globals:
=   viewx,viewy         : point of view
=   viewcos,viewsin     : sin/cos of viewangle
=   scale               : conversion from global value to screen value
=
= sets:
=   screenx,transx,transy,screenheight: projected edge location and size
=
= Returns true if the tile is withing getting distance
=
========================
*/

boolean TransformTile (int tx, int ty, short *dispx, short *dispheight)
{
    fixed gx,gy,gxt,gyt,nx,ny;

//
// translate point to view centered coordinates
//
    gx = ((int32_t)tx<<TILESHIFT)+0x8000-viewx;
    gy = ((int32_t)ty<<TILESHIFT)+0x8000-viewy;

//
// calculate newx
//
    gxt = FixedMul(gx,viewcos);
    gyt = FixedMul(gy,viewsin);
    nx = gxt-gyt-STATOBJSIZE;

//
// calculate newy
//
    gxt = FixedMul(gx,viewsin);
    gyt = FixedMul(gy,viewcos);
    ny = gyt+gxt;


//
// calculate height / perspective ratio
//
    if (nx<MINDIST)                 // too close, don't overflow the divide
        *dispheight = 0;
    else
    {
        *dispx = (short)(centerx + ny*scale/nx);
        *dispheight = (short)(heightnumerator/(nx>>8));
    }

//
// see if it should be grabbed
//
    if (nx<TILEGLOBAL && ny>-TILEGLOBAL/2 && ny<TILEGLOBAL/2)
        return true;
    else
        return false;
}

//==========================================================================

/*
====================
=
= CalcHeight
=
= Calculates the height of xintercept,yintercept from viewx,viewy
=
====================
*/

int CalcHeight()
{
    fixed z = FixedMul(xintercept - viewx, viewcos)
        - FixedMul(yintercept - viewy, viewsin);
    if(z < MINDIST) z = MINDIST;
    int height = heightnumerator / (z >> 8);
    if(height < min_wallheight) min_wallheight = height;
    return height;
}

//==========================================================================


/*
===================
=
= OpenDoorMask
=
===================
*/

static unsigned int OpenDoorMask(WR_LightInfo_LightDoor_t *lightDoorFirst,
    int lightDoorCount)
{
    int i;
    WR_LightInfo_LightDoor_t *lightDoor;
    unsigned int openDoorMask = 0;
    doorobj_t *door;
    int doorNum;
    
    for (i = 0; i < lightDoorCount; i++)
    {
        lightDoor = &lightDoorFirst[i];
        doorNum = lightDoor->doorNum;

        if (doorNum >= 0 && doorNum < doorcount)
        {
            door = &doorobjlist[doorNum];
            if (door->action != dr_closed)
            {
                openDoorMask |= (1 << i);
            }
        }
    }

    return openDoorMask;
}


/*
===================
=
= SearchLightFacets
=
===================
*/

static WR_LightInfo_LightFacet_t *SearchLightFacets(int x, int y, int qn)
{
    int lightFacetIndex;
    WR_LightInfo_LightFacet_t *lightFacet = NULL;
    unsigned int openDoorMask;
    WR_LightInfo_Tile_t *lightTile;

    if (curLightInfo == NULL)
    {
        return NULL;
    }

    lightTile = &curLightInfo->tiles[x][y];

    lightFacetIndex = lightTile->lightFacetLookup[qn];
    if (lightFacetIndex != 0)
    {
        lightFacetIndex--;

        lightFacet = &curLightInfo->lightFacets[lightFacetIndex];

        #if 0
        openDoorMask = OpenDoorMask(lightFacet->lightDoorFirst,
            lightFacet->lightDoorCount);
        lightFacetIndex += openDoorMask;
        #else
        openDoorMask = 0;
        #endif

        assert(lightFacetIndex < curLightInfo->numLightFacets);
        lightFacet = &curLightInfo->lightFacets[lightFacetIndex];
        assert(lightFacet->x == x && lightFacet->y == y && 
            lightFacet->qn == qn && lightFacet->lightDoorMask == openDoorMask);
        return lightFacet;
    }

    return NULL;
}


/*
===================
=
= TileIsWall
=
===================
*/

static int TileIsWall(int x, int y)
{
    return !(x >= 0 && x < 64 && y >= 0 && y < 64) ||
        (tilemap[x][y] != 0 && (tilemap[x][y] & 0x80) == 0 && 
        tilemap[x][y] != PUSHMOVETILE);
}


/*
===================
=
= SpriteLitLevel
=
===================
*/

lwlib_TPoint3f SpriteLitLevel(fixed x, fixed y)
{
    struct TileData_s
    {
        lwlib_TPoint3i pos;
        int wall;
        lwlib_TPoint3f spriteLitLevel;
    } tileData[4];
    int i;
    int qn;
    int relx, rely;
    int tilex, tiley;
    double u, v;
    lwlib_TPoint3f quad[4];
    lwlib_TPoint3f spriteLitLevel;
    lwlib_TPoint3f uv;
    int foundConfig = 0;
    WR_LightInfo_LightFacet_t *lightFacet;
    lwlib_TPoint3i pos;
    int count;

    relx = (x % HALFTILE);
    tilex = (x >> (TILESHIFT - 1));
    if (relx < (HALFTILE / 2))
    {
        tilex--;
        u = 0.5 + (double)relx / HALFTILE;
    }
    else
    {
        u = (double)(relx - (HALFTILE / 2)) / HALFTILE;
    }

    rely = (y % HALFTILE);
    tiley = (y >> (TILESHIFT - 1));
    if (rely < (HALFTILE / 2))
    {
        tiley--;
        v = 0.5 + (double)rely / HALFTILE;
    }
    else
    {
        v = (double)(rely - (HALFTILE / 2)) / HALFTILE;
    }

    tileData[0].pos = lwlib_vec3i(tilex, tiley, 0);
    tileData[1].pos = lwlib_vec3i(tilex + 1, tiley, 0);
    tileData[2].pos = lwlib_vec3i(tilex + 1, tiley + 1, 0);
    tileData[3].pos = lwlib_vec3i(tilex, tiley + 1, 0);

    for (i = 0; i < 4; i++)
    {
        pos = tileData[i].pos;
        tileData[i].wall = TileIsWall(X(pos) / 2, Y(pos) / 2);
        if (!tileData[i].wall)
        {
            spriteLitLevel = lwlib_vec3f_zero();

            count = 0;
            for (qn = 0; qn < WR_LEVEL_QN_TOT; qn++)
            {
                lightFacet = SearchLightFacets(X(pos),
                    128 - 1 - Y(pos), qn);
                if (lightFacet != NULL)
                {
                    spriteLitLevel = lwlib_vec3f_add(spriteLitLevel, 
                        lightFacet->spriteLitLevel);
                    count++;
                }
            }

            tileData[i].spriteLitLevel = lwlib_vec3f_scale(
                spriteLitLevel, 1.0 / count);
        }
    }

    // ..
    // ..
    if (!tileData[0].wall && !tileData[1].wall &&
        !tileData[2].wall && !tileData[3].wall)
    {
        for (i = 0; i < 4; i++)
        {
            quad[i] = tileData[i].spriteLitLevel;
        }
        spriteLitLevel = lwlib_vec3f_quad_lerp(quad, u, v);
    }
    else
    {
        uv = lwlib_vec3f(u, v, 0.0);
        foundConfig = 0;

        for (i = 0; i < 4 && !foundConfig; i++)
        {
            foundConfig = 1;

            // ##
            // ..
            if (!tileData[0].wall && !tileData[1].wall &&
                tileData[2].wall && tileData[3].wall)
            {
                spriteLitLevel = 
                    lwlib_edge3f_lerp(
                        lwlib_edge3f(tileData[0].spriteLitLevel, 
                            tileData[1].spriteLitLevel),
                        u
                        );
            }
            // ##
            // #.
            else if (tileData[0].wall && !tileData[1].wall &&
                tileData[2].wall && tileData[3].wall)
            {
                spriteLitLevel = tileData[1].spriteLitLevel;
            }
            // #.
            // ..
            else if (!tileData[0].wall && !tileData[1].wall &&
                !tileData[2].wall && tileData[3].wall)
            {
                if (u >= 0.5 && v <= 0.5)
                {
                    spriteLitLevel = tileData[1].spriteLitLevel;
                }
                else
                {
                    if (u <= 0.5)
                    {
                        spriteLitLevel = 
                            lwlib_edge3f_lerp(
                                lwlib_edge3f(tileData[0].spriteLitLevel,
                                    tileData[1].spriteLitLevel),
                                u / 0.5
                                );
                    }
                    else if (v >= 0.5)
                    {
                        spriteLitLevel = 
                            lwlib_edge3f_lerp(
                                lwlib_edge3f(tileData[1].spriteLitLevel,
                                    tileData[2].spriteLitLevel),
                                (v - 0.5) / 0.5
                                );
                    }
                }
            }
            else
            {
                foundConfig = 0;

                uv = lwlib_vec3f_rot90_anchor(uv, 
                    lwlib_vec3f(0.5, 0.5, 0.0));
                u = X(uv);
                v = Y(uv);

                lwlib_RotateArrayK(tileData, 4, -1, struct TileData_s);
            }
        }

        if (!foundConfig)
        {
            // ??
            // ??
            spriteLitLevel = tileData[0].spriteLitLevel;
        }
    }

    return spriteLitLevel;
}


/*
===================
=
= GetLightmap
=
=     face
=     |-------|
=  ^  |       |
=  |  |       |
=  v  |-------|
=
=     u ->
=
= u - Texture coordinate going along +x of face. It ranges
= [0, TEXTURESIZE - 1].
=
= v - Texture coordinate going along +y of face. It ranges
= [0, TEXTURESIZE - 1].
=
= tilex - Tile coordinate going along +x of world.
= tiley - Tile coordinate going along +y of world.
=
===================
*/

uint32_t *GetLightmap(LMFace_t face, int u, int v, int tilex, int tiley,
    bool *isDefault)
{
    int lightTileX, lightTileY;
    WR_Level_QuadNumber_t qn;
    WR_LightInfo_LightFacet_t *lightFacet;
    int lm;
    int pix;
    int doortile;
    int doornum;
    doorobj_t *doorobj;
    int vert;

    if (isDefault != NULL)
    {
        *isDefault = false;
    }

    if (curLightInfo == NULL)
    {
        if (isDefault != NULL)
        {
            *isDefault = true;
        }
        return DefaultLightmap() + (u << TEXTURESHIFT) + v;
    }

    if (face == LMFACE_FLOOR || face == LMFACE_CEILING)
    {
        lightTileX = tilex * 2;
        lightTileY = tiley * 2;
        qn = (face == LMFACE_FLOOR ? 
            WR_LEVEL_QN_FLOOR : WR_LEVEL_QN_CEILING);

        if (u >= TEXTURESIZE / 2)
        {
            lightTileX++;
            u -= TEXTURESIZE / 2;
        }

        if (v >= TEXTURESIZE / 2)
        {
            lightTileY++;
            v -= TEXTURESIZE / 2;
        }

        if (face == LMFACE_FLOOR)
        {
            // flip floor texture
            v = (TEXTURESIZE / 2) - 1 - v;
        }
    }
    else if (face == LMFACE_WALL_HORIZ)
    {
        tilex = xtile;
        tiley = 64 - 1 - (ytile - ytilestep);

        if (ytilestep == -1)
        {
            lightTileX = tilex * 2;
            lightTileY = tiley * 2 + 1;

            if (u >= TEXTURESIZE / 2)
            {
                lightTileX++;
                u -= TEXTURESIZE / 2;
            }
            qn = WR_LEVEL_QN_NORTHWALL;
        }
        else
        {
            lightTileX = tilex * 2 + 1;
            lightTileY = tiley * 2;

            if (u >= TEXTURESIZE / 2)
            {
                lightTileX--;
                u -= TEXTURESIZE / 2;
            }
            qn = WR_LEVEL_QN_SOUTHWALL;
        }
    }
    else if (face == LMFACE_WALL_VERT)
    {
        tilex = xtile - xtilestep;
        tiley = 64 - 1 - ytile;

        if (xtilestep == -1)
        {
            lightTileX = tilex * 2;
            lightTileY = tiley * 2;

            if (u >= TEXTURESIZE / 2)
            {
                lightTileY++;
                u -= TEXTURESIZE / 2;
            }
            qn = WR_LEVEL_QN_WESTWALL;
        }
        else
        {
            lightTileX = tilex * 2 + 1;
            lightTileY = tiley * 2 + 1;

            if (u >= TEXTURESIZE / 2)
            {
                lightTileY--;
                u -= TEXTURESIZE / 2;
            }
            qn = WR_LEVEL_QN_EASTWALL;
        }
    }
    else if (face == LMFACE_DOOR_HORIZ)
    {
        tilex = xtile;
        tiley = 64 - 1 - ytile;

        if (ytilestep == -1)
        {
            lightTileX = tilex * 2;
            lightTileY = tiley * 2;

            if (u >= TEXTURESIZE / 2)
            {
                lightTileX++;
                u -= TEXTURESIZE / 2;
            }
            qn = WR_LEVEL_QN_NORTHWALL;
        }
        else
        {
            lightTileX = tilex * 2 + 1;
            lightTileY = tiley * 2 + 1;

            if (u >= TEXTURESIZE / 2)
            {
                lightTileX--;
                u -= TEXTURESIZE / 2;
            }
            qn = WR_LEVEL_QN_SOUTHWALL;
        }
    }
    else if (face == LMFACE_DOOR_VERT)
    {
        tilex = xtile;
        tiley = 64 - 1 - ytile;

        if (xtilestep == -1)
        {
            lightTileX = tilex * 2 + 1;
            lightTileY = tiley * 2;

            if (u >= TEXTURESIZE / 2)
            {
                lightTileY++;
                u -= TEXTURESIZE / 2;
            }
            qn = WR_LEVEL_QN_WESTWALL;
        }
        else
        {
            lightTileX = tilex * 2;
            lightTileY = tiley * 2 + 1;

            if (u >= TEXTURESIZE / 2)
            {
                lightTileY--;
                u -= TEXTURESIZE / 2;
            }
            qn = WR_LEVEL_QN_EASTWALL;
        }
    }
    else
    {
        assert(0);
    }

    lightFacet = SearchLightFacets(lightTileX, lightTileY, qn);
    if (lightFacet != NULL && lightFacet->nu != 0 && lightFacet->nv != 0)
    {
        lm = lightFacet->lm;
        return (uint32_t *)curLightInfo->lightmaps[lm].data + 
            lightFacet->off + (u * lightFacet->nv) + v;
    }

    if (isDefault != NULL)
    {
        *isDefault = true;
    }
    return DefaultLightmap() + (u << TEXTURESHIFT) + v;
}


/*
===================
=
= DefaultLightmap
=
===================
*/

uint32_t *DefaultLightmap(void)
{
    int x, y;
    static uint32_t lmdata[TEXTURESIZE * TEXTURESIZE];
    static bool init = false;
    uint32_t color;

    color = (param_debugmode ? RGBA(255,255,255,0) : RGBA(0,0,0,0));
        
    if (!init)
    {
        for (x = 0; x < TEXTURESIZE; x++)
        {
            for (y = 0; y < TEXTURESIZE; y++)
            {
                lmdata[y * TEXTURESIZE + x] = 
                    ((x % 16 < 8) ^ (y % 16 < 8)) ? 
                    color : RGBA(0,0,0,0);
            }
        }
        init = true;
    }

    return lmdata;
}


/*
===================
=
= ScalePost
=
===================
*/

uint32_t *lmsource;
int postx;

void ScalePost()
{
    int lighttilex, lighttiley;
    int ywcount, yoffs, yw, yd, yendoffs;
    uint32_t col, shadedCol;
    LT_Shade_t shade;
    uint8_t palCol;
    
    #ifdef HORIZON
    if (tilehit == 85)
	{
		return;
	}
    #endif

	int tsz = TEXTURESIZE;
	int tsz2 = TEXTURESIZE/2;

    lighttilex = LIGHTTILE_COORD(xintercept);
    lighttiley = LIGHTTILE_COORD(yintercept);

    ywcount = yd = wallheight[postx] >> 3;
    if(yd <= 0)
        yd = 100;

    yoffs = (viewheight / 2 - ywcount) * screen.pitch;
    if(yoffs < 0)
        yoffs = 0;
    yoffs += postx;

    yendoffs = viewheight / 2 + ywcount - 1;
    yw = tsz-1;

    while (yendoffs >= viewheight)
    {
        ywcount -= tsz2;
        while(ywcount <= 0)
        {
            ywcount += yd;
            yw--;
        }
        yendoffs--;
    }
    if(yw < 0)
        return;

    col = lmsource[yw];
    palCol = LT_ALPHA(col);
    shadedCol = LT_GetShade(lighttilex, lighttiley, col, palCol);

    yendoffs = yendoffs * screen.pitch + postx;
    if ( yw < 0 )
        return;
    while(yoffs <= yendoffs)
    {
        if (palCol != 0xff)
        {
            VBufWriteColor(vbuf, yendoffs, shadedCol);
        }

        ywcount -= tsz2;
        if(ywcount <= 0)
        {
            do
            {
                ywcount += yd;
                if ( --yw < 0 )
                    return;
            }
            while(ywcount <= 0);

            col = lmsource[yw];
            palCol = LT_ALPHA(col);
            shadedCol = LT_GetShade(lighttilex, lighttiley, col, palCol);
        }
        yendoffs -= screen.pitch;
    }
}

/*
====================
=
= ReportHit
=
====================
*/

void ReportHit(int vert)
{
    static int old[2];
    static char strs[2][64];
    static bool init = false;

    if (pixx == 320)
    {
        if (!init)
        {
            old[0] = xintercept;
            old[1] = yintercept;
            init = true;
        }

        if (xintercept != old[0] || yintercept != old[1])
        {
            old[0] = xintercept;
            old[1] = yintercept;
            fixedpt_str(xintercept, strs[0]);
            fixedpt_str(yintercept, strs[1]);
            fprintf(stderr, "%s (%20s, %20s) (%d, %d)\n", 
                vert ? "vert" : "horz", strs[0], strs[1], 
                xtilestep, ytilestep);
        }
    }
}


/*
====================
=
= HitHorizWall
=
====================
*/

void HitHorizWall (void)
{
    int texture;

    //ReportHit(0);

    texture = fixedpt_toint((xintercept + texdelta) << TEXTURESHIFT) &
        (TEXTURESIZE - 1);
    if (ytilestep == -1)
    {
        yintercept += TILEGLOBAL;
    }
    else
    {
        texture = (TEXTURESIZE - 1) - texture;
    }

    wallheight[pixx] = CalcHeight();
    postx = pixx;

    lmsource = GetLightmap(LMFACE_WALL_HORIZ, texture, 0, 0, 0, 0);

    ScalePost();
}


/*
====================
=
= HitVertWall
=
====================
*/

void HitVertWall (void)
{
    int texture;

    //ReportHit(1);

    texture = fixedpt_toint((yintercept + texdelta) << TEXTURESHIFT) &
        (TEXTURESIZE - 1);
    if (xtilestep == -1)
    {
        xintercept += TILEGLOBAL;
        texture = (TEXTURESIZE - 1) - texture;
    }

    wallheight[pixx] = CalcHeight();
    postx = pixx;

    lmsource = GetLightmap(LMFACE_WALL_VERT, texture, 0, 0, 0, 0);

    ScalePost();
}

//==========================================================================

/*
====================
=
= HitHorizDoor
=
====================
*/

void HitHorizDoor (void)
{
    int doornum;
    int texture;

    doornum = tilehit & 0x7f;
    texture = fixedpt_toint((xintercept - doorposition[doornum]) << 
        TEXTURESHIFT) & (TEXTURESIZE - 1);
    if (ytilestep == -1)
    {
    }
    else
    {
        texture = (TEXTURESIZE - 1) - texture;
    }

    wallheight[pixx] = CalcHeight();
    postx = pixx;

    lmsource = GetLightmap(LMFACE_DOOR_HORIZ, texture, 0, 0, 0, 0);

    ScalePost();
}


/*
====================
=
= HitVertDoor
=
====================
*/

void HitVertDoor (void)
{
    int doornum;
    int texture;

    doornum = tilehit & 0x7f;
    texture = fixedpt_toint((yintercept - doorposition[doornum]) <<
        TEXTURESHIFT) & (TEXTURESIZE - 1);
    if (xtilestep == -1)
    {
        texture = (TEXTURESIZE - 1) - texture;
    }

    wallheight[pixx] = CalcHeight();
    postx = pixx;

    lmsource = GetLightmap(LMFACE_DOOR_VERT, texture, 0, 0, 0, 0);

    ScalePost();
}

//==========================================================================

#define HitHorizBorder HitHorizWall
#define HitVertBorder HitVertWall

//==========================================================================

byte vgaCeiling[]=
{
#ifndef SPEAR
 0x1d,0x1d,0x1d,0x1d,0x1d,0x1d,0x1d,0x1d,0x1d,0xbf,
 0x4e,0x4e,0x4e,0x1d,0x8d,0x4e,0x1d,0x2d,0x1d,0x8d,
 0x1d,0x1d,0x1d,0x1d,0x1d,0x2d,0xdd,0x1d,0x1d,0x98,

 0x1d,0x9d,0x2d,0xdd,0xdd,0x9d,0x2d,0x4d,0x1d,0xdd,
 0x7d,0x1d,0x2d,0x2d,0xdd,0xd7,0x1d,0x1d,0x1d,0x2d,
 0x1d,0x1d,0x1d,0x1d,0xdd,0xdd,0x7d,0xdd,0xdd,0xdd
#else
 0x6f,0x4f,0x1d,0xde,0xdf,0x2e,0x7f,0x9e,0xae,0x7f,
 0x1d,0xde,0xdf,0xde,0xdf,0xde,0xe1,0xdc,0x2e,0x1d,0xdc
#endif
};

//==========================================================================

/*
=====================
=
= CalcRotate
=
=====================
*/

int CalcRotate (objtype *ob)
{
    int angle, viewangle;

    // this isn't exactly correct, as it should vary by a trig value,
    // but it is close enough with only eight rotations

    viewangle = player->angle + (centerx - ob->viewx)/8;

    if (ob->obclass == rocketobj || ob->obclass == hrocketobj)
        angle = (viewangle-180) - ob->angle;
    else
        angle = (viewangle-180) - dirangle[ob->dir];

    angle+=ANGLES/16;
    while (angle>=ANGLES)
        angle-=ANGLES;
    while (angle<0)
        angle+=ANGLES;

    if (ob->state->rotate == 2)             // 2 rotation pain frame
        return 0;               // pain with shooting frame bugfix

    return angle/(ANGLES/8);
}

void ScaleShape (int xcenter, int shapenum, unsigned height, 
    uint32_t flags, fixed heightoff, fixed obj_x, fixed obj_y,
    byte blend, t_compshape *shape)
{
    unsigned scale,pixheight;
    unsigned starty,endy;
    word *cmdptr;
    byte *cline;
    byte *line;
    VBuf_t vmem;
    int actx,i,upperedge;
    short newstart;
    int scrstarty,screndy,lpix,rpix,pixcnt,ycnt;
    unsigned j;
    byte col;
    uint32_t spriteColor;
    lwlib_TPoint3f spriteLitLevel;

#ifdef USE_SHADING
    if(flags & FL_FULLBRIGHT)
    {
        spriteLitLevel = lwlib_vec3f_ones();
    }
    else
    {
        spriteLitLevel = LT_GetShadedSpriteLitLevel(obj_x, obj_y);
    }
#endif

    int tsh = TEXTURESHIFT;

    if (shape == NULL)
    {
        if (shapenum == SPR_DEMO)
        {
            return;
        }
        shape = (t_compshape *) PM_GetSprite(shapenum);
    }

    scale=height>>3;            // low three bits are fractional
    if(!scale) return;          // too close or far away

    pixheight=scale*SPRITESCALEFACTOR;
    actx=xcenter-scale;
    upperedge=viewheight/2-scale;
    upperedge -= FP_TRUNC(FixedMul(heightoff, FP(scale)));

    cmdptr=(word *) shape->dataofs;

    for(i=shape->leftpix,pixcnt=i*pixheight,rpix=(pixcnt>>tsh)+actx;i<=shape->rightpix;i++,cmdptr++)
    {
        lpix=rpix;
        if(lpix>=viewwidth) break;
        pixcnt+=pixheight;
        rpix=(pixcnt>>tsh)+actx;
        if(lpix!=rpix && rpix>0)
        {
            if(lpix<0) lpix=0;
            if(rpix>viewwidth) rpix=viewwidth,i=shape->rightpix+1;
            cline=(byte *)shape + *cmdptr;
            while(lpix<rpix)
            {
                if(wallheight[lpix]<=(int)height)
                {
                    line=cline;
                    while((endy = READWORD(line)) != 0)
                    {
                        endy >>= 1;
                        newstart = READWORD(line);
                        starty = READWORD(line) >> 1;
                        j=starty;
                        ycnt=j*pixheight;
                        screndy=(ycnt>>tsh)+upperedge;
                        if(screndy<0)
                            vmem = VBufOffset(vbuf, lpix);
                        else
                            vmem = VBufOffset(vbuf, 
                                screndy * screen.pitch + lpix);
                        for(;j<endy;j++)
                        {
                            scrstarty=screndy;
                            ycnt+=pixheight;
                            screndy=(ycnt>>tsh)+upperedge;
                            if(scrstarty!=screndy && screndy>0)
                            {
                                col=((byte *)shape)[newstart+j];
                                spriteColor = LT_SpriteColor(col,
                                    spriteLitLevel);

                                if(scrstarty<0)
                                    scrstarty=0;
                                if(screndy>viewheight)
                                    screndy=viewheight,j=endy;

                                while(scrstarty<screndy)
                                {
                                    VBufWriteColor(vmem, 0, 
                                        spriteColor);
                                    vmem = VBufOffset(vmem, 
                                        screen.pitch);
                                    scrstarty++;
                                }
                            }
                        }
                    }
                }
                lpix++;
            }
        }
    }
}

void SimpleScaleShape (int xcenter, int shapenum, unsigned height,
    int upperedge_offset, bool shaded)
{
    t_compshape   *shape;
    unsigned scale,pixheight;
    unsigned starty,endy;
    word *cmdptr;
    byte *cline;
    byte *line;
    int actx,i,upperedge;
    short newstart;
    int scrstarty,screndy,lpix,rpix,pixcnt,ycnt;
    unsigned j;
    byte col;
    VBuf_t vmem;
    uint32_t spriteColor;
    lwlib_TPoint3f spriteLitLevel;

    shape = (t_compshape *) PM_GetSprite(shapenum);

    scale=height>>1;
    pixheight=scale*SPRITESCALEFACTOR;
    actx=xcenter-scale;
    upperedge=viewheight/2-scale + upperedge_offset;

    cmdptr=shape->dataofs;

    if (shaded)
    {
        spriteLitLevel = LT_GetShadedSpriteLitLevel(player->x, player->y);
    }
    else
    {
        spriteLitLevel = lwlib_vec3f_ones();
    }

    for(i=shape->leftpix,pixcnt=i*pixheight,rpix=(pixcnt>>6)+actx;i<=shape->rightpix;i++,cmdptr++)
    {
        lpix=rpix;
        if(lpix>=viewwidth) break;
        pixcnt+=pixheight;
        rpix=(pixcnt>>6)+actx;
        if(lpix!=rpix && rpix>0)
        {
            if(lpix<0) lpix=0;
            if(rpix>viewwidth) rpix=viewwidth,i=shape->rightpix+1;
            cline = (byte *)shape + *cmdptr;
            while(lpix<rpix)
            {
                line=cline;
                while((endy = READWORD(line)) != 0)
                {
                    endy >>= 1;
                    newstart = READWORD(line);
                    starty = READWORD(line) >> 1;
                    j=starty;
                    ycnt=j*pixheight;
                    screndy=(ycnt>>6)+upperedge;
                    if(screndy<0)
                        vmem = VBufOffset(vbuf, lpix);
                    else
                        vmem = VBufOffset(vbuf, screndy*screen.pitch+lpix);
                    for(;j<endy;j++)
                    {
                        scrstarty=screndy;
                        ycnt+=pixheight;
                        screndy=(ycnt>>6)+upperedge;
                        if(scrstarty!=screndy && screndy>0)
                        {
                            col=((byte *)shape)[newstart+j];
                            spriteColor = LT_SpriteColor(col,
                                spriteLitLevel);
                            if(scrstarty<0) scrstarty=0;
                            if(screndy>viewheight) screndy=viewheight,j=endy;

                            while(scrstarty<screndy)
                            {
                                VBufWriteColor(vmem, 0, spriteColor);
                                vmem = VBufOffset(vmem, screen.pitch);
                                scrstarty++;
                            }
                        }
                    }
                }
                lpix++;
            }
        }
    }
}

/*
=====================
=
= DrawScaleds
=
= Draws all objects that are visable
=
=====================
*/

#define MAXVISABLE MAXACTORS

typedef struct
{
    fixed      x, y;
    short      viewx,
               viewheight,
               shapenum;
    short      flags;          // this must be changed to uint32_t, when you
                               // you need more than 16-flags for drawing
    fixed      heightoff;
    byte       blend;
#ifdef USE_DIR3DSPR
    statobj_t *transsprite;
#endif
    byte       drawpri;
    byte      *sprite;
} visobj_t;

visobj_t vislist[MAXVISABLE];
visobj_t *visptr,*visstep,*farthest;

static short AnimatedStatObjShapeNum(statobj_t *statptr, short shapenum)
{
    int inrange;
    fixed dx, dy;
    int32_t animTotalTics;
    const int32_t ticsperframe = 10;
    statanim_t *anim = &statptr->anim;
    statprox_t *prox = &statptr->prox;

    if (anim->frames != 0)
    {
        if (anim->period.value != 0)
        {
            animTotalTics = anim->frames * ticsperframe;
            anim->tics += tics;

            if (anim->tics < animTotalTics)
            {
                shapenum += anim->tics / ticsperframe;
            }
            else if (anim->tics >= anim->period.value * animTotalTics)
            {
                if (anim->period.rangeStart < anim->period.rangeEnd)
                {
                    anim->period.value = (US_RndT() % 
                        (anim->period.rangeEnd - anim->period.rangeStart)) +
                        anim->period.rangeStart;
                }
                anim->tics = 0;
            }
        }
        else
        {
            shapenum += (gamestate.TimeCount / ticsperframe) % anim->frames;
        }
    }

    if (prox->enabled)
    {
        dx = TILE2POS(statptr->tilex) - player->x;
        dy = TILE2POS(statptr->tiley) - player->y;
        inrange = abs(dx) < prox->dist && abs(dy) < prox->dist;
        shapenum = prox->shapenum[inrange];
    }

    return shapenum;
}

void DrawScaleds (void)
{
    int      i,least,numvisable,height;
    byte     *tilespot,*visspot;
    unsigned spotloc;
    byte     drawpri;

    statobj_t *statptr;
    objtype   *obj;

    visptr = &vislist[0];

#ifdef ANIMOBJ
// Animated Objects code
    frameon+=tics;
    frameon%=2520;
// End Animated Objects code
#endif

//
// place static objects
//
    for (statptr = &statobjlist[0] ; statptr !=laststatobj ; statptr++)
    {
        if ((visptr->shapenum = statptr->shapenum) == -1)
            continue;                                               // object has been deleted

        if (!*statptr->visspot)
            continue;                                               // not visable

#ifdef ANIMOBJ
        switch (statptr->shapenum)
        {
        case SPR_ANIM0:                      // Cop car
            visptr->shapenum+=int((frameon/48)%2);
            break;
        case SPR_ANI0:                      // Streetlight
            visptr->shapenum+=int((frameon/750)%3);
            break;
        case SPR_AN0:                      // Cop car
            visptr->shapenum+=int((frameon/16)%2);
            break;
        case SPR_VEN:
            visptr->shapenum+=int((frameon/8)%2);
            break;
        }
#endif

        if (TransformTile (statptr->tilex,statptr->tiley,
            &visptr->viewx,&visptr->viewheight) && statptr->flags & FL_BONUS)
        {
            GetBonus (statptr);
            if(statptr->shapenum == -1)
                continue;                                           // object has been taken
        }

        if (!visptr->viewheight)
            continue;                                               // to close to the object

        visptr->shapenum = AnimatedStatObjShapeNum(statptr, visptr->shapenum);

#ifdef USE_DIR3DSPR
        if(statptr->flags & FL_DIR_MASK)
            visptr->transsprite=statptr;
        else
            visptr->transsprite=NULL;
#endif

        if (visptr < &vislist[MAXVISABLE-1])    // don't let it overflow
        {
            visptr->flags = (short) statptr->flags;
            visptr->heightoff = 0;
            visptr->drawpri = statptr->drawpri;
            visptr->x = FP(statptr->tilex) + 0x8000;
            visptr->y = FP(statptr->tiley) + 0x8000;
            visptr->blend = statptr->blend;
            visptr->sprite = statptr->sprite;
            visptr++;
        }
    }

//
// place active objects
//
    for (obj = player->next;obj;obj=obj->next)
    {
        if ((visptr->shapenum = Object_GetShapeNum(obj))==0)
            continue;                                               // no shape

        spotloc = (obj->tilex<<mapshift)+obj->tiley;   // optimize: keep in struct?
        visspot = &spotvis[0][0]+spotloc;
        tilespot = &tilemap[0][0]+spotloc;

        //
        // could be in any of the nine surrounding tiles
        //
        if (*visspot
            || ( *(visspot-1) && !*(tilespot-1) )
            || ( *(visspot+1) && !*(tilespot+1) )
            || ( *(visspot-65) && !*(tilespot-65) )
            || ( *(visspot-64) && !*(tilespot-64) )
            || ( *(visspot-63) && !*(tilespot-63) )
            || ( *(visspot+65) && !*(tilespot+65) )
            || ( *(visspot+64) && !*(tilespot+64) )
            || ( *(visspot+63) && !*(tilespot+63) ) )
        {
            obj->active = ac_yes;
            TransformActor (obj);
            if (!obj->viewheight)
                continue;                                               // too close or far away

            visptr->viewx = obj->viewx;
            visptr->viewheight = obj->viewheight;
            if (visptr->shapenum == -1)
                visptr->shapenum = obj->temp1;  // special shape
            else
                visptr->shapenum += obj->sproffset;

            if (obj->state->rotate)
                visptr->shapenum += CalcRotate (obj);

            if (visptr < &vislist[MAXVISABLE-1])    // don't let it overflow
            {
                visptr->flags = (short) obj->flags;
                visptr->heightoff = obj->heightoff;
#ifdef USE_DIR3DSPR
                visptr->transsprite = NULL;
#endif
                visptr->drawpri = obj->drawpri;
                visptr->x = obj->x;
                visptr->y = obj->y;
                visptr->blend = obj->blend;
                visptr->sprite = NULL;
                visptr++;
            }
            obj->flags |= FL_VISABLE;
        }
        else
            obj->flags &= ~FL_VISABLE;
    }

//
// draw from back to front
//
    numvisable = (int) (visptr-&vislist[0]);

    if (!numvisable)
        return;                                                                 // no visable objects

    for (i = 0; i<numvisable; i++)
    {
        least = 32000;
        drawpri = 0;
        for (visstep=&vislist[0] ; visstep<visptr ; visstep++)
        {
            height = visstep->viewheight;
            if (height < least || (height == least && visstep->drawpri > drawpri))
            {
                least = height;
                farthest = visstep;
                drawpri = visstep->drawpri;
            }
        }
        //
        // draw farthest
        //
#ifdef USE_DIR3DSPR
        if(farthest->transsprite)
            Scale3DShape(vbuf, farthest->transsprite, farthest->blend);
        else
#endif
            ScaleShape(farthest->viewx, farthest->shapenum, 
                farthest->viewheight, farthest->flags, farthest->heightoff,
                farthest->x, farthest->y, farthest->blend, 
                (t_compshape *)farthest->sprite);

        farthest->viewheight = 32000;
    }
}

//==========================================================================

/*
==============
=
= DrawPlayerWeapon
=
= Draw the player's hands
=
==============
*/

void DrawPlayerWeapon (void)
{
    int i;
    int shapenum;
#ifdef WEAPONBOBBLE
    int xcenter;
    unsigned height;
    int upperedge_offset;
    Phys_Particle_t *particle;
#endif 

#ifndef SPEAR
    if (gamestate.victoryflag)
    {
#ifndef APOGEE_1_0
        if (player->state == &s_deathcam && (GameTimeCount()&32) )
            SimpleScaleShape(viewwidth/2,SPR_DEATHCAM,viewheight+1,0,false);
#endif
        return;
    }
#endif

    if (gamestate.weapon != -1)
    {
        shapenum = weapinfo[gamestate.weapon].weaponscale +
            gamestate.weaponframe;

#ifdef WEAPONBOBBLE
        xcenter = (viewwidth / 2) - 10 + gamestate.bobber2;
        height = viewheight + 1 + gamestate.bobber;

        particle = &gamestate.weaponshudder.particle;

        xcenter += fixedpt_toint(particle->pos.v[0] + particle->jitterPos.v[0]);
        height += fixedpt_toint(particle->pos.v[2]);
        upperedge_offset = fixedpt_toint(
            particle->pos.v[1] + particle->jitterPos.v[1]);
        for (i = 0; i < 2; i++)
        {
            if (i == 1 && !LT_ShapeNumHasLitVersion(shapenum))
            {
                break;
            }
            SimpleScaleShape(xcenter, 
                (i == 1 ? LT_GetLitShapeNum(shapenum) : shapenum),
                height, MAX(0, upperedge_offset), (i == 0));
        }
#else 
        SimpleScaleShape(viewwidth/2, shapenum, viewheight + 1, 0, true);
#endif
    }

    if (demorecord || demoplayback)
        SimpleScaleShape(viewwidth/2,SPR_DEMO,viewheight+1,0,false);
}

//==========================================================================


/*
=====================
=
= CalcTics
=
=====================
*/

void CalcTics (void)
{
//
// calculate tics since last refresh for adaptive timing
//
    if (lasttimecount > (int32_t) GameTimeCount())
        lasttimecount = GameTimeCount();    // if the game was paused a LONG time

    uint32_t curtime = SDL_GetTicks();
    tics = (curtime * wolfticrate) / 100 - lasttimecount;
    if(!tics)
    {
        // wait until end of current tic
        SDL_Delay(((lasttimecount + 1) * 100) / wolfticrate - curtime);
        tics = 1;
    }

    lasttimecount += tics;

    if (tics>MAXTICS)
        tics = MAXTICS;
}

/*
=====================
=
= ChangeWolfTicRate
=
=====================
*/

void ChangeWolfTicRate (int ticrate)
{
    wolfticrate = ticrate;
    lasttimecount = GameTimeCount();
}


//==========================================================================

void AsmRefresh()
{
    int32_t xstep,ystep;
    longword xpartial,ypartial;
    boolean playerInPushwallBackTile = tilemap[focaltx][focalty] == 64;

    for(pixx=0;pixx<viewwidth;pixx++)
    {
        short angl=midangle+pixelangle[pixx];
        if(angl<0) angl+=FINEANGLES;
        if(angl>=3600) angl-=FINEANGLES;
        if(angl<900)
        {
            xtilestep=1;
            ytilestep=-1;
            xstep=finetangent[900-1-angl];
            ystep=-finetangent[angl];
            xpartial=xpartialup;
            ypartial=ypartialdown;
        }
        else if(angl<1800)
        {
            xtilestep=-1;
            ytilestep=-1;
            xstep=-finetangent[angl-900];
            ystep=-finetangent[1800-1-angl];
            xpartial=xpartialdown;
            ypartial=ypartialdown;
        }
        else if(angl<2700)
        {
            xtilestep=-1;
            ytilestep=1;
            xstep=-finetangent[2700-1-angl];
            ystep=finetangent[angl-1800];
            xpartial=xpartialdown;
            ypartial=ypartialup;
        }
        else if(angl<3600)
        {
            xtilestep=1;
            ytilestep=1;
            xstep=finetangent[angl-2700];
            ystep=finetangent[3600-1-angl];
            xpartial=xpartialup;
            ypartial=ypartialup;
        }
        yintercept=FixedMul(ystep,xpartial)+viewy;
        xtile=focaltx+xtilestep;
        xspot=(word)((xtile<<mapshift)+((uint32_t)yintercept>>16));
        xintercept=FixedMul(xstep,ypartial)+viewx;
        ytile=focalty+ytilestep;
        yspot=(word)((((uint32_t)xintercept>>16)<<mapshift)+ytile);
        texdelta=0;

        // Special treatment when player is in back tile of pushwall
        if(playerInPushwallBackTile)
        {
            if(    pwalldir == di_east && xtilestep ==  1
                || pwalldir == di_west && xtilestep == -1)
            {
                int32_t yintbuf = yintercept - ((ystep * (64 - pwallpos)) >> 6);
                if((yintbuf >> 16) == focalty)   // ray hits pushwall back?
                {
                    if(pwalldir == di_east)
                        xintercept = (focaltx << TILESHIFT) + (pwallpos << 10);
                    else
                        xintercept = (focaltx << TILESHIFT) - TILEGLOBAL + ((64 - pwallpos) << 10);
                    yintercept = yintbuf;
                    ytile = (short) (yintercept >> TILESHIFT);
                    tilehit = pwalltile;
                    HitVertWall();
                    continue;
                }
            }
            else if(pwalldir == di_south && ytilestep ==  1
                ||  pwalldir == di_north && ytilestep == -1)
            {
                int32_t xintbuf = xintercept - ((xstep * (64 - pwallpos)) >> 6);
                if((xintbuf >> 16) == focaltx)   // ray hits pushwall back?
                {
                    xintercept = xintbuf;
                    if(pwalldir == di_south)
                        yintercept = (focalty << TILESHIFT) + (pwallpos << 10);
                    else
                        yintercept = (focalty << TILESHIFT) - TILEGLOBAL + ((64 - pwallpos) << 10);
                    xtile = (short) (xintercept >> TILESHIFT);
                    tilehit = pwalltile;
                    HitHorizWall();
                    continue;
                }
            }
        }

        do
        {
            if(ytilestep==-1 && (yintercept>>16)<=ytile) goto horizentry;
            if(ytilestep==1 && (yintercept>>16)>=ytile) goto horizentry;
vertentry:
            if((uint32_t)yintercept>mapheight*65536-1 || (word)xtile>=mapwidth)
            {
                if(xtile<0) xintercept=0, xtile=0;
                else if(xtile>=mapwidth) xintercept=mapwidth<<TILESHIFT, xtile=mapwidth-1;
                else xtile=(short) (xintercept >> TILESHIFT);
                if(yintercept<0) yintercept=0, ytile=0;
                else if(yintercept>=(mapheight<<TILESHIFT)) yintercept=mapheight<<TILESHIFT, ytile=mapheight-1;
                yspot=0xffff;
                tilehit=0;
                HitHorizBorder();
                break;
            }
            if(xspot>=maparea) break;
            tilehit=((byte *)tilemap)[xspot];
            if(tilehit)
            {
                if(tilehit&0x80)
                {
                    int32_t yintbuf=yintercept+(ystep>>1);
                    if((yintbuf>>16)!=(yintercept>>16))
                        goto passvert;
                    if((word)yintbuf<doorposition[tilehit&0x7f])
                        goto passvert;
                    yintercept=yintbuf;
                    xintercept=(xtile<<TILESHIFT)|0x8000;
                    ytile = (short) (yintercept >> TILESHIFT);
                    HitVertDoor();
                }
                else
                {
                    if(tilehit==PUSHMOVETILE)
                    {
                        if(pwalldir==di_west || pwalldir==di_east)
                        {
	                        int32_t yintbuf;
                            int pwallposnorm;
                            int pwallposinv;
                            if(pwalldir==di_west)
                            {
                                pwallposnorm = 64-pwallpos;
                                pwallposinv = pwallpos;
                            }
                            else
                            {
                                pwallposnorm = pwallpos;
                                pwallposinv = 64-pwallpos;
                            }
                            if(pwalldir == di_east && xtile==pwallx && ((uint32_t)yintercept>>16)==pwally
                                || pwalldir == di_west && !(xtile==pwallx && ((uint32_t)yintercept>>16)==pwally))
                            {
                                yintbuf=yintercept+((ystep*pwallposnorm)>>6);
                                if((yintbuf>>16)!=(yintercept>>16))
                                    goto passvert;

                                xintercept=(xtile<<TILESHIFT)+TILEGLOBAL-(pwallposinv<<10);
                                yintercept=yintbuf;
                                ytile = (short) (yintercept >> TILESHIFT);
                                tilehit=pwalltile;
                                HitVertWall();
                            }
                            else
                            {
                                yintbuf=yintercept+((ystep*pwallposinv)>>6);
                                if((yintbuf>>16)!=(yintercept>>16))
                                    goto passvert;

                                xintercept=(xtile<<TILESHIFT)-(pwallposinv<<10);
                                yintercept=yintbuf;
                                ytile = (short) (yintercept >> TILESHIFT);
                                tilehit=pwalltile;
                                HitVertWall();
                            }
                        }
                        else
                        {
                            int pwallposi = pwallpos;
                            if(pwalldir==di_north) pwallposi = 64-pwallpos;
                            if(pwalldir==di_south && (word)yintercept<(pwallposi<<10)
                                || pwalldir==di_north && (word)yintercept>(pwallposi<<10))
                            {
                                if(((uint32_t)yintercept>>16)==pwally && xtile==pwallx)
                                {
                                    if(pwalldir==di_south && (int32_t)((word)yintercept)+ystep<(pwallposi<<10)
                                            || pwalldir==di_north && (int32_t)((word)yintercept)+ystep>(pwallposi<<10))
                                        goto passvert;

                                    if(pwalldir==di_south)
                                        yintercept=(yintercept&0xffff0000)+(pwallposi<<10);
                                    else
                                        yintercept=(yintercept&0xffff0000)-TILEGLOBAL+(pwallposi<<10);
                                    xintercept=xintercept-((xstep*(64-pwallpos))>>6);
                                    xtile = (short) (xintercept >> TILESHIFT);
                                    tilehit=pwalltile;
                                    HitHorizWall();
                                }
                                else
                                {
                                    texdelta = -(pwallposi<<10);
                                    xintercept=xtile<<TILESHIFT;
                                    ytile = (short) (yintercept >> TILESHIFT);
                                    tilehit=pwalltile;
                                    HitVertWall();
                                }
                            }
                            else
                            {
                                if(((uint32_t)yintercept>>16)==pwally && xtile==pwallx)
                                {
                                    texdelta = -(pwallposi<<10);
                                    xintercept=xtile<<TILESHIFT;
                                    ytile = (short) (yintercept >> TILESHIFT);
                                    tilehit=pwalltile;
                                    HitVertWall();
                                }
                                else
                                {
                                    if(pwalldir==di_south && (int32_t)((word)yintercept)+ystep>(pwallposi<<10)
                                            || pwalldir==di_north && (int32_t)((word)yintercept)+ystep<(pwallposi<<10))
                                        goto passvert;

                                    if(pwalldir==di_south)
                                        yintercept=(yintercept&0xffff0000)-((64-pwallpos)<<10);
                                    else
                                        yintercept=(yintercept&0xffff0000)+((64-pwallpos)<<10);
                                    xintercept=xintercept-((xstep*pwallpos)>>6);
                                    xtile = (short) (xintercept >> TILESHIFT);
                                    tilehit=pwalltile;
                                    HitHorizWall();
                                }
                            }
                        }
                    }
                    else
                    {
                        xintercept=xtile<<TILESHIFT;
                        ytile = (short) (yintercept >> TILESHIFT);
                        HitVertWall();
                    }
                }
                break;
            }
passvert:
            *((byte *)spotvis+xspot)=1;
            xtile+=xtilestep;
            yintercept+=ystep;
            xspot=(word)((xtile<<mapshift)+((uint32_t)yintercept>>16));
        }
        while(1);
        continue;

        do
        {
            if(xtilestep==-1 && (xintercept>>16)<=xtile) goto vertentry;
            if(xtilestep==1 && (xintercept>>16)>=xtile) goto vertentry;
horizentry:
            if((uint32_t)xintercept>mapwidth*65536-1 || (word)ytile>=mapheight)
            {
                if(ytile<0) yintercept=0, ytile=0;
                else if(ytile>=mapheight) yintercept=mapheight<<TILESHIFT, ytile=mapheight-1;
                else ytile=(short) (yintercept >> TILESHIFT);
                if(xintercept<0) xintercept=0, xtile=0;
                else if(xintercept>=(mapwidth<<TILESHIFT)) xintercept=mapwidth<<TILESHIFT, xtile=mapwidth-1;
                xspot=0xffff;
                tilehit=0;
                HitVertBorder();
                break;
            }
            if(yspot>=maparea) break;
            tilehit=((byte *)tilemap)[yspot];
            if(tilehit)
            {
                if(tilehit&0x80)
                {
                    int32_t xintbuf=xintercept+(xstep>>1);
                    if((xintbuf>>16)!=(xintercept>>16))
                        goto passhoriz;
                    if((word)xintbuf<doorposition[tilehit&0x7f])
                        goto passhoriz;
                    xintercept=xintbuf;
                    yintercept=(ytile<<TILESHIFT)+0x8000;
                    xtile = (short) (xintercept >> TILESHIFT);
                    HitHorizDoor();
                }
                else
                {
                    if(tilehit==PUSHMOVETILE)
                    {
                        if(pwalldir==di_north || pwalldir==di_south)
                        {
                            int32_t xintbuf;
                            int pwallposnorm;
                            int pwallposinv;
                            if(pwalldir==di_north)
                            {
                                pwallposnorm = 64-pwallpos;
                                pwallposinv = pwallpos;
                            }
                            else
                            {
                                pwallposnorm = pwallpos;
                                pwallposinv = 64-pwallpos;
                            }
                            if(pwalldir == di_south && ytile==pwally && ((uint32_t)xintercept>>16)==pwallx
                                || pwalldir == di_north && !(ytile==pwally && ((uint32_t)xintercept>>16)==pwallx))
                            {
                                xintbuf=xintercept+((xstep*pwallposnorm)>>6);
                                if((xintbuf>>16)!=(xintercept>>16))
                                    goto passhoriz;

                                yintercept=(ytile<<TILESHIFT)+TILEGLOBAL-(pwallposinv<<10);
                                xintercept=xintbuf;
                                xtile = (short) (xintercept >> TILESHIFT);
                                tilehit=pwalltile;
                                HitHorizWall();
                            }
                            else
                            {
                                xintbuf=xintercept+((xstep*pwallposinv)>>6);
                                if((xintbuf>>16)!=(xintercept>>16))
                                    goto passhoriz;

                                yintercept=(ytile<<TILESHIFT)-(pwallposinv<<10);
                                xintercept=xintbuf;
                                xtile = (short) (xintercept >> TILESHIFT);
                                tilehit=pwalltile;
                                HitHorizWall();
                            }
                        }
                        else
                        {
                            int pwallposi = pwallpos;
                            if(pwalldir==di_west) pwallposi = 64-pwallpos;
                            if(pwalldir==di_east && (word)xintercept<(pwallposi<<10)
                                    || pwalldir==di_west && (word)xintercept>(pwallposi<<10))
                            {
                                if(((uint32_t)xintercept>>16)==pwallx && ytile==pwally)
                                {
                                    if(pwalldir==di_east && (int32_t)((word)xintercept)+xstep<(pwallposi<<10)
                                            || pwalldir==di_west && (int32_t)((word)xintercept)+xstep>(pwallposi<<10))
                                        goto passhoriz;

                                    if(pwalldir==di_east)
                                        xintercept=(xintercept&0xffff0000)+(pwallposi<<10);
                                    else
                                        xintercept=(xintercept&0xffff0000)-TILEGLOBAL+(pwallposi<<10);
                                    yintercept=yintercept-((ystep*(64-pwallpos))>>6);
                                    ytile = (short) (yintercept >> TILESHIFT);
                                    tilehit=pwalltile;
                                    HitVertWall();
                                }
                                else
                                {
                                    texdelta = -(pwallposi<<10);
                                    yintercept=ytile<<TILESHIFT;
                                    xtile = (short) (xintercept >> TILESHIFT);
                                    tilehit=pwalltile;
                                    HitHorizWall();
                                }
                            }
                            else
                            {
                                if(((uint32_t)xintercept>>16)==pwallx && ytile==pwally)
                                {
                                    texdelta = -(pwallposi<<10);
                                    yintercept=ytile<<TILESHIFT;
                                    xtile = (short) (xintercept >> TILESHIFT);
                                    tilehit=pwalltile;
                                    HitHorizWall();
                                }
                                else
                                {
                                    if(pwalldir==di_east && (int32_t)((word)xintercept)+xstep>(pwallposi<<10)
                                            || pwalldir==di_west && (int32_t)((word)xintercept)+xstep<(pwallposi<<10))
                                        goto passhoriz;

                                    if(pwalldir==di_east)
                                        xintercept=(xintercept&0xffff0000)-((64-pwallpos)<<10);
                                    else
                                        xintercept=(xintercept&0xffff0000)+((64-pwallpos)<<10);
                                    yintercept=yintercept-((ystep*pwallpos)>>6);
                                    ytile = (short) (yintercept >> TILESHIFT);
                                    tilehit=pwalltile;
                                    HitVertWall();
                                }
                            }
                        }
                    }
                    else
                    {
                        yintercept=ytile<<TILESHIFT;
                        xtile = (short) (xintercept >> TILESHIFT);
                        HitHorizWall();
                    }
                }
                break;
            }
passhoriz:
            *((byte *)spotvis+yspot)=1;
            ytile+=ytilestep;
            xintercept+=xstep;
            yspot=(word)((((uint32_t)xintercept>>16)<<mapshift)+ytile);
        }
        while(1);
    }
}

/*
====================
=
= WallRefresh
=
====================
*/

void WallRefresh (void)
{
    xpartialdown = viewx&(TILEGLOBAL-1);
    xpartialup = TILEGLOBAL-xpartialdown;
    ypartialdown = viewy&(TILEGLOBAL-1);
    ypartialup = TILEGLOBAL-ypartialdown;

    min_wallheight = viewheight;
    AsmRefresh ();
    ScalePost ();                   // no more optimization on last post
}

void CalcViewVariables()
{
    viewangle = player->angle;
    midangle = viewangle*(FINEANGLES/ANGLES);
    viewsin = sintable[viewangle];
    viewcos = costable[viewangle];
    viewx = player->x - FixedMul(focallength,viewcos);
    viewy = player->y + FixedMul(focallength,viewsin);

    focaltx = (short)(viewx>>TILESHIFT);
    focalty = (short)(viewy>>TILESHIFT);

    viewtx = (short)(player->x >> TILESHIFT);
    viewty = (short)(player->y >> TILESHIFT);
}

//==========================================================================

/*
========================
=
= ThreeDRefresh
=
========================
*/

void    ThreeDRefresh (void)
{
    unsigned screenofs;

//
// clear out the traced array
//
    memset(spotvis,0,maparea);
    spotvis[player->tilex][player->tiley] = 1;       // Detect all sprites over player fix

    screenofs = viewscreeny*screen.pitch+viewscreenx;

    vbuf = VBufLockSurface();
	if ( !vbuf.start )
		return;				// avoid crashes when in background
    vbuf = VBufOffset(vbuf, screenofs);

    CalcViewVariables();

//
// follow the walls from there to the right, drawing as we go
//
#if defined(USE_FEATUREFLAGS) && defined(USE_STARSKY)
    if(GetFeatureFlags() & FF_STARSKY)
        DrawStarSky(vbuf);
#endif
#ifdef HORIZON
#if defined(USE_FEATUREFLAGS) && defined(USE_PARALLAX)
    if(GetFeatureFlags() & FF_PARALLAXSKY)
        DrawParallax(vbuf);
#endif
#endif
    WallRefresh ();

#ifndef HORIZON
#if defined(USE_FEATUREFLAGS) && defined(USE_PARALLAX)
    if(GetFeatureFlags() & FF_PARALLAXSKY)
        DrawParallax(vbuf);
#endif
#endif
#if defined(USE_FEATUREFLAGS) && defined(USE_CLOUDSKY)
    if(GetFeatureFlags() & FF_CLOUDSKY)
        DrawClouds(vbuf, vbufPitch, min_wallheight);
#endif
#ifdef USE_FLOORCEILINGTEX
    DrawFloorAndCeiling(vbuf, min_wallheight);
#endif

//
// draw all the scaled images
//
    DrawScaleds();                  // draw scaled stuff

#if defined(USE_FEATUREFLAGS) && defined(USE_RAIN)
    if(GetFeatureFlags() & FF_RAIN)
        DrawRain(vbuf, vbufPitch);
#endif
#if defined(USE_FEATUREFLAGS) && defined(USE_SNOW)
    if(GetFeatureFlags() & FF_SNOW)
        DrawSnow(vbuf, vbufPitch);
#endif

    DrawPlayerWeapon ();    // draw player's hands

    vbuf = VBufUnlockSurface();


//
// show screen and time last cycle
//

    if (fizzlein)
    {
        FizzleFade(0, 0, screen.w, screen.h, 20, false);
        fizzlein = false;

        lasttimecount = GameTimeCount();          // don't make a big tic count
    }
    else
    {
#ifndef REMDEBUG
        if (fpscounter)
        {
            fontnumber = 0;
            SETFONTCOLOR(7,127);
            PrintX=4; PrintY=1;
            VWB_Bar(0,0,50,10,bordercol);
            US_PrintSigned(fps);
            US_Print(" fps");
        }
#endif
        VH_UpdateScreen();
    }

#ifndef REMDEBUG
    if (fpscounter)
    {
        fps_frames++;
#if 0
        fps_time+=tics;

        if(fps_time>35)
        {
            fps_time-=35;
            fps=fps_frames<<1;
            fps_frames=0;
        }
#else
		// new code to output true FPS once per second
		uint32_t t = SDL_GetTicks();
		if ( !lastfpsinit )
		{
			lastfpsticks = t;
			lastfpsinit = 1;
		}
		int32_t delta = ((int32_t)t - (int32_t)lastfpsticks);
		if ( delta >= 1000 )
		{
			fps = (int32_t)floor( fps_frames * 1000.0 / delta + 0.5 );
			fps_frames = 0;
			lastfpsticks = t;
		}
#endif
    }
#endif
}

//==========================================================================

#define VBUF_MAX_CACHE_WRITES (640 * 400 * 2)

typedef struct VBufCacheWrite_s
{
    int off;
    uint32_t color;
} VBufCacheWrite_t;

typedef struct VBufCache_s
{
    VBufCacheWrite_t writes[VBUF_MAX_CACHE_WRITES];
    int totalWrites;
} VBufCache_t;

typedef struct VBufFlash_s
{
    VBufLightTable_t lightTable;
} VBufFlash_t;

VBufLightTable_t VBufLightTable;
VBufFadeTable_t VBufFadeTable;
VBufBlendTable_t VBufBlendTable;
VBufLightTableFast_t *VBufLightTableFast;
VBufBlendTableFast_t VBufBlendTableFast;

static VBufCache_t s_vbufCache;
static VBufLightTable_t s_vbufLightTableDefault;
static VBufLightTable_t s_vbufLightShiftTableStart;
static VBufLightTable_t s_vbufNightVisionTable;

static bool s_vbufLightShiftInProgress = false;

void VBufSetLightTable(SDL_Color *pal, int red, int green, int blue)
{
    int max;
    int r, g, b;
    int shade, index;
    uint32_t col;

    for (shade = 0; shade < LT_SHADE_RANGE; shade++)
    {
        for (index = 0; index < 256; index++)
        {
            r = (shade * pal[index].r + ((LT_SHADE_RANGE - 1) - shade) * red) /
                255;
            g = (shade * pal[index].g + ((LT_SHADE_RANGE - 1) - shade) * green) /
                255;
            b = (shade * pal[index].b + ((LT_SHADE_RANGE - 1) - shade) * blue) /
                255;
            col = LT_ClipPack(r, g, b);

            memcpy(&VBufLightTable[shade][index], &col, 4);
        }
    }
}

void VBufApplyLightShift(int red, int green, int blue)
{
    int k;
    int shade, index;
    int value;
    bool endshift;
    uint32_t col;

    endshift = (red == 0 && green == 0 && blue == 0);

    if (!s_vbufLightShiftInProgress && !endshift)
    {
        memcpy(s_vbufLightShiftTableStart, 
            VBufLightTable, sizeof(VBufLightTable_t));
        s_vbufLightShiftInProgress = true;
    }
    else if (s_vbufLightShiftInProgress && endshift)
    {
        memcpy(VBufLightTable, 
            s_vbufLightShiftTableStart, sizeof(VBufLightTable_t));
        s_vbufLightShiftInProgress = false;
    }

    if (endshift)
    {
        return;
    }

    for (shade = 0; shade < LT_SHADE_RANGE; shade++)
    {
        for (index = 0; index < 256; index++)
        {
            memcpy(&col, &s_vbufLightShiftTableStart[shade][index], 4);
            col = LT_ClipPack(LT_RED(col) + red, LT_GREEN(col) + green,
                LT_BLUE(col) + blue);
            memcpy(VBufLightTable[shade][index], &col, 4);
        }
    }
}

void VBufInitFadeTable(int red, int green, int blue)
{
    int val, shift;

    for (val = 0; val < 256; val++)
    {
        for (shift = 0; shift < 256; shift++)
        {
            VBufFadeTable[val][shift][0] = 
                ((val * (255 - shift)) + (red * shift)) / 255;
            VBufFadeTable[val][shift][1] = 
                ((val * (255 - shift)) + (green * shift)) / 255;
            VBufFadeTable[val][shift][2] = 
                ((val * (255 - shift)) + (blue * shift)) / 255;
        }
    }
}

void VBufInitBlendTable(void)
{
    int x, y, z;

    for (x = 0; x < 256; x++)
    {
        for (y = 0; y < 256; y++)
        {
            for (z = 0; z < 256; z++)
            {
                VBufBlendTable[z][x][y] = 
                    (x * (255 - z) + y * z) / 255;
            }
        }
    }
}

static void VBufInitNightVisionTable(void)
{
    int shade, index;
    fixed I;

    for (shade = 0; shade < 256; shade++)
    {
        for (index = 0; index < 256; index++)
        {
            I = FP(s_vbufLightTableDefault[shade][index][0]) / 255 + 
                FP(s_vbufLightTableDefault[shade][index][1]) / 255 + 
                FP(s_vbufLightTableDefault[shade][index][2]) / 255;
            I = MIN(I, FP(1));

            s_vbufNightVisionTable[shade][index][1] = 255 - fixedpt_toint(I * 255);
        }
    }
}

void VBufInitTables(void)
{
    VBufSetLightTable(gamepal, 0, 0, 0);
    memcpy(s_vbufLightTableDefault, VBufLightTable,
        sizeof(VBufLightTable_t));
}

void VBufSetNightVisionLightTable(void)
{
    s_vbufLightShiftInProgress = false;
    memcpy(VBufLightTable, s_vbufNightVisionTable,
        sizeof(VBufLightTable));
}

void VBufSetDefaultLightTable(void)
{
    s_vbufLightShiftInProgress = false;
    memcpy(VBufLightTable, s_vbufLightTableDefault, 
        sizeof(VBufLightTable));
}

VBuf_t VBufLockSurfaceEx(SDL_Surface *surf)
{
    VBuf_t vbuf;
    vbuf.start = vbuf.pix = VL_LockSurface(surf);
    return vbuf;
}

VBuf_t VBufUnlockSurfaceEx(SDL_Surface *surf)
{
    VL_UnlockSurface(surf);
    return VBuf(NULL);
}

VBuf_t VBufLockSurface(void)
{
    return VBufLockSurfaceEx(screen.buf);
}

VBuf_t VBufUnlockSurface(void)
{
    return VBufUnlockSurfaceEx(screen.buf);
}

void VBufWritePix2D(VBuf_t vbuf, int off, byte pix)
{
    VBufCacheWrite_t *write;
    uint32_t color;

    color = *((uint32_t *)s_vbufLightTableDefault[255][pix]);

    if (vbuf.start == screen.buf->pixels && screen.isDoubleBuffered)
    {
        write = &s_vbufCache.writes[s_vbufCache.totalWrites];
        write->off = off + ((vbuf.pix - vbuf.start) / 4);
        write->color = color;

        assert(s_vbufCache.totalWrites < VBUF_MAX_CACHE_WRITES);
        s_vbufCache.totalWrites++;
    }

    VBufWriteColor(vbuf, off, color);
}

void VBufFlushWriteCache(void)
{
    int i;
    VBufCacheWrite_t *write;
    VBuf_t vbuf;

    if (!screen.isDoubleBuffered)
    {
        return;
    }

    vbuf = VBufLockSurface();

    for (i = 0; i < s_vbufCache.totalWrites; i++)
    {
        write = &s_vbufCache.writes[i];
        VBufWriteColor(vbuf, write->off, write->color);
    }

    VBufUnlockSurface();

    s_vbufCache.totalWrites = 0;
}

void VBufResetWriteCache(void)
{
    if (!screen.isDoubleBuffered)
    {
        return;
    }

    s_vbufCache.totalWrites = 0;
}

//==========================================================================

/*
===================
=
= HUD_DrawPlayBorder
=
===================
*/

void HUD_DrawPlayBorder (void)
{
    DrawPlayBorderEx(true);
}

//==========================================================================

fixed Math_Distance2(fixed x1, fixed y1, fixed x2, fixed y2)
{
    fixed dist;

    if (x1 != x2 && y1 != y2)
    {
        dist = fixedpt_sqrt(FP_SQ(x1 - x2) + FP_SQ(y1 - y2));
    }
    else if (x1 != x2)
    {
        dist = abs(x1 - x2);
    }
    else if (y1 != y2)
    {
        dist = abs(y1 - y2);
    }
    else
    {
        dist = 0;
    }

    return dist;
}
