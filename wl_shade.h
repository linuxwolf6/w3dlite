#if defined(USE_SHADING) && !defined(_WL_SHADE_H_)
#define _WL_SHADE_H_

#include "wl_def.h"
#include "lw_vec.h"

#define LT_DeleteLight(lightId) LT_DeleteLightEx(lightId, true)

LT_t *LT(void);
void LT_Startup(void);
void LT_Shutdown(void);
int LT_NewLight(void);
void LT_DeleteLightEx(int lightId, bool markOnly);
LT_Light_t *LT_GetLightById(int lightId);
LT_Spectra_t LT_ComputeLightTileIntensity(int lighttilex, int lighttiley);
int32_t LT_SaveTheGame(FILE *file, int32_t checksum);
int32_t LT_LoadTheGame(FILE *file, int32_t checksum);
void LT_ResetLights(void);
int LT_SpawnLightPredef(fixed x, fixed y, LT_Light_Predef_t predef);
LT_Spectra_t LT_GlobalAmbient(void);
void LT_BlitSurface(void);
void LT_LightThinkFade(LT_Light_t *light);
void LT_LightThinkJitter(LT_Light_t *light);
void LT_LightThinkStrobe(LT_Light_t *light);
void LT_LightThinkLifeTimed(LT_Light_t *light);
void LT_LightThinkTaserFlash(LT_Light_t *light);
void LT_UpdateLights(void);
uint32_t LT_GetShade(int lighttilex, int lighttiley, uint32_t col,
    byte palCol);
LT_Shade_t LT_GetShadeFast(int scale, int lighttilex, int lighttiley, LT_Spectra_t lightintensity);
void LT_SetDistAttenuation(int distAttenuation);
extern uint32_t LT_SpriteColor(byte col, lwlib_TPoint3f spriteLitLevel);
lwlib_TPoint3f LT_GetShadedSpriteLitLevel(fixed x, fixed y);
bool LT_ShapeNumHasLitVersion(int shapenum);
int LT_GetLitShapeNum(int shapenum);
void LT_RefreshBrightnessTable(LT_Light_t *light);

#endif
