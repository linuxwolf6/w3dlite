#include "version.h"

#ifdef USE_DIR3DSPR
#include "wl_def.h"
#include "wl_shade.h"
#include "lw_intmap.h"

const char *spritestrs[SPR_NUMSPRITES] =
{
    "SPR_DEMO",
#ifndef APOGEE_1_0
    "SPR_DEATHCAM",
#endif
//
// static sprites
//
    "SPR_STAT_0","SPR_STAT_1","SPR_STAT_2","SPR_STAT_3",
    "SPR_STAT_4","SPR_STAT_5","SPR_STAT_6","SPR_STAT_7",

    "SPR_STAT_8","SPR_STAT_9","SPR_STAT_10","SPR_STAT_11",
    "SPR_STAT_12","SPR_STAT_13","SPR_STAT_14","SPR_STAT_15",

    "SPR_STAT_16","SPR_STAT_17","SPR_STAT_18","SPR_STAT_19",
    "SPR_STAT_20","SPR_STAT_21","SPR_STAT_22","SPR_STAT_23",

    "SPR_STAT_24","SPR_STAT_25","SPR_STAT_26","SPR_STAT_27",
    "SPR_STAT_28","SPR_STAT_29","SPR_STAT_30","SPR_STAT_31",

    "SPR_STAT_32","SPR_STAT_33","SPR_STAT_34","SPR_STAT_35",
    "SPR_STAT_36","SPR_STAT_37","SPR_STAT_38","SPR_STAT_39",

    "SPR_STAT_40","SPR_STAT_41","SPR_STAT_42","SPR_STAT_43",
    "SPR_STAT_44","SPR_STAT_45","SPR_STAT_46","SPR_STAT_47",

#ifdef SPEAR
    "SPR_STAT_48","SPR_STAT_49","SPR_STAT_50","SPR_STAT_51",
#endif

//
// guard
//
    "SPR_GRD_S_1","SPR_GRD_S_2","SPR_GRD_S_3","SPR_GRD_S_4",
    "SPR_GRD_S_5","SPR_GRD_S_6","SPR_GRD_S_7","SPR_GRD_S_8",

    "SPR_GRD_W1_1","SPR_GRD_W1_2","SPR_GRD_W1_3","SPR_GRD_W1_4",
    "SPR_GRD_W1_5","SPR_GRD_W1_6","SPR_GRD_W1_7","SPR_GRD_W1_8",

    "SPR_GRD_W2_1","SPR_GRD_W2_2","SPR_GRD_W2_3","SPR_GRD_W2_4",
    "SPR_GRD_W2_5","SPR_GRD_W2_6","SPR_GRD_W2_7","SPR_GRD_W2_8",

    "SPR_GRD_W3_1","SPR_GRD_W3_2","SPR_GRD_W3_3","SPR_GRD_W3_4",
    "SPR_GRD_W3_5","SPR_GRD_W3_6","SPR_GRD_W3_7","SPR_GRD_W3_8",

    "SPR_GRD_W4_1","SPR_GRD_W4_2","SPR_GRD_W4_3","SPR_GRD_W4_4",
    "SPR_GRD_W4_5","SPR_GRD_W4_6","SPR_GRD_W4_7","SPR_GRD_W4_8",

    "SPR_GRD_PAIN_1","SPR_GRD_DIE_1","SPR_GRD_DIE_2","SPR_GRD_DIE_3",
    "SPR_GRD_PAIN_2","SPR_GRD_DEAD",

    "SPR_GRD_SHOOT1","SPR_GRD_SHOOT2","SPR_GRD_SHOOT3",

//
// dogs
//
    "SPR_DOG_W1_1","SPR_DOG_W1_2","SPR_DOG_W1_3","SPR_DOG_W1_4",
    "SPR_DOG_W1_5","SPR_DOG_W1_6","SPR_DOG_W1_7","SPR_DOG_W1_8",

    "SPR_DOG_W2_1","SPR_DOG_W2_2","SPR_DOG_W2_3","SPR_DOG_W2_4",
    "SPR_DOG_W2_5","SPR_DOG_W2_6","SPR_DOG_W2_7","SPR_DOG_W2_8",

    "SPR_DOG_W3_1","SPR_DOG_W3_2","SPR_DOG_W3_3","SPR_DOG_W3_4",
    "SPR_DOG_W3_5","SPR_DOG_W3_6","SPR_DOG_W3_7","SPR_DOG_W3_8",

    "SPR_DOG_W4_1","SPR_DOG_W4_2","SPR_DOG_W4_3","SPR_DOG_W4_4",
    "SPR_DOG_W4_5","SPR_DOG_W4_6","SPR_DOG_W4_7","SPR_DOG_W4_8",

    "SPR_DOG_DIE_1","SPR_DOG_DIE_2","SPR_DOG_DIE_3","SPR_DOG_DEAD",
    "SPR_DOG_JUMP1","SPR_DOG_JUMP2","SPR_DOG_JUMP3",



//
// ss
//
    "SPR_SS_S_1","SPR_SS_S_2","SPR_SS_S_3","SPR_SS_S_4",
    "SPR_SS_S_5","SPR_SS_S_6","SPR_SS_S_7","SPR_SS_S_8",

    "SPR_SS_W1_1","SPR_SS_W1_2","SPR_SS_W1_3","SPR_SS_W1_4",
    "SPR_SS_W1_5","SPR_SS_W1_6","SPR_SS_W1_7","SPR_SS_W1_8",

    "SPR_SS_W2_1","SPR_SS_W2_2","SPR_SS_W2_3","SPR_SS_W2_4",
    "SPR_SS_W2_5","SPR_SS_W2_6","SPR_SS_W2_7","SPR_SS_W2_8",

    "SPR_SS_W3_1","SPR_SS_W3_2","SPR_SS_W3_3","SPR_SS_W3_4",
    "SPR_SS_W3_5","SPR_SS_W3_6","SPR_SS_W3_7","SPR_SS_W3_8",

    "SPR_SS_W4_1","SPR_SS_W4_2","SPR_SS_W4_3","SPR_SS_W4_4",
    "SPR_SS_W4_5","SPR_SS_W4_6","SPR_SS_W4_7","SPR_SS_W4_8",

    "SPR_SS_PAIN_1","SPR_SS_DIE_1","SPR_SS_DIE_2","SPR_SS_DIE_3",
    "SPR_SS_PAIN_2","SPR_SS_DEAD",

    "SPR_SS_SHOOT1","SPR_SS_SHOOT2","SPR_SS_SHOOT3",

//
// mutant
//
    "SPR_MUT_S_1","SPR_MUT_S_2","SPR_MUT_S_3","SPR_MUT_S_4",
    "SPR_MUT_S_5","SPR_MUT_S_6","SPR_MUT_S_7","SPR_MUT_S_8",

    "SPR_MUT_W1_1","SPR_MUT_W1_2","SPR_MUT_W1_3","SPR_MUT_W1_4",
    "SPR_MUT_W1_5","SPR_MUT_W1_6","SPR_MUT_W1_7","SPR_MUT_W1_8",

    "SPR_MUT_W2_1","SPR_MUT_W2_2","SPR_MUT_W2_3","SPR_MUT_W2_4",
    "SPR_MUT_W2_5","SPR_MUT_W2_6","SPR_MUT_W2_7","SPR_MUT_W2_8",

    "SPR_MUT_W3_1","SPR_MUT_W3_2","SPR_MUT_W3_3","SPR_MUT_W3_4",
    "SPR_MUT_W3_5","SPR_MUT_W3_6","SPR_MUT_W3_7","SPR_MUT_W3_8",

    "SPR_MUT_W4_1","SPR_MUT_W4_2","SPR_MUT_W4_3","SPR_MUT_W4_4",
    "SPR_MUT_W4_5","SPR_MUT_W4_6","SPR_MUT_W4_7","SPR_MUT_W4_8",

    "SPR_MUT_PAIN_1","SPR_MUT_DIE_1","SPR_MUT_DIE_2","SPR_MUT_DIE_3",
    "SPR_MUT_PAIN_2","SPR_MUT_DIE_4","SPR_MUT_DEAD",

    "SPR_MUT_SHOOT1","SPR_MUT_SHOOT2","SPR_MUT_SHOOT3","SPR_MUT_SHOOT4",

//
// officer
//
    "SPR_OFC_S_1","SPR_OFC_S_2","SPR_OFC_S_3","SPR_OFC_S_4",
    "SPR_OFC_S_5","SPR_OFC_S_6","SPR_OFC_S_7","SPR_OFC_S_8",

    "SPR_OFC_W1_1","SPR_OFC_W1_2","SPR_OFC_W1_3","SPR_OFC_W1_4",
    "SPR_OFC_W1_5","SPR_OFC_W1_6","SPR_OFC_W1_7","SPR_OFC_W1_8",

    "SPR_OFC_W2_1","SPR_OFC_W2_2","SPR_OFC_W2_3","SPR_OFC_W2_4",
    "SPR_OFC_W2_5","SPR_OFC_W2_6","SPR_OFC_W2_7","SPR_OFC_W2_8",

    "SPR_OFC_W3_1","SPR_OFC_W3_2","SPR_OFC_W3_3","SPR_OFC_W3_4",
    "SPR_OFC_W3_5","SPR_OFC_W3_6","SPR_OFC_W3_7","SPR_OFC_W3_8",

    "SPR_OFC_W4_1","SPR_OFC_W4_2","SPR_OFC_W4_3","SPR_OFC_W4_4",
    "SPR_OFC_W4_5","SPR_OFC_W4_6","SPR_OFC_W4_7","SPR_OFC_W4_8",

    "SPR_OFC_PAIN_1","SPR_OFC_DIE_1","SPR_OFC_DIE_2","SPR_OFC_DIE_3",
    "SPR_OFC_PAIN_2","SPR_OFC_DIE_4","SPR_OFC_DEAD",

    "SPR_OFC_SHOOT1","SPR_OFC_SHOOT2","SPR_OFC_SHOOT3",

#ifndef SPEAR
//
// ghosts
//
    "SPR_BLINKY_W1","SPR_BLINKY_W2","SPR_PINKY_W1","SPR_PINKY_W2",
    "SPR_CLYDE_W1","SPR_CLYDE_W2","SPR_INKY_W1","SPR_INKY_W2",

//
// hans
//
    "SPR_BOSS_W1","SPR_BOSS_W2","SPR_BOSS_W3","SPR_BOSS_W4",
    "SPR_BOSS_SHOOT1","SPR_BOSS_SHOOT2","SPR_BOSS_SHOOT3","SPR_BOSS_DEAD",

    "SPR_BOSS_DIE1","SPR_BOSS_DIE2","SPR_BOSS_DIE3",

//
// schabbs
//
    "SPR_SCHABB_W1","SPR_SCHABB_W2","SPR_SCHABB_W3","SPR_SCHABB_W4",
    "SPR_SCHABB_SHOOT1","SPR_SCHABB_SHOOT2",

    "SPR_SCHABB_DIE1","SPR_SCHABB_DIE2","SPR_SCHABB_DIE3","SPR_SCHABB_DEAD",
    "SPR_HYPO1","SPR_HYPO2","SPR_HYPO3","SPR_HYPO4",

//
// fake
//
    "SPR_FAKE_W1","SPR_FAKE_W2","SPR_FAKE_W3","SPR_FAKE_W4",
    "SPR_FAKE_SHOOT","SPR_FIRE1","SPR_FIRE2",

    "SPR_FAKE_DIE1","SPR_FAKE_DIE2","SPR_FAKE_DIE3","SPR_FAKE_DIE4",
    "SPR_FAKE_DIE5","SPR_FAKE_DEAD",

//
// hitler
//
    "SPR_MECHA_W1","SPR_MECHA_W2","SPR_MECHA_W3","SPR_MECHA_W4",
    "SPR_MECHA_SHOOT1","SPR_MECHA_SHOOT2","SPR_MECHA_SHOOT3","SPR_MECHA_DEAD",

    "SPR_MECHA_DIE1","SPR_MECHA_DIE2","SPR_MECHA_DIE3",

    "SPR_HITLER_W1","SPR_HITLER_W2","SPR_HITLER_W3","SPR_HITLER_W4",
    "SPR_HITLER_SHOOT1","SPR_HITLER_SHOOT2","SPR_HITLER_SHOOT3","SPR_HITLER_DEAD",

    "SPR_HITLER_DIE1","SPR_HITLER_DIE2","SPR_HITLER_DIE3","SPR_HITLER_DIE4",
    "SPR_HITLER_DIE5","SPR_HITLER_DIE6","SPR_HITLER_DIE7",

//
// giftmacher
//
    "SPR_GIFT_W1","SPR_GIFT_W2","SPR_GIFT_W3","SPR_GIFT_W4",
    "SPR_GIFT_SHOOT1","SPR_GIFT_SHOOT2",

    "SPR_GIFT_DIE1","SPR_GIFT_DIE2","SPR_GIFT_DIE3","SPR_GIFT_DEAD",
#endif
//
// "Rocket", smoke and small explosion
//
    "SPR_ROCKET_1","SPR_ROCKET_2","SPR_ROCKET_3","SPR_ROCKET_4",
    "SPR_ROCKET_5","SPR_ROCKET_6","SPR_ROCKET_7","SPR_ROCKET_8",

    "SPR_SMOKE_1","SPR_SMOKE_2","SPR_SMOKE_3","SPR_SMOKE_4",
    "SPR_BOOM_1","SPR_BOOM_2","SPR_BOOM_3",

//
// Angel of Death's DeathSparks(tm)
//
#ifdef SPEAR
    "SPR_HROCKET_1","SPR_HROCKET_2","SPR_HROCKET_3","SPR_HROCKET_4",
    "SPR_HROCKET_5","SPR_HROCKET_6","SPR_HROCKET_7","SPR_HROCKET_8",

    "SPR_HSMOKE_1","SPR_HSMOKE_2","SPR_HSMOKE_3","SPR_HSMOKE_4",
    "SPR_HBOOM_1","SPR_HBOOM_2","SPR_HBOOM_3",

    "SPR_SPARK1","SPR_SPARK2","SPR_SPARK3","SPR_SPARK4", // needle in vswap
#endif

#ifndef SPEAR
//
// gretel
//
    "SPR_GRETEL_W1","SPR_GRETEL_W2","SPR_GRETEL_W3","SPR_GRETEL_W4",
    "SPR_GRETEL_SHOOT1","SPR_GRETEL_SHOOT2","SPR_GRETEL_SHOOT3","SPR_GRETEL_DEAD",

    "SPR_GRETEL_DIE1","SPR_GRETEL_DIE2","SPR_GRETEL_DIE3",

//
// fat face
//
    "SPR_FAT_W1","SPR_FAT_W2","SPR_FAT_W3","SPR_FAT_W4",
    "SPR_FAT_SHOOT1","SPR_FAT_SHOOT2","SPR_FAT_SHOOT3","SPR_FAT_SHOOT4",

    "SPR_FAT_DIE1","SPR_FAT_DIE2","SPR_FAT_DIE3","SPR_FAT_DEAD",

//
// bj
//
    "SPR_BJ_W1",
    "SPR_BJ_W2","SPR_BJ_W3","SPR_BJ_W4",
    "SPR_BJ_JUMP1","SPR_BJ_JUMP2","SPR_BJ_JUMP3","SPR_BJ_JUMP4",
#else
//
// THESE ARE FOR 'SPEAR OF DESTINY'
//

//
// Trans Grosse
//
    "SPR_TRANS_W1","SPR_TRANS_W2","SPR_TRANS_W3","SPR_TRANS_W4",
    "SPR_TRANS_SHOOT1","SPR_TRANS_SHOOT2","SPR_TRANS_SHOOT3","SPR_TRANS_DEAD",

    "SPR_TRANS_DIE1","SPR_TRANS_DIE2","SPR_TRANS_DIE3",

//
// Wilhelm
//
    "SPR_WILL_W1","SPR_WILL_W2","SPR_WILL_W3","SPR_WILL_W4",
    "SPR_WILL_SHOOT1","SPR_WILL_SHOOT2","SPR_WILL_SHOOT3","SPR_WILL_SHOOT4",

    "SPR_WILL_DIE1","SPR_WILL_DIE2","SPR_WILL_DIE3","SPR_WILL_DEAD",

//
// UberMutant
//
    "SPR_UBER_W1","SPR_UBER_W2","SPR_UBER_W3","SPR_UBER_W4",
    "SPR_UBER_SHOOT1","SPR_UBER_SHOOT2","SPR_UBER_SHOOT3","SPR_UBER_SHOOT4",

    "SPR_UBER_DIE1","SPR_UBER_DIE2","SPR_UBER_DIE3","SPR_UBER_DIE4",
    "SPR_UBER_DEAD",

//
// Death Knight
//
    "SPR_DEATH_W1","SPR_DEATH_W2","SPR_DEATH_W3","SPR_DEATH_W4",
    "SPR_DEATH_SHOOT1","SPR_DEATH_SHOOT2","SPR_DEATH_SHOOT3","SPR_DEATH_SHOOT4",

    "SPR_DEATH_DIE1","SPR_DEATH_DIE2","SPR_DEATH_DIE3","SPR_DEATH_DIE4",
    "SPR_DEATH_DIE5","SPR_DEATH_DIE6","SPR_DEATH_DEAD",

//
// Ghost
//
    "SPR_SPECTRE_W1","SPR_SPECTRE_W2","SPR_SPECTRE_W3","SPR_SPECTRE_W4",
    "SPR_SPECTRE_F1","SPR_SPECTRE_F2","SPR_SPECTRE_F3","SPR_SPECTRE_F4",

//
// Angel of Death
//
    "SPR_ANGEL_W1","SPR_ANGEL_W2","SPR_ANGEL_W3","SPR_ANGEL_W4",
    "SPR_ANGEL_SHOOT1","SPR_ANGEL_SHOOT2","SPR_ANGEL_TIRED1","SPR_ANGEL_TIRED2",

    "SPR_ANGEL_DIE1","SPR_ANGEL_DIE2","SPR_ANGEL_DIE3","SPR_ANGEL_DIE4",
    "SPR_ANGEL_DIE5","SPR_ANGEL_DIE6","SPR_ANGEL_DIE7","SPR_ANGEL_DEAD",

#endif

//
// player attack frames
//
    "SPR_KNIFEREADY","SPR_KNIFEATK1","SPR_KNIFEATK2","SPR_KNIFEATK3",
    "SPR_KNIFEATK4",

    "SPR_PISTOLREADY","SPR_PISTOLATK1","SPR_PISTOLATK2","SPR_PISTOLATK3",
    "SPR_PISTOLATK4",

    "SPR_MACHINEGUNREADY","SPR_MACHINEGUNATK1","SPR_MACHINEGUNATK2","SPR_MACHINEGUNATK3",
    "SPR_MACHINEGUNATK4",

    "SPR_CHAINREADY","SPR_CHAINATK1","SPR_CHAINATK2","SPR_CHAINATK3",
    "SPR_CHAINATK4",
};

// Define directional 3d sprites in wl_act1.cpp (there are two examples)
// Make sure you have according entries in ScanInfoPlane in wl_game.cpp.


void Scale3DShaper(int x1, int x2, int shapenum, uint32_t flags, fixed ny1,
                   fixed ny2, fixed nx1, fixed nx2, VBuf_t vbuf, 
                   fixed obj_x, fixed obj_y, byte blend, int vert)
{
    t_compshape *shape;
    unsigned scale1;
    int dx,len,i;
    fixed height,dheight,height1,height2;
    int xpos[TEXTURESIZE+1];
    int slinex;
    fixed dxx=(ny2-ny1)<<8,dzz=(nx2-nx1)<<8;
    fixed dxa=0,dza=0;
    int tmpwallheight;
    int texture;

	shape = (t_compshape *) PM_GetSprite(shapenum);

    len=shape->rightpix-shape->leftpix+1;
    if(!len) return;

    ny1+=dxx>>9;
    nx1+=dzz>>9;

    dxa=-(dxx>>1),dza=-(dzz>>1);
    dxx>>=TEXTURESHIFT,dzz>>=TEXTURESHIFT;
    dxa+=shape->leftpix*dxx,dza+=shape->leftpix*dzz;

    xpos[0]=(int)((ny1+(dxa>>8))*scale/(nx1+(dza>>8))+centerx);
    height1 = heightnumerator/((nx1+(dza>>8))>>8);
    height=(((fixed)height1)<<12)+2048;

    for(i=1;i<=len;i++)
    {
        dxa+=dxx,dza+=dzz;
        xpos[i]=(int)((ny1+(dxa>>8))*scale/(nx1+(dza>>8))+centerx);
        if(xpos[i-1]>viewwidth) break;
    }
    len=i-1;
    dx = xpos[len] - xpos[0];
    if(!dx) return;

    height2 = heightnumerator/((nx1+(dza>>8))>>8);
    dheight=(((fixed)height2-(fixed)height1)<<12)/(fixed)dx;

    i=0;
    if(x2>viewwidth) x2=viewwidth;

    for(i=0;i<len;i++)
    {
        for(slinex=xpos[i];slinex<xpos[i+1] && slinex<x2;slinex++)
        {
            height+=dheight;
            if(slinex<0) continue;

            scale1=(unsigned)(height>>15);

            if(wallheight[slinex]<(height>>12) && scale1 /*&& scale1<=maxscale*/)
            {
                texture = (i + shape->leftpix) & (TEXTURESIZE - 1);
                lmsource = GetLightmap(
                    vert ? LMFACE_DOOR_VERT : LMFACE_DOOR_HORIZ,
                    texture, 0, 0, 0, 0);
                tmpwallheight = wallheight[slinex];

                wallheight[slinex] = (height >> 12);
                postx = slinex;
                ScalePost();

                wallheight[slinex] = tmpwallheight;
            }
        }
    }
}

void Scale3DShape(VBuf_t vbuf, statobj_t *ob, byte blend)
{
    fixed x,y;
    fixed nx1,nx2,ny1,ny2;
    int viewx1,viewx2;
    fixed diradd;
    fixed playx = viewx;
    fixed playy = viewy;
    int vert = 0;

    x = (ob->tilex << TILESHIFT) + 0x8000;
    y = (ob->tiley << TILESHIFT) + 0x8000;

    //
    // the following values for "diradd" aren't optimized yet
    // if you have problems with sprites being visible through wall edges
    // where they shouldn't, you can try to adjust these values and SIZEADD
    //

#define SIZEADD 1024

    switch(ob->flags & FL_DIR_POS_MASK)
    {
        case FL_DIR_POS_FW: diradd=0x7ff0+0x8000; break;
        case FL_DIR_POS_BW: diradd=-0x7ff0+0x8000; break;
        case FL_DIR_POS_MID: diradd=0x8000; break;
        default:
            Quit("Unknown directional 3d sprite position (shapenum = %i)", ob->shapenum);
    }

    if(ob->flags & FL_DIR_VERT_FLAG)     // vertical dir 3d sprite
    {
        fixed gy1,gy2,gx,gyt1,gyt2,gxt;
        //
        // translate point to view centered coordinates
        //
        gy1 = (((long)ob->tiley) << TILESHIFT)+0x8000-playy-0x8000L-SIZEADD;
        gy2 = gy1+0x10000L+2*SIZEADD;
        gx = (((long)ob->tilex) << TILESHIFT)+diradd-playx;

        //
        // calculate newx
        //
        gxt = FixedMul(gx,viewcos);
        gyt1 = FixedMul(gy1,viewsin);
        gyt2 = FixedMul(gy2,viewsin);
        nx1 = gxt-gyt1;
        nx2 = gxt-gyt2;

        //
        // calculate newy
        //
        gxt = FixedMul(gx,viewsin);
        gyt1 = FixedMul(gy1,viewcos);
        gyt2 = FixedMul(gy2,viewcos);
        ny1 = gyt1+gxt;
        ny2 = gyt2+gxt;

        xtile = ob->tilex;
        ytile = ob->tiley;
        xtilestep = (player->x > x) ? -1 : 1;
        vert = 1;
    }
    else                                    // horizontal dir 3d sprite
    {
        fixed gx1,gx2,gy,gxt1,gxt2,gyt;
        //
        // translate point to view centered coordinates
        //
        gx1 = (((long)ob->tilex) << TILESHIFT)+0x8000-playx-0x8000L-SIZEADD;
        gx2 = gx1+0x10000L+2*SIZEADD;
        gy = (((long)ob->tiley) << TILESHIFT)+diradd-playy;

        //
        // calculate newx
        //
        gxt1 = FixedMul(gx1,viewcos);
        gxt2 = FixedMul(gx2,viewcos);
        gyt = FixedMul(gy,viewsin);
        nx1 = gxt1-gyt;
        nx2 = gxt2-gyt;

        //
        // calculate newy
        //
        gxt1 = FixedMul(gx1,viewsin);
        gxt2 = FixedMul(gx2,viewsin);
        gyt = FixedMul(gy,viewcos);
        ny1 = gyt+gxt1;
        ny2 = gyt+gxt2;

        xtile = ob->tilex;
        ytile = ob->tiley;
        ytilestep = (player->y > y) ? -1 : 1;
        vert = 0;
    }

    if(nx1 < 0 || nx2 < 0) return;      // TODO: Clip on viewplane

    //
    // calculate perspective ratio
    //
    if(nx1>=0 && nx1<=1792) nx1=1792;
    if(nx1<0 && nx1>=-1792) nx1=-1792;
    if(nx2>=0 && nx2<=1792) nx2=1792;
    if(nx2<0 && nx2>=-1792) nx2=-1792;

    viewx1=(int)(centerx+ny1*scale/nx1);
    viewx2=(int)(centerx+ny2*scale/nx2);

    if(viewx2 < viewx1)
    {
        Scale3DShaper(viewx2,viewx1,ob->shapenum,ob->flags,
            ny2,ny1,nx2,nx1,vbuf,x,y,blend,vert);
    }
    else
    {
        Scale3DShaper(viewx1,viewx2,ob->shapenum,ob->flags,
            ny1,ny2,nx1,nx2,vbuf,x,y,blend,vert);
    }
}

void ShapeToImage(int shapenum, byte *image, bool flip, 
    t_compshape *shape)
{
    int x, y, i;
    word *cmdptr;
    byte *sprite;
    short *linecmds;

    memset(image, 0xff, TEXTURESIZE * TEXTURESIZE);

    if (shape == NULL)
    {
        shape = (t_compshape *)PM_GetSprite(shapenum);
    }
    sprite = (byte *)shape;

    cmdptr = shape->dataofs;
    for (x = shape->leftpix; x <= shape->rightpix; x++)
    {
        linecmds = (short *)(sprite + *cmdptr++);
        for (; *linecmds; linecmds += 3)
        {
            i = linecmds[2] / 2 + linecmds[1];
            for (y = linecmds[2] / 2; y < linecmds[0] / 2; y++, i++)
            {
                image[(flip ? TEXTURESIZE - 1 - x : x) * 64 + y] =
                    sprite[i];
            }
        }
    }
}

#ifdef NOT_TESTED
t_compshape *ImageToShape(byte *image)
{
    int i, j;
    int x, y;
    int tsz, tsh;
    byte col;
    int leftpix, rightpix;
    int starty, endy;
    t_compshape *shape;
    word *linecmds;
    byte *sprite;

    tsz = TEXTURESIZE;
    tsh = TEXTURESHIFT;

    for (x = 0; x < tsz; x++)
    {
        for (y = 0; y < tsz; y++)
        {
            col = image[(y << tsh) + x];
            if (col != 0xff)
            {
                break;
            }
        }
        if (y != tsz)
        {
            break;
        }
    }
    if (x == tsz)
    {
        return NULL;
    }
    shape->leftpix = leftpix = x;

    for (x = tsz - 1; x >= 0; x--)
    {
        for (y = 0; y < tsz; y++)
        {
            col = image[(y << tsh) + x];
            if (col != 0xff)
            {
                break;
            }
        }
        if (y != tsz)
        {
            rightpix = x;
            break;
        }
    }
    if (x == -1)
    {
        return NULL;
    }
    shape->rightpix = rightpix = x;

    shape = (t_compshape *)lwlib_CallocMany(byte, 
        sizeof(t_compshape) + (3 * tsz * sizeof(word)) + (tsz * tsz));

    sprite = (byte *)shape;
    linecmds = (word *)(sprite + sizeof(t_compshape));
    i = (byte *)(linecmds + 3 * tsz) - sprite;

    for (x = leftpix; x <= rightpix; x++)
    {
        shape->dataofs[x - leftpix] = (byte *)linecmds - (byte *)shape;

        for (y = 0; y < tsz; y++)
        {
            while(y < tsz)
            {
                col = image[(y << tsh) + x];
                if (col != 0xff)
                {
                    break;
                }
                y++;
            }
            if (y == tsz)
            {
                break;
            }
            starty = y;
            linecmds[1] = i - starty * 2;
            linecmds[2] = starty * 2;

            while (y < tsz)
            {
                col = image[(y << tsh) + x];
                if (col == 0xff)
                {
                    break;
                }
                else
                {
                    sprite[i++] = col;
                }
                y++;
            }
            y--;
            endy = y;
            linecmds[0] = endy * 2;

            linecmds += 3;
        }
    }

    return shape;
}
#else
t_compshape *BuildSprite(SDL_Surface *surf)
{
    byte pix;
    int x, y;
    int x1, x2;
    word leftpix, rightpix, spanpix, totalpix, prevpix;
    int total_linecmds;
    t_compshape *shape;
    short *linecmds;
    byte *data;
    int numbytes;
    word *cmdptr;
    byte *pixels;
    int starty, endy;
    byte *image;
    int pixels_offset;
    int linecmds_offset;

    assert(surf->w == TEXTURESIZE && surf->h == TEXTURESIZE);
    SDL_LockSurface(surf);
    image = (byte *)surf->pixels;

    leftpix = 0;
    for (x = 0; x < TEXTURESIZE; x++)
    {
        for (y = 0; y < TEXTURESIZE; y++)
        {
            pix = image[(y << TEXTURESHIFT) + x];
            if (pix != 0xff)
            {
                break;
            }
        }
        if (y != TEXTURESIZE)
        {
            leftpix = x;
            break;
        }
    }

    rightpix = -1;
    for (x = TEXTURESIZE - 1; x >= 0; x--)
    {
        for (y = 0; y < TEXTURESIZE; y++)
        {
            pix = image[(y << TEXTURESHIFT) + x];
            if (pix != 0xff)
            {
                break;
            }
        }
        if (y != TEXTURESIZE)
        {
            rightpix = x;
            break;
        }
    }

    totalpix = 0;
    total_linecmds = 0;
    for (x = leftpix; x <= rightpix; x++)
    {
        starty = endy = 0;
        prevpix = 0xff;
        for (y = 0; y < TEXTURESIZE; y++)
        {
            pix = image[(y << TEXTURESHIFT) + x];
            if (pix != 0xff)
            {
                totalpix++;
            }

            if (prevpix == 0xff && pix != 0xff)
            {
                starty = y;
            }
            else if (prevpix != 0xff && pix == 0xff)
            {
                endy = y;
            }
            else if (y == TEXTURESIZE - 1 && pix != 0xff)
            {
                endy = TEXTURESIZE;
            }

            if (endy > starty)
            {
                total_linecmds++;
                endy = starty = 0;
            }

            prevpix = pix;
        }
        total_linecmds++;
    }

    spanpix = rightpix - leftpix + 1;
    linecmds_offset = 4 + (spanpix * 2);
    pixels_offset = linecmds_offset + (total_linecmds * 6);
    numbytes = pixels_offset + totalpix;
    data = (byte *)malloc(numbytes);
    CHECKMALLOCRESULT(data);
    memset(data, 0, numbytes);

    shape = (t_compshape *)data;
    shape->leftpix = leftpix;
    shape->rightpix = rightpix;

    if (rightpix < leftpix)
    {
        return shape;
    }

    cmdptr = shape->dataofs;
    linecmds = (short *)(data + linecmds_offset);
    pixels = data + pixels_offset;

    for (x = leftpix; x <= rightpix; x++)
    {
        *cmdptr++ = (word)((uintptr_t)linecmds - (uintptr_t)data);

        starty = endy = 0;
        prevpix = 0xff;
        for (y = 0; y < TEXTURESIZE; y++)
        {
            pix = image[(y << TEXTURESHIFT) + x];
            if (pix != 0xff)
            {
                *pixels++ = pix;
            }

            if (prevpix == 0xff && pix != 0xff)
            {
                starty = y;
            }
            else if (prevpix != 0xff && pix == 0xff)
            {
                endy = y;
            }
            else if (y == TEXTURESIZE - 1 && pix != 0xff)
            {
                endy = TEXTURESIZE;
            }

            if (endy > starty)
            {
                linecmds[0] = 2 * endy;
                linecmds[1] = (word)((uintptr_t)pixels - 
                    (endy - starty) - (uintptr_t)data - starty);
                linecmds[2] = 2 * starty;
                linecmds += 3;

                endy = starty = 0;
            }

            prevpix = pix;
        }
        linecmds[0] = linecmds[1] = linecmds[2] = 0;
        linecmds += 3;
    }

    SDL_UnlockSurface(surf);
    return shape;
}
#endif

#endif
