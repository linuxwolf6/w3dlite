// ID_VL.C

#include <string.h>
#include "wl_def.h"
#pragma hdrstop

// Uncomment the following line, if you get destination out of bounds
// assertion errors and want to ignore them during debugging
//#define IGNORE_BAD_DEST

#ifdef IGNORE_BAD_DEST
#undef assert
#define assert(x) if(!(x)) return
#define assert_ret(x) if(!(x)) return 0
#else
#define assert_ret(x) assert(x)
#endif

boolean fullscreen = true;
boolean usedoublebuffering = true;

Screen_t screen = 
{
    NULL, // buf
    640, // w
    400, // h
    32, // bits
    0, // pitch
    false, // isDoubleBuffered
    { 0 }, // fakedblbuf
};

Screen_t fizzleStartScreen = { 0 };
Screen_t fizzleEndScreen = { 0 };

Screen_t fadeScreen = { 0 };
int fadeRed, fadeGreen, fadeBlue;

unsigned scaleFactor;

boolean	 screenfaded;
unsigned bordercolor;

SDL_Color palette1[256], palette2[256];
SDL_Color curpal[256];

int scxtrans, scytrans;

#define CASSERT(x) extern int ASSERT_COMPILE[((x) != 0) * 2 - 1];
#define RGB(r, g, b) {(r)*255/63, (g)*255/63, (b)*255/63, 0}

SDL_Color gamepal[]={
#ifdef SPEAR
    #include "sodpal.inc"
#else
    #include "wolfpal.inc"
#endif
};

CASSERT(lengthof(gamepal) == 256)

//===========================================================================


/*
=======================
=
= VL_Shutdown
=
=======================
*/

void	VL_Shutdown (void)
{
	//VL_SetTextMode ();
}

/*
=======================
=
= VL_CreateRGBSurfaceSoft
=
=======================
*/
static SDL_Surface *VL_CreateRGBSurfaceSoft(void)
{
    SDL_Surface *surf;

    surf = SDL_CreateRGBSurface(SDL_SWSURFACE, 
        screen.w, screen.h, screen.bits,
        screen.buf->format->Rmask,
        screen.buf->format->Gmask,
        screen.buf->format->Bmask,
        screen.buf->format->Amask);
    if (surf->pitch % 4 != 0)
    {
        printf("Unexpected surface pitch %i\n", surf->pitch);
        exit(1);
    }

    return surf;
}

/*
=======================
=
= VL_Screen_Create
=
=======================
*/
Screen_t VL_Screen_Create(void)
{
    Screen_t screen;

    memset(&screen, 0, sizeof(screen));
    screen.buf = VL_CreateRGBSurfaceSoft();
    screen.w = screen.buf->w;
    screen.h = screen.buf->h;
    screen.bits = screen.buf->format->BitsPerPixel;
    screen.isDoubleBuffered = false;
    screen.pitch = screen.buf->pitch / 4;

    return screen;
}


/*
=======================
=
= VL_SetVGAPlaneMode
=
=======================
*/

void	VL_SetVGAPlaneMode (void)
{
    int i, k;

#ifdef SPEAR
    SDL_WM_SetCaption("Spear of Destiny", NULL);
#else
    SDL_WM_SetCaption("Wolfenstein 3D", NULL);
#endif

    if(screen.bits == 0)
    {
        const SDL_VideoInfo *vidInfo = SDL_GetVideoInfo();
        screen.bits = vidInfo->vfmt->BitsPerPixel;
    }

    screen.buf = SDL_SetVideoMode(screen.w, screen.h, screen.bits,
          (usedoublebuffering ? SDL_HWSURFACE | SDL_DOUBLEBUF : 0) |
          (fullscreen ? SDL_FULLSCREEN : 0));
    if(!screen.buf)
    {
        printf("Unable to set %ix%ix%i video mode: %s\n", screen.w,
            screen.h, screen.bits, SDL_GetError());
        exit(1);
    }
    if (screen.buf->pitch % 4 != 0)
    {
        printf("Unexpected screen pitch %i\n", screen.buf->pitch);
        exit(1);
    }
    if((screen.buf->flags & SDL_DOUBLEBUF) != SDL_DOUBLEBUF)
    {
        screen.isDoubleBuffered = false;
        usedoublebuffering = false;
    }
    else
    {
        screen.isDoubleBuffered = true;
    }
    if (screen.bits != 32)
    {
        printf("True color RGBA is not supported\n");
        exit(1);
    }
    SDL_ShowCursor(SDL_DISABLE);

    SDL_SetColors(screen.buf, gamepal, 0, 256);
    memcpy(curpal, gamepal, sizeof(SDL_Color) * 256);
#ifdef USE_MIPMAPS
	BuildPalette
#endif

    screen.pitch = screen.buf->pitch / 4;

    if (param_fakedblbuf)
    {
        assert(!usedoublebuffering);

        screen.fakedblbuf.enabled = true;
        screen.fakedblbuf.phys = screen.buf;
        screen.fakedblbuf.cur = 0;
        for (i = 0; i < 2; i++)
        {
            screen.fakedblbuf.bufs[i] = VL_CreateRGBSurfaceSoft();
        }
        assert(screen.fakedblbuf.bufs[0]->pitch == 
            screen.fakedblbuf.bufs[1]->pitch);
        screen.fakedblbuf.pitch = screen.fakedblbuf.bufs[0]->pitch / 4;

        screen.buf = screen.fakedblbuf.bufs[0];
        screen.pitch = screen.fakedblbuf.pitch;
        screen.isDoubleBuffered = true;

        usedoublebuffering = true;
    }

    scaleFactor = screen.w/320;
    if(screen.h/200 < scaleFactor)
        scaleFactor = screen.h/200;

    pixelangle = (short *) malloc(screen.w * sizeof(short));
    CHECKMALLOCRESULT(pixelangle);
    wallheight = (int *) malloc(screen.w * sizeof(int));
    CHECKMALLOCRESULT(wallheight);

    fizzleStartScreen = VL_Screen_Create();
    fizzleEndScreen = VL_Screen_Create();
    fadeScreen = VL_Screen_Create();
}

/*
=============================================================================

						PALETTE OPS

		To avoid snow, do a WaitVBL BEFORE calling these

=============================================================================
*/

/*
=================
=
= VL_ConvertPalette
=
=================
*/

void VL_ConvertPalette(byte *srcpal, SDL_Color *destpal, int numColors)
{
    for(int i=0; i<numColors; i++)
    {
        destpal[i].r = *srcpal++ * 255 / 63;
        destpal[i].g = *srcpal++ * 255 / 63;
        destpal[i].b = *srcpal++ * 255 / 63;
    }
}

/*
=================
=
= VL_FillPalette
=
=================
*/

void VL_FillPalette (int red, int green, int blue)
{
    int i;
    SDL_Color pal[256];

    for(i=0; i<256; i++)
    {
        pal[i].r = red;
        pal[i].g = green;
        pal[i].b = blue;
    }

    VL_SetPalette(pal, true);
}

//===========================================================================

/*
=================
=
= VL_SetColor
=
=================
*/

void VL_SetColor	(int color, int red, int green, int blue)
{
    SDL_Color col = { red, green, blue };
    curpal[color] = col;
}

//===========================================================================

/*
=================
=
= VL_GetColor
=
=================
*/

void VL_GetColor	(int color, int *red, int *green, int *blue)
{
    SDL_Color *col = &curpal[color];
    *red = col->r;
    *green = col->g;
    *blue = col->b;
}

//===========================================================================

//===========================================================================

/*
=================
=
= VL_GetPalette
=
=================
*/

void VL_GetPalette (SDL_Color *palette)
{
    memcpy(palette, curpal, sizeof(SDL_Color) * 256);
}

/*
=================
=
= VL_SetPalette
=
=================
*/

void VL_SetPalette (SDL_Color *palette, bool forceupdate)
{
}

//===========================================================================

/*
=================
=
= VL_FadeOutPrepare
=
=================
*/
void VL_FadeOutPrepare(void)
{
    SDL_BlitSurface(screen.buf, NULL, fadeScreen.buf, NULL);
}


/*
=================
=
= VL_FadeOut
=
= Fades the current palette to the given color in the given number of steps
=
=================
*/

void VL_FadeOut (SDL_Color *pal, int red, int green, int blue, int steps)
{
    int shift;
    int i, off, y;
    uint32_t srcColor, dstColor;
    byte *srcPtr, *dstPtr;
    Screen_t swapScreen;

    if (screenfaded)
    {
        return;
    }

    red = red * 255 / 63;
    green = green * 255 / 63;
    blue = blue * 255 / 63;

    VBufInitFadeTable(red, green, blue);

    VL_WaitVBL(1);

    srcPtr = (byte *)&srcColor;
    dstPtr = (byte *)&dstColor;
    dstPtr[3] = 255;

    // fade through intermediate frames
    for (i = 0; i < steps && !param_nofade; i++)
    {
        VBuf_t vbuf = VBufLockSurface();
        VBuf_t vbufFade = VBufLockSurfaceEx(fadeScreen.buf);

        shift = i * 255 / (steps - 1);

		if ( vbuf.start )		// avoid crashes when app is in background mode
        for (y = 0; y < screen.h; y++)
        {
            for (off = 0; off < screen.w; off++)
            {
                srcColor = VBufReadColor(vbufFade, off);
                dstPtr[0] = VBufFadeTable[srcPtr[0]][shift][2];
                dstPtr[1] = VBufFadeTable[srcPtr[1]][shift][1];
                dstPtr[2] = VBufFadeTable[srcPtr[2]][shift][0];
                VBufWriteColor(vbuf, off, dstColor);
            }
            vbuf = VBufOffset(vbuf, screen.pitch);
            vbufFade = VBufOffset(vbufFade, fadeScreen.pitch);
        }

        VBufUnlockSurfaceEx(fadeScreen.buf);
        VBufUnlockSurface();
        VH_UpdateScreen();

        if (!usedoublebuffering || screen.bits == 8)
        {
            VL_WaitVBL(1);
        }
    }

    SWAP_VALS(screen, fadeScreen, swapScreen);
    fadeRed = red;
    fadeGreen = green;
    fadeBlue = blue;
    VBufSetLightTable(pal, 0, 0, 0);

    screenfaded = true;
}


/*
=================
=
= VL_FadeIn
=
=================
*/

void VL_FadeIn (int steps)
{
    int shift;
    int i, off, y;
    uint32_t srcColor, dstColor;
    byte *srcPtr, *dstPtr;
    Screen_t swapScreen;

    if (!screenfaded)
    {
        return;
    }

    SWAP_VALS(screen, fadeScreen, swapScreen);

    VBufInitFadeTable(fadeRed, fadeGreen, fadeBlue);

    VL_WaitVBL(1);

    srcPtr = (byte *)&srcColor;
    dstPtr = (byte *)&dstColor;
    dstPtr[3] = 255;

    // fade through intermediate frames
    for (i = 0; i < steps && !param_nofade; i++)
    {
        VBuf_t vbuf = VBufLockSurface();
        VBuf_t vbufFade = VBufLockSurfaceEx(fadeScreen.buf);

        shift = (steps - 1 - i) * 255 / (steps - 1);

		if ( vbuf.start )		// avoid crashes when app is in background mode
        for (y = 0; y < screen.h; y++)
        {
            for (off = 0; off < screen.w; off++)
            {
                srcColor = VBufReadColor(vbufFade, off);
                dstPtr[0] = VBufFadeTable[srcPtr[0]][shift][2];
                dstPtr[1] = VBufFadeTable[srcPtr[1]][shift][1];
                dstPtr[2] = VBufFadeTable[srcPtr[2]][shift][0];
                VBufWriteColor(vbuf, off, dstColor);
            }
            vbuf = VBufOffset(vbuf, screen.pitch);
            vbufFade = VBufOffset(vbufFade, fadeScreen.pitch);
        }

        VBufUnlockSurfaceEx(fadeScreen.buf);
        VBufUnlockSurface();
        VH_UpdateScreen();

        if (!usedoublebuffering || screen.bits == 8)
        {
            VL_WaitVBL(1);
        }
    }

    SDL_BlitSurface(fadeScreen.buf, NULL, screen.buf, NULL);
    VH_UpdateScreen();

    screenfaded = false;
}

/*
=============================================================================

							PIXEL OPS

=============================================================================
*/

byte *VL_LockSurface(SDL_Surface *surface)
{
    if(SDL_MUSTLOCK(surface))
    {
        if(SDL_LockSurface(surface) < 0)
            return NULL;
    }
    return (byte *) surface->pixels;
}

void VL_UnlockSurface(SDL_Surface *surface)
{
    if(SDL_MUSTLOCK(surface))
    {
        SDL_UnlockSurface(surface);
    }
}

/*
=================
=
= VL_Plot
=
=================
*/

void VL_Plot (int x, int y, int color)
{
    assert(x >= 0 && (unsigned) x < screen.w
            && y >= 0 && (unsigned) y < screen.h
            && "VL_Plot: Pixel out of bounds!");

    VBuf_t vbuf = VBufLockSurface();
    VBufWritePix2D(vbuf, y * screen.pitch + x, color);
    VBufUnlockSurface();
}


/*
=================
=
= VL_Hlin
=
=================
*/

void VL_Hlin (unsigned x, unsigned y, unsigned width, int color)
{
    assert(x >= 0 && x + width <= screen.w
            && y >= 0 && y < screen.h
            && "VL_Hlin: Destination rectangle out of bounds!");

    VBuf_t vbuf = VBufLockSurface();
    vbuf = VBufOffset(vbuf, y * screen.pitch + x);
    while (width--)
    {
        VBufWritePix2D(vbuf, 0, color);
        vbuf = VBufOffset(vbuf, 1);
    }
    VBufUnlockSurface();
}


/*
=================
=
= VL_Vlin
=
=================
*/

void VL_Vlin (int x, int y, int height, int color)
{
	assert(x >= 0 && (unsigned) x < screen.w
			&& y >= 0 && (unsigned) y + height <= screen.h
			&& "VL_Vlin: Destination rectangle out of bounds!");

	VBuf_t vbuf = VBufLockSurface();
	vbuf = VBufOffset(vbuf, y * screen.pitch + x);
	while (height--)
	{
		VBufWritePix2D(vbuf, 0, color);
		vbuf = VBufOffset(vbuf, screen.pitch);
	}
	VBufUnlockSurface();
}


/*
=================
=
= VL_Bar
=
=================
*/

void VL_BarScaledCoord (int scx, int scy, int scwidth, int scheight, int color)
{
	assert(scx >= 0 && (unsigned) scx + scwidth <= screen.w
			&& scy >= 0 && (unsigned) scy + scheight <= screen.h
			&& "VL_BarScaledCoord: Destination rectangle out of bounds!");

    int x;
	VBuf_t vbuf = VBufLockSurface();
	vbuf = VBufOffset(vbuf, scy * screen.pitch + scx);

	while (scheight--)
	{
		VBuf_t row = vbuf;
		for (x = 0; x < scwidth; x++)
		{
			VBufWritePix2D(vbuf, 0, color);
			vbuf = VBufOffset(vbuf, 1);
		}
		vbuf = VBufOffset(row, screen.pitch);
	}
	VBufUnlockSurface();
}

/*
============================================================================

							MEMORY OPS

============================================================================
*/

/*
=================
=
= VL_MemToLatch
=
=================
*/

void VL_MemToLatch(byte *source, int width, int height,
    SDL_Surface *destSurface, int x, int y)
{
    assert(x >= 0 && (unsigned) x + width <= screen.w
            && y >= 0 && (unsigned) y + height <= screen.h
            && "VL_MemToLatch: Destination rectangle out of bounds!");

    VL_LockSurface(destSurface);
    int pitch = destSurface->pitch;
    byte *dest = (byte *) destSurface->pixels + y * pitch + x;
    for(int ysrc = 0; ysrc < height; ysrc++)
    {
        for(int xsrc = 0; xsrc < width; xsrc++)
        {
            dest[ysrc * pitch + xsrc] = source[(ysrc * (width >> 2) + (xsrc >> 2))
                + (xsrc & 3) * (width >> 2) * height];
        }
    }
    VL_UnlockSurface(destSurface);
}

//===========================================================================


/*
=================
=
= VL_MemToScreenScaledCoord
=
= Draws a block of data to the screen with scaling according to scaleFactor.
=
=================
*/

void VL_MemToScreenScaledCoord (byte *source, int width, int height, int destx, int desty)
{
    assert(destx >= 0 && destx + width * scaleFactor <= screen.w
            && desty >= 0 && desty + height * scaleFactor <= screen.h
            && "VL_MemToScreenScaledCoord: Destination rectangle out of bounds!");

    VBuf_t vbuf = VBufLockSurface();
	if ( !vbuf.start )
		return;		// avoid crashes when app is in background mode
    for(int j=0,scj=0; j<height; j++, scj+=scaleFactor)
    {
        for(int i=0,sci=0; i<width; i++, sci+=scaleFactor)
        {
            byte col = source[(j*(width>>2)+(i>>2))+(i&3)*(width>>2)*height];
            for(unsigned m=0; m<scaleFactor; m++)
            {
                for(unsigned n=0; n<scaleFactor; n++)
                {
                    VBufWritePix2D(vbuf, (scj+m+desty)*screen.pitch+sci+n+destx, 
                        col);
                }
            }
        }
    }
    VBufUnlockSurface();
}

/*
=================
=
= VL_MemToScreenScaledCoord
=
= Draws a part of a block of data to the screen.
= The block has the size origwidth*origheight.
= The part at (srcx, srcy) has the size width*height
= and will be painted to (destx, desty) with scaling according to scaleFactor.
=
=================
*/

void VL_MemToScreenScaledCoord (byte *source, int origwidth, int origheight, int srcx, int srcy,
                                int destx, int desty, int width, int height)
{
    assert(destx >= 0 && destx + width * scaleFactor <= screen.w
            && desty >= 0 && desty + height * scaleFactor <= screen.h
            && "VL_MemToScreenScaledCoord: Destination rectangle out of bounds!");

    VBuf_t vbuf = VBufLockSurface();
    for(int j=0,scj=0; j<height; j++, scj+=scaleFactor)
    {
        for(int i=0,sci=0; i<width; i++, sci+=scaleFactor)
        {
            byte col = source[((j+srcy)*(origwidth>>2)+((i+srcx)>>2))+((i+srcx)&3)*(origwidth>>2)*origheight];
            for(unsigned m=0; m<scaleFactor; m++)
            {
                for(unsigned n=0; n<scaleFactor; n++)
                {
                    VBufWritePix2D(vbuf, (scj+m+desty)*screen.pitch+sci+n+destx, 
                        col);
                }
            }
        }
    }
    VBufUnlockSurface();
}

//==========================================================================

/*
=================
=
= VL_LatchToScreen
=
=================
*/

void VL_LatchToScreenScaledCoord(SDL_Surface *source, int xsrc, int ysrc,
    int width, int height, int scxdest, int scydest)
{
	scxdest += scxtrans;
	scydest += scytrans;

	assert(scxdest >= 0 && scxdest + width * scaleFactor <= screen.w
			&& scydest >= 0 && scydest + height * scaleFactor <= screen.h
			&& "VL_LatchToScreenScaledCoord: Destination rectangle out of bounds!");

	if(scaleFactor == 1)
    {
        VL_LockSurface(source);
        byte *src = (byte *) source->pixels;
        unsigned srcPitch = source->pitch;

        VBuf_t vbuf = VBufLockSurface();
		if ( !vbuf.start )
			return;			// avoid crashes when app in backround
        for(int j=0,scj=0; j<height; j++, scj++)
        {
            for(int i=0,sci=0; i<width; i++, sci++)
            {
                byte col = src[(ysrc + j)*srcPitch + xsrc + i];
                VBufWritePix2D(vbuf, (scydest+scj)*screen.pitch+scxdest+sci,
                    col);
            }
        }
        VBufUnlockSurface();
        VL_UnlockSurface(source);
    }
    else
    {
        VL_LockSurface(source);
        byte *src = (byte *) source->pixels;
        unsigned srcPitch = source->pitch;

        VBuf_t vbuf = VBufLockSurface();
		if ( !vbuf.start )
			return;			// avoid crashes when app in backround
        for(int j=0,scj=0; j<height; j++, scj+=scaleFactor)
        {
            for(int i=0,sci=0; i<width; i++, sci+=scaleFactor)
            {
                byte col = src[(ysrc + j)*srcPitch + xsrc + i];
                for(unsigned m=0; m<scaleFactor; m++)
                {
                    for(unsigned n=0; n<scaleFactor; n++)
                    {
                        VBufWritePix2D(vbuf, 
                            (scydest+scj+m)*screen.pitch+scxdest+sci+n,
                            col);
                    }
                }
            }
        }
        VBufUnlockSurface();
        VL_UnlockSurface(source);
    }
}

//===========================================================================

/*
=================
=
= VL_ScreenToScreen
=
=================
*/

void VL_ScreenToScreen (SDL_Surface *source, SDL_Surface *dest)
{
    SDL_BlitSurface(source, NULL, dest, NULL);
}
