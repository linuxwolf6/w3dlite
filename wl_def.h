#ifndef WL_DEF_H
#define WL_DEF_H

// Defines which version shall be built and configures supported extra features
#include "version.h"

#include <assert.h>
#include <fcntl.h>
#include <math.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#if defined(_arch_dreamcast)
#	include <string.h>
#	include "dc/dc_main.h"
#elif !defined(_WIN32)
#	include <stdint.h>
#	include <string.h>
#	include <stdarg.h>
#endif
#include <SDL.h>

#if !defined O_BINARY
#	define O_BINARY 0
#endif

#include "wolfrad.h"
#include "wr_lightinfo.h"
#include "lw_intmap.h"
#include "wl_ed.h"

// this pack pragma cannot move
// be careful when including headers after this line
#pragma pack(1)

#if defined(_arch_dreamcast)
#define YESBUTTONNAME "A"
#define NOBUTTONNAME  "B"
#elif defined(GP2X)
#define YESBUTTONNAME "Y"
#define NOBUTTONNAME  "B"
#else
#define YESBUTTONNAME "Y"
#define NOBUTTONNAME  "N"
#endif

#include "foreign.h"

#ifndef SPEAR
    #include "audiowl6.h"
    #ifdef UPLOAD
        #include "gfxv_apo.h"
    #else
        #ifdef GOODTIMES
            #include "gfxv_wl6.h"
        #else
            #include "gfxv_apo.h"
        #endif
    #endif
#else
    #include "audiosod.h"
    #include "gfxv_sod.h"
    #include "f_spear.h"
#endif
#include "fixedptc.h"

typedef uint8_t byte;
typedef uint16_t word;
typedef int32_t fixed;
typedef uint32_t longword;
typedef int8_t boolean;
typedef void * memptr;

typedef struct
{
    int x,y;
} Point;
typedef struct
{
    Point ul,lr;
} Rect;

void Quit(const char *errorStr, ...);

#include "id_pm.h"
#include "id_sd.h"
#include "id_in.h"
#include "id_vl.h"
#include "id_vh.h"
#include "id_us.h"
#include "id_ca.h"

#include "wl_menu.h"

#define MAPSPOT(x,y,plane) (mapsegs[plane][((y)<<mapshift)+(x)])

#define SIGN(x)         ((x)>0?1:-1)
#define ABS(x)          ((int)(x)>0?(x):-(x))
#define LABS(x)         ((int32_t)(x)>0?(x):-(x))

#define abs(x) ABS(x)

#ifndef MIN
#define MIN(x,y) ((x)<(y)?(x):(y))
#endif

#ifndef MAX
#define MAX(x,y) ((x)>(y)?(x):(y))
#endif

#ifndef CLIP
#define CLIP(x,a,b) MIN(MAX(x,a),b)
#endif

#define SWAP_VALS(x,y,t) \
do { \
    t = x; \
    x = y; \
    y = t; \
} while(0)

#define TILEDIST(tx1,ty1,tx2,ty2) \
    ((int)abs((tx1) - (tx2)) > abs((ty1) - (ty2)) ? \
        abs((tx1) - (tx2)) : abs((ty1) - (ty2)))
#define TICS2MILLI(tics) ((tics) * 100 / WOLFTICRATE_DEFAULT)
#define TICS2SECFP(tics) (FP(TICS2MILLI(tics)) / 1000)

#define MILLI2TICS(milli) ((milli) * WOLFTICRATE_DEFAULT / 100)
#define SECS2TICS(secs) MILLI2TICS(secs * 1000)

/*
=============================================================================

                            GLOBAL CONSTANTS

=============================================================================
*/

#define MAXTICS 10
#define DEMOTICS        4

#define MAXACTORS       2500        // max number of nazis, etc / map
#define MAXSTATS        1500        // max number of lamps, bonus, etc
#define MAXSLIDINGDOORS 64          // max number of sliding doors
#define MAXDOORS        256         // max number of doors including push wall doors
#define MAXWALLTILES    128         // max number of wall tiles

#define DOORPAGE_ABSOLUTE(x)   ((PMSpriteStart - 8) + ((x) * 2))

//
// tile constants
//

#define ICONARROWS      90
#define PUSHABLETILE    98
#define EXITTILE        99          // at end of castle
#define AREATILE        107         // first of NUMAREAS floor tiles
#define NUMAREAS        37
#define ELEVATORTILE    21
#define AMBUSHTILE      106
#define ALTELEVATORTILE 107
#define PUSHMOVETILE    64

#define NUMBERCHARS     9

#ifdef ENDOBJ
#define EXITLEVEL       105
#endif

#define BLOCKINGTILE    127

//----------------

#define EXTRAPOINTS     40000

#define PLAYERSPEED     3000
#define RUNSPEED        6000

#define SCREENSEG       0xa000

#define SCREENBWIDE     80

#define HEIGHTRATIO     0.50            // also defined in id_mm.c

#define BORDERCOLOR     3
#define FLASHCOLOR      5
#define FLASHTICS       4

#ifndef SPEAR
    #define LRpack      8       // # of levels to store in endgame
#else
    #define LRpack      20
#endif

#define PLAYERSIZE      MINDIST         // player radius
#define MINACTORDIST    0x10000l        // minimum dist from player center
                                        // to any actor center

#define NUMLATCHPICS    100

#undef M_PI
#define PI              3.141592657
#define M_PI PI

#define GLOBAL1         (1l<<16)
#define TILEGLOBAL      GLOBAL1
#define PIXGLOBAL       (GLOBAL1/64)
#define HALFTILE        (TILEGLOBAL / 2)
#define TILESHIFT       16l
#define UNSIGNEDSHIFT   8
#define TILE2POS(x)     (((x) << TILESHIFT) + 0x8000)

#define ANGLES          360             // must be divisable by 4
#define ANGLEQUAD       (ANGLES/4)
#define FINEANGLES      3600
#define ANG90           (FINEANGLES/4)
#define ANG180          (ANG90*2)
#define ANG270          (ANG90*3)
#define ANG360          (ANG90*4)
#define VANG90          (ANGLES/4)
#define VANG180         (VANG90*2)
#define VANG270         (VANG90*3)
#define VANG360         (VANG90*4)

#define MINDIST         (0x5800l)

#define mapshift        6
#define MAPSIZE         (1<<mapshift)
#define maparea         MAPSIZE*MAPSIZE

#define mapheight       MAPSIZE
#define mapwidth        MAPSIZE

#define LT_MAPSIZE      MAPSIZE

#ifdef USE_HIRES

#define TEXTURESHIFT    7
#define TEXTURESIZE     (1<<TEXTURESHIFT)
#define TEXTUREFROMFIXEDSHIFT 2
#define TEXTUREMASK     (TEXTURESIZE*(TEXTURESIZE-1))

#define SPRITESCALEFACTOR 1

#else

#define TEXTURESHIFT    6
#define TEXTURESIZE     (1<<TEXTURESHIFT)
#define TEXTUREFROMFIXEDSHIFT 4
#define TEXTUREMASK     (TEXTURESIZE*(TEXTURESIZE-1))

#define SPRITESCALEFACTOR 2

#endif

#define TEXTUREPIXELS (TEXTURESIZE * TEXTURESIZE)

#define LIGHTTILE_RES_LOG2     6
#define LIGHTTILE_RES          (1 << LIGHTTILE_RES_LOG2)
#define LIGHTTILESHIFT         (TILESHIFT - LIGHTTILE_RES_LOG2)
#define LIGHTTILE_COORD(x)     ((x) >> LIGHTTILESHIFT)
#define LIGHTMAPSIZE           (1 << (mapshift + LIGHTTILE_RES_LOG2))
#define LIGHTTILE_IN_MAP(x, y) (((x) >= 0 && (x) < LIGHTMAPSIZE) && ((y) >= 0 && (y) < LIGHTMAPSIZE))
#define LIGHTTILE_CENTER(x)    (((x) << LIGHTTILESHIFT) + (1 << (LIGHTTILESHIFT - 1)))
#define LIGHTTILE_TO_TILE(x)   ((x) >> LIGHTTILE_RES_LOG2)

#define LT_DISTTABLE_REACH  16
#define LT_DISTTABLE_WIDTH  \
    ((LT_DISTTABLE_REACH / 2) * LIGHTTILE_RES)
#define LT_DISTTABLE_SIZE   \
    (LT_DISTTABLE_WIDTH * LT_DISTTABLE_WIDTH)

#define SPOT_FROMTILE(x, y) (((y) << mapshift) + (x))

#define SPOT_TOTILEX(spot) (((spot) >> 0) & (MAPSIZE - 1))
#define SPOT_TOTILEY(spot) (((spot) >> mapshift) & (MAPSIZE - 1))

#define NORTH   0
#define EAST    1
#define SOUTH   2
#define WEST    3


#define STATUSLINES     40

#define SCREENSIZE      (SCREENBWIDE*208)
#define PAGE1START      0
#define PAGE2START      (SCREENSIZE)
#define PAGE3START      (SCREENSIZE*2u)
#define FREESTART       (SCREENSIZE*3u)


#define PIXRADIUS       512

#define STARTAMMO       8

#ifdef RGB_LSB_FIRST
#define RGBA(r,g,b,a) \
    (((a) << 24) + ((r) << 0) + ((g) << 8) + ((b) << 16))
#else
#define RGBA(r,g,b,a) \
    (((a) << 24) + ((r) << 16) + ((g) << 8) + ((b)))
#endif

#define RGBA_FROMPAL(pal,i) RGBA((pal)[i].r, (pal)[i].g, (pal)[i].b, 255)

#define RGBA_GREEN RGBA(0,255,0,255)
#define RGBA_GREEN_DARKER RGBA(0,255 / 8,0,255)

#define RGBA_ORANGE RGBA(255,165,0,255)
#define RGBA_ORANGE_DARKER RGBA(255 / 8,165 / 8,0,255)

#define RGBA_SLATEBLUE RGBA(0,165,255,255)
#define RGBA_SLATEBLUE_DARKER RGBA(0,165 / 8,255 / 8,255)

#define RGBA_RED RGBA(255,0,0,255)
#define RGBA_RED_DARKER RGBA(255 / 8,0,0,255)

#define RGBA_BLACK RGBA(0,0,0,255)
#define RGBA_GOLD RGBA(255,215,0,255)

// object flag values

typedef enum
{
    FL_NONE             = 0x00000000,
    FL_SHOOTABLE        = 0x00000001,
    FL_BONUS            = 0x00000002,
    FL_NEVERMARK        = 0x00000004,
    FL_VISABLE          = 0x00000008,
    FL_ATTACKMODE       = 0x00000010,
    FL_FIRSTATTACK      = 0x00000020,
    FL_AMBUSH           = 0x00000040,
    FL_NONMARK          = 0x00000080,
    FL_FULLBRIGHT       = 0x00000100,
#ifdef USE_DIR3DSPR
    // you can choose one of the following values in wl_act1.cpp
    // to make a static sprite a directional 3d sprite
    // (see example at the end of the statinfo array)
    FL_DIR_HORIZ_MID    = 0x00000200,
    FL_DIR_HORIZ_FW     = 0x00000400,
    FL_DIR_HORIZ_BW     = 0x00000600,
    FL_DIR_VERT_MID     = 0x00000a00,
    FL_DIR_VERT_FW      = 0x00000c00,
    FL_DIR_VERT_BW      = 0x00000e00,

    // these values are just used to improve readability of code
    FL_DIR_NONE         = 0x00000000,
    FL_DIR_POS_MID      = 0x00000200,
    FL_DIR_POS_FW       = 0x00000400,
    FL_DIR_POS_BW       = 0x00000600,
    FL_DIR_POS_MASK     = 0x00000600,
    FL_DIR_VERT_FLAG    = 0x00000800,
    FL_DIR_MASK         = 0x00000e00,
#endif
    FL_BLOCKING         = 0x00001000,
    FL_BLOCKLINE        = 0x00002000,
    // next free bit is   0x00004000
} objflag_t;

#define CHECK_FL_HORIZ_MID(flg) \
    ( \
        ((flg) & FL_DIR_POS_MID) != 0 && \
        ((flg) & FL_DIR_VERT_FLAG) == 0 \
    )

#define CHECK_FL_VERT_MID(flg) \
    ( \
        ((flg) & FL_DIR_POS_MID) != 0 && \
        ((flg) & FL_DIR_VERT_FLAG) != 0 \
    )

//
// sprite constants
//

enum
{
    SPR_DEMO,
#ifndef APOGEE_1_0
    SPR_DEATHCAM,
#endif
//
// static sprites
//
    SPR_STAT_0,SPR_STAT_1,SPR_STAT_2,SPR_STAT_3,
    SPR_STAT_4,SPR_STAT_5,SPR_STAT_6,SPR_STAT_7,

    SPR_STAT_8,SPR_STAT_9,SPR_STAT_10,SPR_STAT_11,
    SPR_STAT_12,SPR_STAT_13,SPR_STAT_14,SPR_STAT_15,

    SPR_STAT_16,SPR_STAT_17,SPR_STAT_18,SPR_STAT_19,
    SPR_STAT_20,SPR_STAT_21,SPR_STAT_22,SPR_STAT_23,

    SPR_STAT_24,SPR_STAT_25,SPR_STAT_26,SPR_STAT_27,
    SPR_STAT_28,SPR_STAT_29,SPR_STAT_30,SPR_STAT_31,

    SPR_STAT_32,SPR_STAT_33,SPR_STAT_34,SPR_STAT_35,
    SPR_STAT_36,SPR_STAT_37,SPR_STAT_38,SPR_STAT_39,

    SPR_STAT_40,SPR_STAT_41,SPR_STAT_42,SPR_STAT_43,
    SPR_STAT_44,SPR_STAT_45,SPR_STAT_46,SPR_STAT_47,

#ifdef SPEAR
    SPR_STAT_48,SPR_STAT_49,SPR_STAT_50,SPR_STAT_51,
#endif

//
// guard
//
    SPR_GRD_S_1,SPR_GRD_S_2,SPR_GRD_S_3,SPR_GRD_S_4,
    SPR_GRD_S_5,SPR_GRD_S_6,SPR_GRD_S_7,SPR_GRD_S_8,

    SPR_GRD_W1_1,SPR_GRD_W1_2,SPR_GRD_W1_3,SPR_GRD_W1_4,
    SPR_GRD_W1_5,SPR_GRD_W1_6,SPR_GRD_W1_7,SPR_GRD_W1_8,

    SPR_GRD_W2_1,SPR_GRD_W2_2,SPR_GRD_W2_3,SPR_GRD_W2_4,
    SPR_GRD_W2_5,SPR_GRD_W2_6,SPR_GRD_W2_7,SPR_GRD_W2_8,

    SPR_GRD_W3_1,SPR_GRD_W3_2,SPR_GRD_W3_3,SPR_GRD_W3_4,
    SPR_GRD_W3_5,SPR_GRD_W3_6,SPR_GRD_W3_7,SPR_GRD_W3_8,

    SPR_GRD_W4_1,SPR_GRD_W4_2,SPR_GRD_W4_3,SPR_GRD_W4_4,
    SPR_GRD_W4_5,SPR_GRD_W4_6,SPR_GRD_W4_7,SPR_GRD_W4_8,

    SPR_GRD_PAIN_1,SPR_GRD_DIE_1,SPR_GRD_DIE_2,SPR_GRD_DIE_3,
    SPR_GRD_PAIN_2,SPR_GRD_DEAD,

    SPR_GRD_SHOOT1,SPR_GRD_SHOOT2,SPR_GRD_SHOOT3,

//
// dogs
//
    SPR_DOG_W1_1,SPR_DOG_W1_2,SPR_DOG_W1_3,SPR_DOG_W1_4,
    SPR_DOG_W1_5,SPR_DOG_W1_6,SPR_DOG_W1_7,SPR_DOG_W1_8,

    SPR_DOG_W2_1,SPR_DOG_W2_2,SPR_DOG_W2_3,SPR_DOG_W2_4,
    SPR_DOG_W2_5,SPR_DOG_W2_6,SPR_DOG_W2_7,SPR_DOG_W2_8,

    SPR_DOG_W3_1,SPR_DOG_W3_2,SPR_DOG_W3_3,SPR_DOG_W3_4,
    SPR_DOG_W3_5,SPR_DOG_W3_6,SPR_DOG_W3_7,SPR_DOG_W3_8,

    SPR_DOG_W4_1,SPR_DOG_W4_2,SPR_DOG_W4_3,SPR_DOG_W4_4,
    SPR_DOG_W4_5,SPR_DOG_W4_6,SPR_DOG_W4_7,SPR_DOG_W4_8,

    SPR_DOG_DIE_1,SPR_DOG_DIE_2,SPR_DOG_DIE_3,SPR_DOG_DEAD,
    SPR_DOG_JUMP1,SPR_DOG_JUMP2,SPR_DOG_JUMP3,



//
// ss
//
    SPR_SS_S_1,SPR_SS_S_2,SPR_SS_S_3,SPR_SS_S_4,
    SPR_SS_S_5,SPR_SS_S_6,SPR_SS_S_7,SPR_SS_S_8,

    SPR_SS_W1_1,SPR_SS_W1_2,SPR_SS_W1_3,SPR_SS_W1_4,
    SPR_SS_W1_5,SPR_SS_W1_6,SPR_SS_W1_7,SPR_SS_W1_8,

    SPR_SS_W2_1,SPR_SS_W2_2,SPR_SS_W2_3,SPR_SS_W2_4,
    SPR_SS_W2_5,SPR_SS_W2_6,SPR_SS_W2_7,SPR_SS_W2_8,

    SPR_SS_W3_1,SPR_SS_W3_2,SPR_SS_W3_3,SPR_SS_W3_4,
    SPR_SS_W3_5,SPR_SS_W3_6,SPR_SS_W3_7,SPR_SS_W3_8,

    SPR_SS_W4_1,SPR_SS_W4_2,SPR_SS_W4_3,SPR_SS_W4_4,
    SPR_SS_W4_5,SPR_SS_W4_6,SPR_SS_W4_7,SPR_SS_W4_8,

    SPR_SS_PAIN_1,SPR_SS_DIE_1,SPR_SS_DIE_2,SPR_SS_DIE_3,
    SPR_SS_PAIN_2,SPR_SS_DEAD,

    SPR_SS_SHOOT1,SPR_SS_SHOOT2,SPR_SS_SHOOT3,

//
// mutant
//
    SPR_MUT_S_1,SPR_MUT_S_2,SPR_MUT_S_3,SPR_MUT_S_4,
    SPR_MUT_S_5,SPR_MUT_S_6,SPR_MUT_S_7,SPR_MUT_S_8,

    SPR_MUT_W1_1,SPR_MUT_W1_2,SPR_MUT_W1_3,SPR_MUT_W1_4,
    SPR_MUT_W1_5,SPR_MUT_W1_6,SPR_MUT_W1_7,SPR_MUT_W1_8,

    SPR_MUT_W2_1,SPR_MUT_W2_2,SPR_MUT_W2_3,SPR_MUT_W2_4,
    SPR_MUT_W2_5,SPR_MUT_W2_6,SPR_MUT_W2_7,SPR_MUT_W2_8,

    SPR_MUT_W3_1,SPR_MUT_W3_2,SPR_MUT_W3_3,SPR_MUT_W3_4,
    SPR_MUT_W3_5,SPR_MUT_W3_6,SPR_MUT_W3_7,SPR_MUT_W3_8,

    SPR_MUT_W4_1,SPR_MUT_W4_2,SPR_MUT_W4_3,SPR_MUT_W4_4,
    SPR_MUT_W4_5,SPR_MUT_W4_6,SPR_MUT_W4_7,SPR_MUT_W4_8,

    SPR_MUT_PAIN_1,SPR_MUT_DIE_1,SPR_MUT_DIE_2,SPR_MUT_DIE_3,
    SPR_MUT_PAIN_2,SPR_MUT_DIE_4,SPR_MUT_DEAD,

    SPR_MUT_SHOOT1,SPR_MUT_SHOOT2,SPR_MUT_SHOOT3,SPR_MUT_SHOOT4,

//
// officer
//
    SPR_OFC_S_1,SPR_OFC_S_2,SPR_OFC_S_3,SPR_OFC_S_4,
    SPR_OFC_S_5,SPR_OFC_S_6,SPR_OFC_S_7,SPR_OFC_S_8,

    SPR_OFC_W1_1,SPR_OFC_W1_2,SPR_OFC_W1_3,SPR_OFC_W1_4,
    SPR_OFC_W1_5,SPR_OFC_W1_6,SPR_OFC_W1_7,SPR_OFC_W1_8,

    SPR_OFC_W2_1,SPR_OFC_W2_2,SPR_OFC_W2_3,SPR_OFC_W2_4,
    SPR_OFC_W2_5,SPR_OFC_W2_6,SPR_OFC_W2_7,SPR_OFC_W2_8,

    SPR_OFC_W3_1,SPR_OFC_W3_2,SPR_OFC_W3_3,SPR_OFC_W3_4,
    SPR_OFC_W3_5,SPR_OFC_W3_6,SPR_OFC_W3_7,SPR_OFC_W3_8,

    SPR_OFC_W4_1,SPR_OFC_W4_2,SPR_OFC_W4_3,SPR_OFC_W4_4,
    SPR_OFC_W4_5,SPR_OFC_W4_6,SPR_OFC_W4_7,SPR_OFC_W4_8,

    SPR_OFC_PAIN_1,SPR_OFC_DIE_1,SPR_OFC_DIE_2,SPR_OFC_DIE_3,
    SPR_OFC_PAIN_2,SPR_OFC_DIE_4,SPR_OFC_DEAD,

    SPR_OFC_SHOOT1,SPR_OFC_SHOOT2,SPR_OFC_SHOOT3,

#ifndef SPEAR
//
// ghosts
//
    SPR_BLINKY_W1,SPR_BLINKY_W2,SPR_PINKY_W1,SPR_PINKY_W2,
    SPR_CLYDE_W1,SPR_CLYDE_W2,SPR_INKY_W1,SPR_INKY_W2,

//
// hans
//
    SPR_BOSS_W1,SPR_BOSS_W2,SPR_BOSS_W3,SPR_BOSS_W4,
    SPR_BOSS_SHOOT1,SPR_BOSS_SHOOT2,SPR_BOSS_SHOOT3,SPR_BOSS_DEAD,

    SPR_BOSS_DIE1,SPR_BOSS_DIE2,SPR_BOSS_DIE3,

//
// schabbs
//
    SPR_SCHABB_W1,SPR_SCHABB_W2,SPR_SCHABB_W3,SPR_SCHABB_W4,
    SPR_SCHABB_SHOOT1,SPR_SCHABB_SHOOT2,

    SPR_SCHABB_DIE1,SPR_SCHABB_DIE2,SPR_SCHABB_DIE3,SPR_SCHABB_DEAD,
    SPR_HYPO1,SPR_HYPO2,SPR_HYPO3,SPR_HYPO4,

//
// fake
//
    SPR_FAKE_W1,SPR_FAKE_W2,SPR_FAKE_W3,SPR_FAKE_W4,
    SPR_FAKE_SHOOT,SPR_FIRE1,SPR_FIRE2,

    SPR_FAKE_DIE1,SPR_FAKE_DIE2,SPR_FAKE_DIE3,SPR_FAKE_DIE4,
    SPR_FAKE_DIE5,SPR_FAKE_DEAD,

//
// hitler
//
    SPR_MECHA_W1,SPR_MECHA_W2,SPR_MECHA_W3,SPR_MECHA_W4,
    SPR_MECHA_SHOOT1,SPR_MECHA_SHOOT2,SPR_MECHA_SHOOT3,SPR_MECHA_DEAD,

    SPR_MECHA_DIE1,SPR_MECHA_DIE2,SPR_MECHA_DIE3,

    SPR_HITLER_W1,SPR_HITLER_W2,SPR_HITLER_W3,SPR_HITLER_W4,
    SPR_HITLER_SHOOT1,SPR_HITLER_SHOOT2,SPR_HITLER_SHOOT3,SPR_HITLER_DEAD,

    SPR_HITLER_DIE1,SPR_HITLER_DIE2,SPR_HITLER_DIE3,SPR_HITLER_DIE4,
    SPR_HITLER_DIE5,SPR_HITLER_DIE6,SPR_HITLER_DIE7,

//
// giftmacher
//
    SPR_GIFT_W1,SPR_GIFT_W2,SPR_GIFT_W3,SPR_GIFT_W4,
    SPR_GIFT_SHOOT1,SPR_GIFT_SHOOT2,

    SPR_GIFT_DIE1,SPR_GIFT_DIE2,SPR_GIFT_DIE3,SPR_GIFT_DEAD,
#endif
//
// Rocket, smoke and small explosion
//
    SPR_ROCKET_1,SPR_ROCKET_2,SPR_ROCKET_3,SPR_ROCKET_4,
    SPR_ROCKET_5,SPR_ROCKET_6,SPR_ROCKET_7,SPR_ROCKET_8,

    SPR_SMOKE_1,SPR_SMOKE_2,SPR_SMOKE_3,SPR_SMOKE_4,
    SPR_BOOM_1,SPR_BOOM_2,SPR_BOOM_3,

//
// Angel of Death's DeathSparks(tm)
//
#ifdef SPEAR
    SPR_HROCKET_1,SPR_HROCKET_2,SPR_HROCKET_3,SPR_HROCKET_4,
    SPR_HROCKET_5,SPR_HROCKET_6,SPR_HROCKET_7,SPR_HROCKET_8,

    SPR_HSMOKE_1,SPR_HSMOKE_2,SPR_HSMOKE_3,SPR_HSMOKE_4,
    SPR_HBOOM_1,SPR_HBOOM_2,SPR_HBOOM_3,

    SPR_SPARK1,SPR_SPARK2,SPR_SPARK3,SPR_SPARK4,
#endif

#ifndef SPEAR
//
// gretel
//
    SPR_GRETEL_W1,SPR_GRETEL_W2,SPR_GRETEL_W3,SPR_GRETEL_W4,
    SPR_GRETEL_SHOOT1,SPR_GRETEL_SHOOT2,SPR_GRETEL_SHOOT3,SPR_GRETEL_DEAD,

    SPR_GRETEL_DIE1,SPR_GRETEL_DIE2,SPR_GRETEL_DIE3,

//
// fat face
//
    SPR_FAT_W1,SPR_FAT_W2,SPR_FAT_W3,SPR_FAT_W4,
    SPR_FAT_SHOOT1,SPR_FAT_SHOOT2,SPR_FAT_SHOOT3,SPR_FAT_SHOOT4,

    SPR_FAT_DIE1,SPR_FAT_DIE2,SPR_FAT_DIE3,SPR_FAT_DEAD,

//
// bj
//
#ifdef APOGEE_1_0
    SPR_BJ_W1=360,
#elif defined(APOGEE_1_1) && defined(UPLOAD)
    SPR_BJ_W1=406,
#else
    SPR_BJ_W1,
#endif
    SPR_BJ_W2,SPR_BJ_W3,SPR_BJ_W4,
    SPR_BJ_JUMP1,SPR_BJ_JUMP2,SPR_BJ_JUMP3,SPR_BJ_JUMP4,
#else
//
// THESE ARE FOR 'SPEAR OF DESTINY'
//

//
// Trans Grosse
//
    SPR_TRANS_W1,SPR_TRANS_W2,SPR_TRANS_W3,SPR_TRANS_W4,
    SPR_TRANS_SHOOT1,SPR_TRANS_SHOOT2,SPR_TRANS_SHOOT3,SPR_TRANS_DEAD,

    SPR_TRANS_DIE1,SPR_TRANS_DIE2,SPR_TRANS_DIE3,

//
// Wilhelm
//
    SPR_WILL_W1,SPR_WILL_W2,SPR_WILL_W3,SPR_WILL_W4,
    SPR_WILL_SHOOT1,SPR_WILL_SHOOT2,SPR_WILL_SHOOT3,SPR_WILL_SHOOT4,

    SPR_WILL_DIE1,SPR_WILL_DIE2,SPR_WILL_DIE3,SPR_WILL_DEAD,

//
// UberMutant
//
    SPR_UBER_W1,SPR_UBER_W2,SPR_UBER_W3,SPR_UBER_W4,
    SPR_UBER_SHOOT1,SPR_UBER_SHOOT2,SPR_UBER_SHOOT3,SPR_UBER_SHOOT4,

    SPR_UBER_DIE1,SPR_UBER_DIE2,SPR_UBER_DIE3,SPR_UBER_DIE4,
    SPR_UBER_DEAD,

//
// Death Knight
//
    SPR_DEATH_W1,SPR_DEATH_W2,SPR_DEATH_W3,SPR_DEATH_W4,
    SPR_DEATH_SHOOT1,SPR_DEATH_SHOOT2,SPR_DEATH_SHOOT3,SPR_DEATH_SHOOT4,

    SPR_DEATH_DIE1,SPR_DEATH_DIE2,SPR_DEATH_DIE3,SPR_DEATH_DIE4,
    SPR_DEATH_DIE5,SPR_DEATH_DIE6,SPR_DEATH_DEAD,

//
// Ghost
//
    SPR_SPECTRE_W1,SPR_SPECTRE_W2,SPR_SPECTRE_W3,SPR_SPECTRE_W4,
    SPR_SPECTRE_F1,SPR_SPECTRE_F2,SPR_SPECTRE_F3,SPR_SPECTRE_F4,

//
// Angel of Death
//
    SPR_ANGEL_W1,SPR_ANGEL_W2,SPR_ANGEL_W3,SPR_ANGEL_W4,
    SPR_ANGEL_SHOOT1,SPR_ANGEL_SHOOT2,SPR_ANGEL_TIRED1,SPR_ANGEL_TIRED2,

    SPR_ANGEL_DIE1,SPR_ANGEL_DIE2,SPR_ANGEL_DIE3,SPR_ANGEL_DIE4,
    SPR_ANGEL_DIE5,SPR_ANGEL_DIE6,SPR_ANGEL_DIE7,SPR_ANGEL_DEAD,

#endif

//
// player attack frames
//
    SPR_KNIFEREADY,SPR_KNIFEATK1,SPR_KNIFEATK2,SPR_KNIFEATK3,
    SPR_KNIFEATK4,

    SPR_PISTOLREADY,SPR_PISTOLATK1,SPR_PISTOLATK2,SPR_PISTOLATK3,
    SPR_PISTOLATK4,

    SPR_MACHINEGUNREADY,SPR_MACHINEGUNATK1,SPR_MACHINEGUNATK2,SPR_MACHINEGUNATK3,
    SPR_MACHINEGUNATK4,

    SPR_CHAINREADY,SPR_CHAINATK1,SPR_CHAINATK2,SPR_CHAINATK3,
    SPR_CHAINATK4,

    SPR_NUMSPRITES,
};

extern const char *spritestrs[SPR_NUMSPRITES];

/*
=============================================================================

                             WL_MATH DEFINITIONS

=============================================================================
*/

#define X(a) (a).v[0]
#define Y(a) (a).v[1]
#define Z(a) (a).v[2]
#define C(a, i) (a).v[i]

#define Sq(a) ((a) * (a))

/* access numbered component of vector */
#define C(a, i) (a).v[i]

/* access (row,col) component of matrix */
#define CM(a, r, c) (a).v[(r) * 3 + (c)]

typedef struct mat3f_s
{
    float v[3 * 3];
} mat3f_t;

typedef struct vec3f_s
{ 
    float v[3];
} vec3f_t;

typedef struct vec3fixed_s
{
    fixed v[3];
} vec3fixed_t;

typedef struct mat3fixed_s
{
    fixed v[3 * 3];
} mat3fixed_t;

void Math_PushMatrix(void);

void Math_PopMatrix(void);

void Math_PushMatrixIdent(void);

void Math_TranslateMatrix(float tx, float ty);

void Math_ScaleMatrix(float sx, float sy);

void Math_ShearMatrix(float sx, float sy);

vec3f_t Math_MatrixMultPoint(vec3f_t pt);

vec3f_t Math_MatrixMultPoint(vec3f_t pt);

vec3f_t vec3f(float x, float y, float z);

mat3f_t mat3f_ident(void);

mat3f_t mat3f_mult(mat3f_t a, mat3f_t b);

vec3f_t mat3f_mult_point(mat3f_t a, vec3f_t b);

static inline vec3fixed_t vec3fixed(int32_t x, int32_t y, int32_t z)
{
    vec3fixed_t a;
    C(a, 0) = x;
    C(a, 1) = y;
    C(a, 2) = z;
    return a;
}

static inline vec3fixed_t vec3fixed_zero(void)
{
    vec3fixed_t a = { 0, 0, 0 };
    return a;
}

static inline vec3fixed_t vec3fixed_add(vec3fixed_t a, vec3fixed_t b)
{
    return vec3fixed(
        C(a, 0) + C(b, 0), 
        C(a, 1) + C(b, 1),
        C(a, 2) + C(b, 2)
        );
}

static inline vec3fixed_t vec3fixed_subtract(vec3fixed_t a, vec3fixed_t b)
{
    return vec3fixed(
        C(a, 0) - C(b, 0), 
        C(a, 1) - C(b, 1),
        C(a, 2) - C(b, 2)
        );
}

static inline vec3fixed_t vec3fixed_negate(vec3fixed_t a)
{
    return vec3fixed(-C(a, 0), -C(a, 1), -C(a, 2));
}

static inline fixed vec3fixed_dot(vec3fixed_t a, vec3fixed_t b)
{
    return fixedpt_mul(C(a, 0), C(b, 0)) + fixedpt_mul(C(a, 1), C(b, 1)) + 
        fixedpt_mul(C(a, 2), C(b, 2));
}

static inline vec3fixed_t vec3fixed_scale(vec3fixed_t a, fixed b)
{
    return vec3fixed(fixedpt_mul(X(a), b), fixedpt_mul(Y(a), b), fixedpt_mul(Z(a), b));
}

static inline vec3fixed_t vec3fixed_div(vec3fixed_t a, fixed b)
{
    return vec3fixed(fixedpt_div(X(a), b), fixedpt_div(Y(a), b), fixedpt_div(Z(a), b));
}

vec3fixed_t vec3fixed_anglevec(int32_t angle);

static inline fixed vec3fixed_length(vec3fixed_t a)
{
    return fixedpt_sqrt(vec3fixed_dot(a, a));
}

static inline vec3fixed_t vec3fixed_normalize(vec3fixed_t a)
{
    fixed n;

    n = fixedpt_sqrt(vec3fixed_dot(a, a));
    return n ? vec3fixed(fixedpt_div(X(a), n), 
        fixedpt_div(Y(a), n), fixedpt_div(Z(a), n)) : 
        a;
}

static inline vec3fixed_t vec3fixed_unit(vec3fixed_t a, vec3fixed_t b)
{
    return vec3fixed_normalize(vec3fixed_subtract(b, a));
}

mat3fixed_t mat3fixed_zero(void);

mat3fixed_t mat3fixed_ident(void);

mat3fixed_t mat3fixed_diag(fixed a, fixed b, fixed c);

vec3fixed_t mat3fixed_mult_point(mat3fixed_t a, vec3fixed_t b);

static inline vec3fixed_t mat3fixed_mult_point2d(mat3fixed_t a, vec3fixed_t b)
{
    return vec3fixed(
        fixedpt_mul(CM(a, 0, 0), C(b, 0)) + fixedpt_mul(CM(a, 0, 1), C(b, 1)),
        fixedpt_mul(CM(a, 1, 0), C(b, 0)) + fixedpt_mul(CM(a, 1, 1), C(b, 1)),
        0);
}

mat3fixed_t mat3fixed_rot(fixed angle);

mat3fixed_t mat3fixed_transpose(mat3fixed_t a);

uint32_t Math_FixedLerpRgba(uint32_t a, uint32_t b, byte blend);

/*
=============================================================================

                             WL_PHYSICS DEFINITIONS

=============================================================================
*/

#define PHYS_MAX_FORCEGENS 5

typedef struct Phys_Vec3_s
{
    fixed v[3];
} Phys_Vec3_t;

typedef enum Phys_ForceGeneratorId_e
{
    PHYS_FORCEGENID_SPRING,
    PHYS_FORCEGENID_DAMPING,
    PHYS_FORCEGENID_JITTER,
    PHYS_FORCEGENID_CONSTANT,
} Phys_ForceGeneratorId_t;

typedef struct Phys_ForceGenerator_s
{
    Phys_ForceGeneratorId_t id;
    struct
    {
        Phys_Vec3_t constant;
    } spring;
    struct
    {
        Phys_Vec3_t constant;
    } damping;
    struct
    {
        Phys_Vec3_t initialAmplitude;
        int32_t idleTics;
        Phys_Vec3_t amplitudeFallRate;
    } jitter;
    struct
    {
        Phys_Vec3_t force;
    } constant;
} Phys_ForceGenerator_t;

typedef struct Phys_Particle_s
{
    Phys_Vec3_t pos;
    Phys_Vec3_t vel;
    fixed inv_mass;
    Phys_Vec3_t force;
    bool rejitter;
    Phys_Vec3_t jitterPos;
    Phys_Vec3_t jitterAmplitude;
    int32_t jitterTics;
    Phys_ForceGenerator_t forceGenerators[PHYS_MAX_FORCEGENS];
    int numForceGenerators;
} Phys_Particle_t;

extern Phys_Vec3_t Phys_Vec3(fixed x, fixed y, fixed z);

extern Phys_Vec3_t Phys_Vec3Zero(void);

extern Phys_Vec3_t Phys_Vec3Add(Phys_Vec3_t a, Phys_Vec3_t b);

extern Phys_Vec3_t Phys_Vec3Rot2D(Phys_Vec3_t pt, fixed angle);

extern void Phys_ParticleMove(Phys_Particle_t *particle);

extern void Phys_ParticleReset(Phys_Particle_t *particle);

extern void Phys_ParticleUpdateForce(Phys_Particle_t *particle);

/*
=============================================================================

                               GLOBAL TYPES

=============================================================================
*/

typedef enum {
    di_north,
    di_east,
    di_south,
    di_west
} controldir_t;

typedef enum {
    key_gold,
    key_silver,
    key_3,
    key_4,
    key_max,
} keytype_t;

typedef enum {
    dr_normal,
    dr_lock1,
    dr_lock2,
    dr_lock3,
    dr_lock4,
    dr_locklast = dr_lock4,
    dr_elevator,
    dr_max,
    dr_pwdoor = 0xff,
} door_t;

typedef enum {
  dr_orient_horz,
  dr_orient_vert,
  dr_orient_max
} door_orient_t;

typedef enum {
  dr_face_front,
  dr_face_side,
  dr_face_max,
} door_face_t;

typedef enum {
    ac_no,
    ac_yes,
    ac_allways
} activetype;

typedef struct sprreloc_s {
    int src;
    int dst;
    int len;
} sprreloc_t;

typedef struct objclass_s {
    sprreloc_t sprrelocs[3];
} objclass_t;

typedef enum {
    nothing,
    playerobj,
    inertobj,
    guardobj,
    officerobj,
    ssobj,
    dogobj,
    bossobj,
    schabbobj,
    fakeobj,
    mechahitlerobj,
    mutantobj,
    needleobj,
    fireobj,
    bjobj,
    ghostobj,
    realhitlerobj,
    gretelobj,
    giftobj,
    fatobj,
    rocketobj,

    spectreobj,
    angelobj,
    transobj,
    uberobj,
    willobj,
    deathobj,
    hrocketobj,
    sparkobj,
} classtype;

typedef enum {
    none,
    block,
    bo_gibs,
    bo_alpo,
    bo_firstaid,
    bo_key1,
    bo_key2,
    bo_key3,
    bo_key4,
    bo_cross,
    bo_chalice,
    bo_bible,
    bo_crown,
    bo_clip,
    bo_clip2,
    bo_machinegun,
    bo_chaingun,
    bo_food,
    bo_fullheal,
    bo_25clip,
    bo_spear,
} wl_stat_t;

typedef enum {
    east,
    northeast,
    north,
    northwest,
    west,
    southwest,
    south,
    southeast,
    nodir
} dirtype;

typedef enum
{
	dmg_normal,
	dmg_legrope,
	dmg_taser
} damagetype;

#define NUMENEMIES  22
typedef enum {
    en_guard,
    en_officer,
    en_ss,
    en_dog,
    en_boss,
    en_schabbs,
    en_fake,
    en_hitler,
    en_mutant,
    en_blinky,
    en_clyde,
    en_pinky,
    en_inky,
    en_gretel,
    en_gift,
    en_fat,
    en_spectre,
    en_angel,
    en_trans,
    en_uber,
    en_will,
} enemy_t;

typedef void (* statefunc) (void *);

typedef struct statestruct
{
    boolean rotate;
    short   shapenum;           // a shapenum of -1 means get from ob->temp1
    short   tictime;
    void    (*think) (void *),(*action) (void *);
    struct  statestruct *next;
} statetype;

typedef struct statanimperiodstruct
{
    int value;
    int rangeStart;
    int rangeEnd;
} statanimperiod_t;

typedef struct statanimstruct
{
    byte frames;
    statanimperiod_t period;
    int32_t tics;
} statanim_t;

typedef struct statproxstruct
{
    bool enabled;
    fixed dist;
    short shapenum[2];
} statprox_t;

typedef void (*statthink_t)(void *statptr);

#define STATINFO_MAX_BLOCKS 5

typedef struct StatBlock_s
{
    fixed w;
    fixed h0, h1;
} statblock_t;

typedef statblock_t statblockset_t[STATINFO_MAX_BLOCKS];

//---------------------
//
// trivial actor structure
//
//---------------------

typedef struct statstruct
{
    byte      tilex,tiley;
    short     shapenum;           // if shapenum == -1 the obj has been removed
    byte      *sprite;
    byte      *visspot;
    uint32_t  flags;
    byte      itemnumber;
    statanim_t anim;
    byte      drawpri;
    statblockset_t blocks;
    int       lightId;
    statthink_t think;
    statprox_t prox;
    byte      blend;
    int       radLightKey;
} statobj_t;

//---------------------
//
// door actor structure
//
//---------------------

typedef enum
{
    dr_open,dr_closed,dr_opening,dr_closing
} doortype;

typedef enum pwdoorfacedir_e
{
    pwdoorfacedirfirst,
    pwdoorfacedireast = pwdoorfacedirfirst,
    pwdoorfacedirnorth,
    pwdoorfacedirwest,
    pwdoorfacedirsouth,
    pwdoorfacedirmax,
} pwdoorfacedir_t;

typedef int pwdoorfacedirmask_t;

typedef struct doorstruct
{
    byte     tilex,tiley;
    boolean  vertical;
    byte     lock;
    doortype action;
    short    ticcount;
    int      quadIndexArr[8];
    int      quadIndexCount;
    int      start_tilex;
    int      start_tiley;
    pwdoorfacedirmask_t faces;
} doorobj_t;

//--------------------
//
// thinking actor structure
//
//--------------------

typedef enum AiEnemySuspicionLevel_e
{
    SUSPICION_PASSIVE,
    SUSPICION_ALERTED,
    SUSPICION_ALARMED,
} AiEnemySuspicionLevel_t;

typedef struct AiEnemy_s
{
    double suspicionMeter;
    int suspicionLevel;
    bool killed;
    int suspicionSpot;
    bool suspicionTargetIsPlayer;
} AiEnemy_t;

typedef struct objstruct
{
    int         id;
    activetype  active;
    short       ticcount;
    classtype   obclass;
    statetype   *state;

    uint32_t    flags;              // FL_SHOOTABLE, etc

    int32_t     distance;           // if negative, wait for that door to open
    dirtype     dir;

    fixed       x,y;
    word        tilex,tiley;
    byte        areanumber;

    short       viewx;
    word        viewheight;
    fixed       transx,transy;      // in global coord

    short       angle;
    short       hitpoints;
    short       starthitpoints;
    int32_t     speed;
    short       sproffset;
    word        tile;
    int32_t     minactordist;
    fixed       heightoff;
    int32_t     statetics;
    fixed       actorsize;
    byte        drawpri;
    byte        blend;
    int         lightId;
    int         projShooter;
    byte        isAiEnemy;
    AiEnemy_t   aiEnemy;

    statefunc   onRemove;
    short       temp1,temp2,hidden;
    struct objstruct *next,*prev;
} objtype;

typedef void (*objfunc_t)(objtype *obj);

enum
{
    bt_nobutton=-1,
    bt_attack=0,
    bt_strafe,
    bt_run,
    bt_use,
    bt_readyknife,
    bt_readypistol,
    bt_readymachinegun,
    bt_readychaingun,
    bt_nextweapon,
    bt_prevweapon,
    bt_esc,
    bt_pause,
    bt_strafeleft,
    bt_straferight,
    bt_moveforward,
    bt_movebackward,
    bt_turnleft,
    bt_turnright,
    NUMBUTTONS
};


typedef enum
{
    wp_knife,
    wp_pistol,
    wp_machinegun,
    wp_chaingun,
    wp_max
} weapontype;

typedef enum
{
    ammogroup_knife,
    ammogroup_bullet,
    ammogroup_max,
} ammogrouptype_t;

typedef struct
{
    short ammolimit;
    short ammo;
} ammogroup_t;

typedef struct
{
    int fullheal_ammo;
    int ammolimit;
    int startammo;
} ammogroupinfo_t;


typedef struct
{
    int hud_pic;
    int weapitem_ammo;
    soundnames shootsnd;
    int weaponscale;
    bool requires_ammo;
    ammogrouptype_t ammogrouptype;
} weaponinfo_t;

enum
{
    gd_baby,
    gd_easy,
    gd_medium,
    gd_hard,
    gd_max,
};

typedef struct
{
    Phys_Particle_t particle;
} weaponshudder_t;

//---------------
//
// gamestate structure
//
//---------------

typedef struct
{
    short       difficulty;
    short       mapon;
    int32_t     oldscore,score,nextextra;
    short       lives;
    short       health;
    ammogroup_t ammogroups[ammogroup_max];
    short       keys;
    weapontype  bestweapon,weapon,chosenweapon;
    bool        haveweapon[wp_max];

#ifdef WEAPONBOBBLE
    boolean            bobdir, bobdir2;
    short              bobber, bobber2;
    fixed              bobberdist;
    fixed              bobberx, bobbery;
#endif

    short       faceframe;
    short       attackframe,attackcount,weaponframe;

    short       episode,secretcount,treasurecount,killcount,
                secrettotal,treasuretotal,killtotal;
    int32_t     TimeCount;
    int32_t     killx,killy;
    boolean     victoryflag;            // set during victory animations
    int32_t     PrevLoopTimeCount;
    weaponshudder_t weaponshudder;
} gametype;


typedef enum
{
    ex_stillplaying,
    ex_completed,
    ex_died,
    ex_warped,
    ex_resetgame,
    ex_loadedgame,
    ex_victorious,
    ex_abort,
    ex_demodone,
    ex_secretlevel
} exit_t;


extern word *mapsegs[MAPPLANES];
extern int mapon;

extern objclass_t objclasses[];

/*
=============================================================================

                             WL_MAIN DEFINITIONS

=============================================================================
*/

#define HUD_CURRENT hudConfigData[hudconfig]

enum HudConfig_e
{
    HUD_CONFIG_SCORE,
    HUD_CONFIG_MAX,
};

typedef struct HudConfigData_s
{
    int statusBarPic;
    int32_t *scoreField;
    int scoreFieldWidth;
} HudConfigData_t;

extern HudConfigData_t hudConfigData[HUD_CONFIG_MAX];

extern WolfRad_t wolfRad;
extern ED_t      ed;

#define SAVE_FIELD(x) \
do { \
    fwrite(&(x), sizeof(x), 1, file); \
    checksum = DoChecksum((byte *)&(x),sizeof(x),checksum); \
} while (0)

#define LOAD_FIELD(x) \
do { \
    fread(&(x), sizeof(x), 1, file); \
    checksum = DoChecksum((byte *)&(x),sizeof(x),checksum); \
} while (0)

extern  boolean  loadedgame;
extern  fixed    focallength;
extern  int      viewscreenx, viewscreeny;
extern  int      viewwidth;
extern  int      viewheight;
extern  short    centerx;
extern  int32_t  heightnumerator;
extern  fixed    scale;

extern  int      dirangle[9];

extern  int      mouseadjustment;
extern  int      hudconfig;
extern  int      shootdelta;

extern  boolean  startgame;
extern  char     str[80];
extern  char     configdir[256];
extern  char     configname[13];

//
// Command line parameter variables
//
extern  boolean  param_debugmode;
extern  boolean  param_nowait;
extern  boolean  param_nosound;
extern  boolean  param_noenemies;
extern  int      param_difficulty;
extern  int      param_tedlevel;
extern  int      param_joystickindex;
extern  int      param_joystickhat;
extern  int      param_samplerate;
extern  int      param_audiobuffer;
extern  int      param_mission;
extern  boolean  param_goodtimes;
extern  boolean  param_ignorenumchunks;
extern  boolean  param_nofade;
extern  boolean  param_fakedblbuf;
extern  boolean  param_rad;


void            NewGame (int difficulty,int episode);
void            CalcProjection (int32_t focal);
void            NewViewSize (int width);
boolean         SetViewSize (unsigned width, unsigned height);
boolean         LoadTheGame(FILE *file,int x,int y);
boolean         SaveTheGame(FILE *file,int x,int y);
void            ShowViewSize (int width);
void            ShutdownId (void);
int32_t         DoChecksum(byte *source,unsigned size,int32_t checksum);

/*
=============================================================================

                         WL_GAME DEFINITIONS

=============================================================================
*/

extern  gametype        gamestate;
extern  byte            bordercol;
extern  SDL_Surface     *latchpics[NUMLATCHPICS];
extern  char            demoname[13];

void    SetupGameLevel (void);
void    GameLoop (void);
void    DrawPlayBorderEx (bool hudSwitch);
void    DrawPlayBorder (void);
void    DrawStatusBorder (byte color);
void    DrawPlayScreen (void);
void    DrawPlayBorderSides (void);
void    ShowActStatus();

void    PlayDemo (int demonumber);
void    RecordDemo (void);


#ifdef SPEAR
extern  int32_t            spearx,speary;
extern  unsigned        spearangle;
extern  boolean         spearflag;
#endif


#define ClearMemory SD_StopDigitized


// JAB
#define PlaySoundLocTile(s,tx,ty)       PlaySoundLocGlobal(s,(((int32_t)(tx) << TILESHIFT) + (1L << (TILESHIFT - 1))),(((int32_t)ty << TILESHIFT) + (1L << (TILESHIFT - 1))))
#define PlaySoundLocActor(s,ob)         PlaySoundLocGlobal(s,(ob)->x,(ob)->y)
void    PlaySoundLocGlobal(word s,fixed gx,fixed gy);
void UpdateSoundLoc(void);


/*
=============================================================================

                            WL_PLAY DEFINITIONS

=============================================================================
*/

#define BASEMOVE                35
#define RUNMOVE                 70
#define BASETURN                35
#define RUNTURN                 70

#define JOYSCALE                2

// unused
#define REDSTEPS        8
#define WHITESTEPS      20

#define BONUSFLASH_PEAK 200

extern  int             damagepeak;

extern  byte            tilemap[MAPSIZE][MAPSIZE];      // wall values only
extern  byte            spotvis[MAPSIZE][MAPSIZE];
extern  objtype         *actorat[MAPSIZE][MAPSIZE];
extern  statobj_t       *statat[MAPSIZE][MAPSIZE];

extern  objtype         *player;

extern  unsigned        tics;
extern  int             viewsize;

extern  int             lastgamemusicoffset;
extern  boolean         palshifted;

//
// current user input
//
extern  int         controlx,controly;              // range from -100 to 100
extern  boolean     buttonstate[NUMBUTTONS];
extern  objtype     objlist[MAXACTORS];
extern  boolean     buttonheld[NUMBUTTONS];
extern  exit_t      playstate;
extern  int         madenoise;
extern  statobj_t   statobjlist[MAXSTATS];
extern  statobj_t   *laststatobj;
extern  objtype     *newobj,*killerobj;
extern  doorobj_t   doorobjlist[MAXDOORS];
extern  doorobj_t   *lastdoorobj;
extern  int         godmode;
extern  int         notargetmode;

extern  boolean     demorecord,demoplayback;
extern  int8_t      *demoptr, *lastdemoptr;
extern  memptr      demobuffer;

//
// control info
//
extern  boolean     mouseenabled,joystickenabled;
extern  int         dirscan[4];
extern  int         buttonscan[NUMBUTTONS];
extern  int         buttonmouse[4];
extern  int         buttonjoy[32];

void    InitActorList (void);
int     GetNewActor (void);
void    PlayLoop (void);

void    CenterWindow(word w,word h);

void    InitRedShifts (void);
void    FinishPaletteShifts (void);

void    RemoveObj (objtype *gone);
void    PollControls (void);
int     StopMusic(void);
void    StartMusic(void);
void    ContinueMusic(int offs);
void    StartDamageFlash (int damage);
void    StartBonusFlash (void);

#ifdef SPEAR
extern  int32_t     funnyticount;           // FOR FUNNY BJ FACE
#endif

extern  boolean     noclip,ammocheat;
extern  int         singlestep, extravbls;

extern  ammogroupinfo_t ammogroupinfo[ammogroup_max];
extern  weaponinfo_t weapinfo[wp_max];

extern  int         flash_red, flash_white;

extern int          objcount, lastobjindex;

/*
=============================================================================

                                WL_INTER

=============================================================================
*/

void IntroScreen (void);
void PG13(void);
void DrawHighScores(void);
void CheckHighScore (int32_t score,word other);
void Victory (void);
void LevelCompleted (void);
void ClearSplitVWB (void);

void PreloadGraphics(void);


/*
=============================================================================

                                WL_DEBUG

=============================================================================
*/

int DebugKeys (void);

/*
=============================================================================

                            WL_DRAW DEFINITIONS

=============================================================================
*/

#define WOLFTICRATE_DEFAULT 7
#define ACTORSIZE       0x4000
#define STATOBJSIZE     0x2000

#define LT_SHADE_RANGE 512

typedef enum LMFace_e
{
    LMFACE_WALL_HORIZ,
    LMFACE_WALL_VERT,
    LMFACE_FLOOR,
    LMFACE_CEILING,
    LMFACE_DOOR_HORIZ,
    LMFACE_DOOR_VERT,
} LMFace_t;

typedef struct VBuf_s
{
    byte *start;
    byte *pix;
} VBuf_t;

// input (shade, color index)
// output (RGBA)
typedef byte VBufLightTable_t[LT_SHADE_RANGE][256][4];

// preset fade towards (red, green, blue)
// input (component value, shift, component)
// output (component value)
typedef byte VBufFadeTable_t[256][256][3];

// input (source component value, target component value, shift)
// output (component value)
//
// f(x, y, z) = x * (1 - z) + y * z
// f(x, y, z) = x + y - f(x, y, 1 - z)
// reorder: [z(alpha)][x][y]
typedef byte VBufBlendTable_t[256][256][256];

extern VBufLightTable_t VBufLightTable;
extern VBufFadeTable_t VBufFadeTable;
extern VBufBlendTable_t VBufBlendTable;

extern void VBufSetLightTable(SDL_Color *pal, int red, int green, int blue);
extern void VBufInitFadeTable(int red, int green, int blue);
extern void VBufInitBlendTable(void);
extern void VBufInitTables(void);
extern void VBufApplyLightShift(int red, int green, int blue);
extern void VBufSetNightVisionLightTable(void);
extern void VBufSetDefaultLightTable(void);
extern void VBufWritePix2D(VBuf_t vbuf, int off, byte pix);
extern void VBufFlushWriteCache(void);
extern void VBufResetWriteCache(void);

static inline VBuf_t VBuf(byte *pix)
{
    VBuf_t vbuf;
    vbuf.start = vbuf.pix = pix;
    return vbuf;
}

static inline VBuf_t VBufOffset(VBuf_t vbuf, int off)
{
    VBuf_t vmem;
    vmem.start = vbuf.start;
    vmem.pix = vbuf.pix + (off << 2);
    return vmem;
}

static inline void VBufWritePix(VBuf_t vbuf, int off, 
    byte shade, byte pix)
{
    *((uint32_t *)&vbuf.pix[off << 2]) = *((uint32_t *)VBufLightTable[shade][pix]);
}

typedef uint32_t VBufLightTableFast_t;
typedef byte *VBufBlendTableFast_t;

extern VBufLightTableFast_t *VBufLightTableFast;
extern VBufBlendTableFast_t VBufBlendTableFast;

static inline void VBufSetShadeFast(byte shade)
{
	VBufLightTableFast = (uint32_t *)VBufLightTable[shade][0];
}

static inline void VBufSetBlendFast( byte alpha )
{
	VBufBlendTableFast = (byte *)VBufBlendTable[alpha][0];
}

// note: this is only faster for non-constant shades (i.e. if shade is an argument to func and so on)
static inline void VBufWritePixFast(VBuf_t vbuf, int off, 
    byte pix)
{
    *((uint32_t *)&vbuf.pix[off << 2]) = VBufLightTableFast[pix];
}

static inline void VBufWriteColor(VBuf_t vbuf, int off, uint32_t color)
{
    *((uint32_t *)&vbuf.pix[off << 2]) = color;
}

static inline uint32_t VBufReadColor(VBuf_t vbuf, int off)
{
    return *((uint32_t *)&vbuf.pix[off << 2]);
}

static inline void VBufWriteBlendPix(VBuf_t vbuf, int off, 
    byte shade, byte pix, byte blend)
{
    vbuf.pix[(off << 2) + 0] = VBufBlendTable[blend][VBufLightTable[shade][pix][0]][vbuf.pix[(off << 2) + 0]];
    vbuf.pix[(off << 2) + 1] = VBufBlendTable[blend][VBufLightTable[shade][pix][1]][vbuf.pix[(off << 2) + 1]];
    vbuf.pix[(off << 2) + 2] = VBufBlendTable[blend][VBufLightTable[shade][pix][2]][vbuf.pix[(off << 2) + 2]];
}

static inline void VBufWriteBlendPixFast(VBuf_t vbuf, int off, 
    byte pix)
{
    vbuf.pix[(off << 2) + 0] = VBufBlendTableFast[ (((uint8_t *)(VBufLightTableFast + pix))[0] << 8 ) + vbuf.pix[(off << 2) + 0]];
    vbuf.pix[(off << 2) + 1] = VBufBlendTableFast[ (((uint8_t *)(VBufLightTableFast + pix))[1] << 8 ) + vbuf.pix[(off << 2) + 1]];
    vbuf.pix[(off << 2) + 2] = VBufBlendTableFast[ (((uint8_t *)(VBufLightTableFast + pix))[2] << 8 ) + vbuf.pix[(off << 2) + 2]];
}

static inline void VBufWriteBlendColor(VBuf_t vbuf, int off, uint32_t color, byte blend)
{
    vbuf.pix[(off << 2) + 0] = VBufBlendTable[blend][(color & 0x000000ff) >> 0][vbuf.pix[(off << 2) + 0]];
    vbuf.pix[(off << 2) + 1] = VBufBlendTable[blend][(color & 0x0000ff00) >> 8][vbuf.pix[(off << 2) + 1]];
    vbuf.pix[(off << 2) + 2] = VBufBlendTable[blend][(color & 0x00ff0000) >> 16][vbuf.pix[(off << 2) + 2]];
}

static inline void VBufWriteBlendColorFast(VBuf_t vbuf, int off, uint32_t color)
{
    vbuf.pix[(off << 2) + 0] = VBufBlendTableFast[(((color & 0x000000ff) >> 0) << 8) + vbuf.pix[(off << 2) + 0]];
    vbuf.pix[(off << 2) + 1] = VBufBlendTableFast[(((color & 0x0000ff00) >> 8) << 8) + vbuf.pix[(off << 2) + 1]];
    vbuf.pix[(off << 2) + 2] = VBufBlendTableFast[(((color & 0x00ff0000) >> 16) << 8) + vbuf.pix[(off << 2) + 2]];
}

static inline void LT_BlendTrueColor(VBuf_t vbuf, int off,
    uint32_t col, byte alpha)
{
    if (alpha == 0xff)
    {
        VBufWriteColor(vbuf, off, col);
    }
    else if (alpha > 0x00)
    {
        VBufWriteBlendColor(vbuf, off, col, alpha);
    }
}

extern  void ScalePost();

extern  uint32_t *lmsource;
extern  int      postx;
extern  short    xtile, ytile;
extern  short    xtilestep, ytilestep;

//
// math tables
//
extern  short *pixelangle;
extern  int32_t finetangent[FINEANGLES/4];
extern  fixed sintable[];
extern  fixed *costable;
extern  int *wallheight;
extern  word horizwall[],vertwall[];
extern  WR_LightInfo_t *curLightInfo;
extern  int32_t    wolfticrate;
extern  int32_t    lasttimecount;
extern  int32_t    frameon;

extern  unsigned screenloc[3];

extern  boolean fizzlein, fpscounter;

extern  fixed   viewx,viewy;                    // the focal point
extern  fixed   viewsin,viewcos;

extern  int     doorlock_to_doorpage[dr_max][dr_face_max];

extern  byte    vgaCeiling[];

void    ThreeDRefresh (void);
void    CalcTics (void);

typedef struct
{
    word leftpix,rightpix;
    word dataofs[64];
// table data after dataofs[rightpix-leftpix+1]
} t_compshape;

extern VBuf_t VBufLockSurfaceEx(SDL_Surface *surf);
extern VBuf_t VBufUnlockSurfaceEx(SDL_Surface *surf);

extern VBuf_t VBufLockSurface(void);
extern VBuf_t VBufUnlockSurface(void);

void HUD_CheckSwitch (void);
void HUD_Switch (void);
void HUD_DrawPlayBorder (void);

void ChangeWolfTicRate (int ticrate);

uint32_t *GetLightmap(LMFace_t face, int u, int v, int tilex, 
    int tiley, bool *isDefault);
uint32_t *DefaultLightmap(void);

lwlib_TPoint3f SpriteLitLevel(fixed x, fixed y);

/*
=============================================================================

                             WL_STATE DEFINITIONS

=============================================================================
*/
#define TURNTICS        10
#define SPDPATROL       512
#define SPDDOG          1500


void    InitHitRect (objtype *ob, unsigned radius);
void    SpawnNewObj (unsigned tilex, unsigned tiley, statetype *state);
void    NewState (objtype *ob, statetype *state);

boolean TryWalk (objtype *ob);
void    SelectChaseDir (objtype *ob);
void    SelectDodgeDir (objtype *ob);
void    SelectRunDir (objtype *ob);
void    SelectRandomDir (objtype *ob);
void    MoveObj (objtype *ob, int32_t move);
boolean SightPlayer (objtype *ob);
void    FirstSighting (objtype *ob);
void    LoseSighting (objtype *ob);

void    KillActor (objtype *ob, int dmgtype = dmg_normal );
void    DamageActor (objtype *ob, int damage, int dmgtype = dmg_normal );

boolean CheckLine (objtype *ob);
boolean CheckSight (objtype *ob);

fixed Math_Distance2(fixed x1, fixed y1, fixed x2, fixed y2);

/*
=============================================================================

                             WL_AGENT DEFINITIONS

=============================================================================
*/

#define WEAPAMMO(weapon) WeaponAmmoGroup(weapon)->ammo
#define WEAPAMMOLIMIT(weapon) WeaponAmmoGroup(weapon)->ammolimit

#define AMMOGROUP(ammogrouptype) gamestate.ammogroups[ammogrouptype]
#define AMMOGROUPAMMO(ammogrouptype) \
    gamestate.ammogroups[ammogrouptype].ammo
#define AMMOGROUPLIMIT(ammogrouptype) \
    gamestate.ammogroups[ammogrouptype].ammolimit

extern  short    anglefrac;
extern  int      facecount, facetimes;
extern  word     plux,pluy;         // player coordinates scaled to unsigned
extern  int32_t  thrustspeed;
extern  objtype  *LastAttacker;

void    WeaponBobble_Update(void);
void    Thrust (int angle, int32_t speed);
void    SpawnPlayer (int tilex, int tiley, int dir);
void    TakeDamage (int points,objtype *attacker);
void    Suicide (void);
void    GivePoints (int32_t points);
void    GetBonus (statobj_t *check);
void    GiveWeapon (int weapon);
void    GiveAmmo (int ammo, ammogrouptype_t ammogrouptype);
void    GiveKey (int key);
bool    HaveKey (int key);
void    UseCoins (int coins);
void    ResetWeapons (void);
ammogroup_t *WeaponAmmoGroup (int weapon);

//
// player state info
//

void    StatusDrawFace(unsigned picnum);
void    DrawFace (void);
void    DrawHealth (void);
void    HealSelf (int points);
void    DrawLevel (void);
void    DrawLives (void);
void    GiveExtraMan (void);
void    DrawScore (void);
void    DrawWeapon (void);
void    DrawKeys (void);
void    DrawAmmo (void);

//
// weapon shudder
//

void WeaponShudder_UpdatePhysics(void);

void WeaponShudder_Push(fixed vx, fixed vy, 
    fixed spring_constant, fixed damping_constant);

/*
=============================================================================

                             WL_SHADE DEFINITIONS

=============================================================================
*/

#define LT_MAX_LIGHTS 500
#define LT_LERP_GROUP_PIXELS 8
#define LT_GLOBAL_AMBIENT (FP(1) / 5)
#define LT_BLACKOUT_DIVISOR 3

#define LT_DEFAULT_DISTANCE_ATTENUATION 5
#define LT_GASATTACK_DISTANCE_ATTENUATION 0

#ifdef RGB_LSB_FIRST
#define LT_CPT(col, c) \
    (((col) & (0x000000FF << ((c) * 8))) >> ((c) * 8))
#define LT_PACK(r, g, b) \
    (((r) << 0) + ((g) << 8) + ((b) << 16) + 0xFF000000)
#define LT_PALCPT(pal, c) \
    (pal)[c]
#else
#define LT_CPT(col, c) \
    (((col) & (0x000000FF << ((2 - (c)) * 8))) >> ((2 - (c)) * 8))
#define LT_PACK(r, g, b) \
    (((r) << 16) + ((g) << 8) + ((b) << 0) + 0xFF000000)
#define LT_PALCPT(pal, c) \
    (pal)[2 - (c)]
#endif

#define LT_RED(col) LT_CPT(col, 0)
#define LT_GREEN(col) LT_CPT(col, 1)
#define LT_BLUE(col) LT_CPT(col, 2)
#define LT_ALPHA(col) (((col) & 0xFF000000) >> 24)

#define LT_PALRED(pal) LT_PALCPT(pal, 0)
#define LT_PALGREEN(pal) LT_PALCPT(pal, 1)
#define LT_PALBLUE(pal) LT_PALCPT(pal, 2)

#define LT_RGB_EXPAND(col) LT_RED(col), LT_GREEN(col), LT_BLUE(col)

#define LT_TRANS(col) (((col) & 0xFF000000) == 0xFF000000)

#define LT_DISTTABLE_ACCESS(table, ltx, lty) \
    (table)->cells[((lty) << (table)->widthShift) + (ltx)]

typedef fixed LT_Spectra_t;
typedef int LT_Shade_t;
typedef uint32_t LT_Color_t;
typedef void (*LT_Light_AnonThink_t)(void *light);

typedef enum LT_Light_Predef_e
{
    LT_LIGHT_PREDEF_NONE,
    LT_LIGHT_PREDEF_MEDIUM,
    LT_LIGHT_PREDEF_HIGH,
    LT_LIGHT_PREDEF_EXTRAHIGH,
    LT_LIGHT_PREDEF_EMPPROJ,
    LT_LIGHT_PREDEF_MUZZLEFLASH,
    LT_LIGHT_PREDEF_BURNING_1,
    LT_LIGHT_PREDEF_BURNING_2,
    LT_LIGHT_PREDEF_EXPLODE,
    LT_LIGHT_PREDEF_FADE_DOWN,
    LT_LIGHT_PREDEF_TASERFLASH,
    LT_LIGHT_PREDEF_TREASURE,
    LT_LIGHT_PREDEF_COUNT,
} LT_Light_Predef_t;

typedef enum LT_CachedBrightnessTable_e
{
    LT_CACHED_BRIGHTNESS_TABLE_NONE,
    LT_CACHED_BRIGHTNESS_TABLE_TREASURE,
    LT_CACHED_BRIGHTNESS_TABLE_MAX,
} LT_CachedBrightnessTable_t;

typedef struct LT_Light_Cfg_s
{
    fixed reach;
    LT_Spectra_t strength;
    LT_Spectra_t ambient;
    LT_Light_AnonThink_t think;
    int cachedBrightnessTable;
    fixed x;
    fixed y;
    bool disabled;
    int lifeTics;
    fixed halfReachSq;
    fixed invHalfReachSq;
} LT_Light_Cfg_t;

typedef struct LT_Light_PredefCfg_s
{
    LT_Light_Cfg_t cfg;
    struct
    {
        LT_Light_Cfg_t cfg;
        int32_t tics;
        bool destroyLight;
    } fade;
    struct
    {
        LT_Light_Cfg_t cfg;
        int32_t tics;
    } jitter;
    struct
    {
        LT_Light_Cfg_t cfg;
        int32_t tics;
    } strobe;
} LT_Light_PredefCfg_t;

typedef struct LT_Light_State_s
{
    LT_Light_Cfg_t cfg;
    struct
    {
        int32_t ticsElapsed;
    } fade;
    struct
    {
        bool start;
        LT_Light_Cfg_t cfg;
        int32_t ticsElapsed;
    } jitter;
    struct
    {
        bool start;
        LT_Light_Cfg_t cfg;
        int32_t ticsElapsed;
    } strobe;
    struct
    {
        int32_t ticsElapsed;
    } lifeTimed;
    LT_Light_Predef_t predef;
    LT_Light_PredefCfg_t predefCfg;
    bool deleteMark;
} LT_Light_State_t;

typedef struct LT_DistTable_s
{
    fixed *cells;
    int width;
    int widthShift;
    int size;
    bool ready;
} LT_DistTable_t;

typedef struct LT_Light_s
{
    int id;
    LT_Light_State_t state;
    LT_DistTable_t *brightnessTable;
    struct LT_Light_s *prev;
    struct LT_Light_s *next;
} LT_Light_t;

typedef void (*LT_Light_Think_t)(LT_Light_t *light);

typedef struct LT_LerpPoint_s
{
    fixed gu, gv;
    bool shadeComputed;
    LT_Spectra_t shade;
} LT_LerpPoint_t;

typedef struct LT_LerpGroup_s
{
    int pix;
    fixed du, dv;
    LT_LerpPoint_t start;
    LT_LerpPoint_t end;
    LT_Spectra_t curShade;
    LT_Spectra_t stepShade;
} LT_LerpGroup_t;

typedef struct LT_Lerp_s
{
    int y;
    fixed du, dv;
    fixed gu, gv;
    LT_LerpGroup_t group;
} LT_Lerp_t;

typedef struct LT_Tile_s
{
    vec_t(int) lightNums;
} LT_Tile_t;

typedef struct LT_s
{
    int distAttenuation;
    int distShadeLimit;
    LT_Light_t lights[LT_MAX_LIGHTS];
    int numLights;
    int lastLightIndex;
    LT_Light_PredefCfg_t predefCfgs[LT_LIGHT_PREDEF_COUNT];
    bool blackOutElectricLights;
    bool blackOutGlobalAmbient;
    LT_Spectra_t ambient;
    LT_Tile_t tiles[LT_MAPSIZE * LT_MAPSIZE];
    lwlib_IntMap_t litSpriteMap;
    LT_DistTable_t cachedBrightnessTables[LT_CACHED_BRIGHTNESS_TABLE_MAX];
} LT_t;

static inline uint32_t LT_ClipPack(int r, int g, int b)
{
	int max;
	uint32_t col;

    max = r;
    if (g > max)
    {
        max = g;
    }
    if (b > max)
    {
        max = b;
    }

    if (max > 255)
    {
        col = LT_PACK(r * 255 / max, g * 255 / max, b * 255 / max);
    }
    else
    {
        col = LT_PACK(r, g, b);
    }

	return col;
}

/*
=============================================================================

                             WL_ACT1 DEFINITIONS

=============================================================================
*/

typedef struct StatInfo_s
{
    short      picnum;
    wl_stat_t  type;
    uint32_t   specialFlags;    // they are ORed to the statobj_t flags
    int radLightKey;
    LT_Light_Predef_t lightPredef;
    statanim_t anim;
    statblockset_t blocks;
    statthink_t think;
    statprox_t prox;
} StatInfo_t;

extern  doorobj_t doorobjlist[MAXDOORS];
extern  doorobj_t *lastdoorobj;
extern  short     doorcount;

extern  word      doorposition[MAXDOORS];

extern  byte      areaconnect[NUMAREAS][NUMAREAS];

extern  boolean   areabyplayer[NUMAREAS];

extern word     pwallstate;
extern word     pwallpos;        // amount a pushable wall has been moved (0-63)
extern word     pwallx,pwally;
extern byte     pwalldir,pwalltile;


void InitDoorList (void);
void InitStaticList (void);
void SpawnStatic (int tilex, int tiley, int type, int tile);
void SpawnDoor (int tilex, int tiley, boolean vertical, int lock);
void MoveDoors (void);
void MovePWalls (void);
void OpenDoor (int door);
statobj_t *PlaceItemType (int itemtype, int tilex, int tiley);
void PushWall (int checkx, int checky, int dir);
void OperateDoor (int door);
void InitAreas (void);


void SpawnPwDoors (void);
void PwDoors_PushWallEnteredBlock(int oldtilex, int oldtiley, 
    int newtilex, int newtiley);

/*
=============================================================================

                             WL_ACT2 DEFINITIONS

=============================================================================
*/

#define PROJECTILESIZE  0xc000l

#define s_nakedbody s_static10

extern short starthitpoints[4][NUMENEMIES];

extern  statetype s_grddie1;
extern  statetype s_dogdie1;
extern  statetype s_ofcdie1;
extern  statetype s_mutdie1;
extern  statetype s_ssdie1;
extern  statetype s_bossdie1;
extern  statetype s_schabbdie1;
extern  statetype s_fakedie1;
extern  statetype s_mechadie1;
extern  statetype s_hitlerdie1;
extern  statetype s_greteldie1;
extern  statetype s_giftdie1;
extern  statetype s_fatdie1;

extern  statetype s_spectredie1;
extern  statetype s_angeldie1;
extern  statetype s_transdie0;
extern  statetype s_uberdie0;
extern  statetype s_willdie1;
extern  statetype s_deathdie1;

extern  statetype s_ofcstand;
extern  statetype s_mutstand;
extern  statetype s_ssstand;

extern  statetype s_grdchase1;
extern  statetype s_dogchase1;
extern  statetype s_ofcchase1;
extern  statetype s_sschase1;
extern  statetype s_mutchase1;
extern  statetype s_bosschase1;
extern  statetype s_schabbchase1;
extern  statetype s_fakechase1;
extern  statetype s_mechachase1;
extern  statetype s_gretelchase1;
extern  statetype s_giftchase1;
extern  statetype s_fatchase1;

extern  statetype s_spectrechase1;
extern  statetype s_angelchase1;
extern  statetype s_transchase1;
extern  statetype s_uberchase1;
extern  statetype s_willchase1;
extern  statetype s_deathchase1;

extern  statetype s_blinkychase1;
extern  statetype s_hitlerchase1;

extern  statetype s_grdsuspicious1;
extern  statetype s_grdsuspicious1s;
extern  statetype s_grdsuspicious2;
extern  statetype s_grdsuspicious3;
extern  statetype s_grdsuspicious3s;
extern  statetype s_grdsuspicious4;

extern  statetype s_ofcsuspicious1;
extern  statetype s_ofcsuspicious1s;
extern  statetype s_ofcsuspicious2;
extern  statetype s_ofcsuspicious3;
extern  statetype s_ofcsuspicious3s;
extern  statetype s_ofcsuspicious4;

extern  statetype s_mutsuspicious1;
extern  statetype s_mutsuspicious1s;
extern  statetype s_mutsuspicious2;
extern  statetype s_mutsuspicious3;
extern  statetype s_mutsuspicious3s;
extern  statetype s_mutsuspicious4;

extern  statetype s_sssuspicious1;
extern  statetype s_sssuspicious1s;
extern  statetype s_sssuspicious2;
extern  statetype s_sssuspicious3;
extern  statetype s_sssuspicious3s;
extern  statetype s_sssuspicious4;

extern  statetype s_grdpain;
extern  statetype s_grdpain1;
extern  statetype s_grdsuspiciouspain;
extern  statetype s_grdsuspiciouspain1;
extern  statetype s_ofcpain;
extern  statetype s_ofcpain1;
extern  statetype s_ofcsuspiciouspain;
extern  statetype s_ofcsuspiciouspain1;
extern  statetype s_sspain;
extern  statetype s_sspain1;
extern  statetype s_sssuspiciouspain;
extern  statetype s_sssuspiciouspain1;
extern  statetype s_mutpain;
extern  statetype s_mutpain1;
extern  statetype s_mutsuspiciouspain;
extern  statetype s_mutsuspiciouspain1;

extern  statetype s_deathcam;

extern  statetype s_schabbdeathcam2;
extern  statetype s_hitlerdeathcam2;
extern  statetype s_giftdeathcam2;
extern  statetype s_fatdeathcam2;

extern statetype s_willshoot1;
extern statetype s_willchase1s;

void SpawnStand (enemy_t which, int tilex, int tiley, int dir);
void SpawnPatrol (enemy_t which, int tilex, int tiley, int dir);

void SpawnDeadGuard (int tilex, int tiley);
void SpawnBoss (int tilex, int tiley);
void SpawnGretel (int tilex, int tiley);
void SpawnTrans (int tilex, int tiley);
void SpawnUber (int tilex, int tiley);
void SpawnWill (int tilex, int tiley);
void SpawnDeath (int tilex, int tiley);
void SpawnAngel (int tilex, int tiley);
void SpawnSpectre (int tilex, int tiley);
void SpawnGhosts (int which, int tilex, int tiley);
void SpawnSchabbs (int tilex, int tiley);
void SpawnGift (int tilex, int tiley);
void SpawnFat (int tilex, int tiley);
void SpawnFakeHitler (int tilex, int tiley);
void SpawnHitler (int tilex, int tiley);

void A_DeathScream (objtype *ob);
void SpawnBJVictory (void);

void T_Aim (objtype *ob);
void T_Aim2 (objtype *ob);

boolean ProjectileTryMove (objtype *ob, fixed movex, fixed movey);

//
// generic
//

void Object_ChangeState(objtype *ob, statetype *state);

short Object_GetShapeNum(objtype *ob);

void Object_RemoveShootable(objtype *ob);

int Object_TileDist(objtype *ob1, objtype *ob2);

bool Object_InShootDelta(objtype *ob);

bool Object_CheckUsed(objtype *ob);

void Object_LogDiscScreens(objtype *ob, const char *action, 
    bool perLevel);

bool Object_InState(objtype *ob, int stateIndex);

void Object_NewState(objtype *ob, int stateIndex);

objtype *Object_ProjShooter(objtype *ob);

/*
=============================================================================

                             WL_ANYACTOR DEFINITIONS

=============================================================================
*/

#define ANYACTOR(tile) (*AnyActor_Get(tile))
#define ANYACTOR_ISA(ob, code) (ob->tile == code)
#define ANYACTOR_MAXTILE 2048

#define ANYACTOR_SWITCH_HOOK(obc, obt, fn, ...) \
    obclass = obc; \
    if (ANYACTOR(obt).fn != NULL) \
    { \
        ANYACTOR(obt).fn(__VA_ARGS__); \
    } \
    else \
    { \
        if (ANYACTOR(obt).actorBaseObClass != nothing) \
        { \
            obclass = ANYACTOR(obt).actorBaseObClass; \
        } \
    } \
    switch (obclass)

typedef enum AnyActor_Tile_s
{
    ANYACTOR_TILE_NONE = 0,
} AnyActor_Tile_t;

typedef struct AnyActor_s
{
    statetype *(*actorStates)(void);
    void (*actorDeathScream)(objtype *ob);
    void (*actorLaunch)(objtype *ob, objtype *proj);
    void (*actorDoAttackState)(objtype *ob);
    bool (*actorIsBetterShot)(objtype *ob);
    void (*actorPlayShootSound)(objtype *ob);
    void (*actorBiteTakeDamage)(objtype *ob);
    void (*actorDoShockState)(objtype *ob);
    void (*actorSpawn)(int x, int y);
    void (*actorKill)(objtype *ob);
    bool (*actorDamageActor)(objtype *ob, int damage, int dmgtype);
    void (*actorDoPainState)(objtype *ob);
    void (*actorDoChaseState)(objtype *ob);
    void (*actorReactionTime)(objtype *ob);
    void (*actorNewHealthBar)(objtype *ob);
    const char *actorLogFileAlias;
    int actorSprOffset;
    bool actorIsHobo;
    classtype actorBaseObClass;
} AnyActor_t;

extern void AnyActor_Init(void);

extern bool AnyActor_IsReg(int tile);

extern AnyActor_t *AnyActor_Get(int tile);

extern void AnyActor_ReactionTime(objtype *ob);

/*
=============================================================================

                             WL_POLYGON DEFINITIONS

=============================================================================
*/

/* Describes a single point (used for a single vertex) */
struct PolyPoint {
   int X;   /* X coordinate */
   int Y;   /* Y coordinate */
};

/* Describes a series of points (used to store a list of vertices that
   describe a polygon; each vertex is assumed to connect to the two
   adjacent vertices, and the last vertex is assumed to connect to the
   first) */
struct PointListHeader {
   int Length;                /* # of points */
   struct PolyPoint * PointPtr;   /* pointer to list of points */
};

/* Describes the beginning and ending X coordinates of a single
   horizontal line */
struct HLine {
   int XStart; /* X coordinate of leftmost pixel in line */
   int XEnd;   /* X coordinate of rightmost pixel in line */
};

/* Describes a Length-long series of horizontal lines, all assumed to
   be on contiguous scan lines starting at YStart and proceeding
   downward (used to describe a scan-converted polygon to the
   low-level hardware-dependent drawing code) */
struct HLineList {
   int Length;                /* # of horizontal lines */
   int YStart;                /* Y coordinate of topmost line */
   struct HLine * HLinePtr;   /* pointer to list of horz lines */
};

extern int FillConvexPolygon(struct PointListHeader * VertexList, 
    int Color, int XOffset, int YOffset, byte Blend);

extern int FillConvexPolygonEx(int Length, 
    struct PolyPoint *PointPtr, int Color, byte Blend);

/*
=============================================================================

                             WL_LED DEFINITIONS

=============================================================================
*/

void LedDraw(void);

/*
=============================================================================

                             WL_HEALTHMETER DEFINITIONS

=============================================================================
*/

int HealthMeter_NewBar(void);

void HealthMeter_DeleteBar(int barId);

void HealthMeter_Reset(void);

void HealthMeter_SetBarFlashing(int barId, bool flashing);

void HealthMeter_Draw(void);

int HealthMeter_NewBarEnemy(objtype *ob, int shapenum, int cropx,
    int cropy, int cropw, int croph);

int32_t HealthMeter_SaveTheGame(FILE *file, int32_t checksum);

int32_t HealthMeter_LoadTheGame(FILE *file, int32_t checksum);

/*
=============================================================================

                             WL_TEXT DEFINITIONS

=============================================================================
*/

typedef enum EndText_Type_s
{
    ENDTEXT_TYPE_LEVEL,
    ENDTEXT_TYPE_VICTORY,
    ENDTEXT_TYPE_NEWGAME,
} EndText_Type_t;

#define VaStringSize 128
typedef char    VaString_t[VaStringSize];

extern  char    helpfilename[],endfilename[];

extern  void    HelpScreens(void);
extern  void    EndText(const EndText_Type_t type);
#ifdef HAVOC
extern  void    LogDiscScreensEx(const char* choice, bool perLevel);
extern  void    LogDiscScreens(const char* choice);
extern  void    LogDiscScreensNoLev(const char* choice);
#endif

extern char     *VaString(VaString_t string, const char *format, ...);

/*
=============================================================================

                             WL_AI DEFINITIONS

=============================================================================
*/

namespace WL_AI_NoiseFlags
{
    enum e
    {
        distanceFalloff = 0x01,
        suspicionNotCapped = 0x02,
    };
};

namespace WL_AI_Constants
{
    extern const double gunLoudness;
};

typedef struct WL_AI_s
{
    lwlib_IntMap_t enemyMap;
} WL_AI_t;

extern WL_AI_t ai;

void WL_AI_InitEnemyMap(WL_AI_t *ai);

void WL_AI_FreeEnemyMap(WL_AI_t *ai);

void WL_AI_Update(WL_AI_t *ai);

void WL_AI_DoDamage(WL_AI_t *ai, objtype *ob, int damage, 
    int attackerSpot);

void WL_AI_DoKill(WL_AI_t *ai, objtype *ob);

void WL_AI_MakeNoise(WL_AI_t *ai, double loudness, int noiseSpot,
    int noiseFlags);

bool WL_AI_IsSuspicious(WL_AI_t *ai, objtype *ob);

bool WL_AI_SuspicionTargetIsPlayer(WL_AI_t *ai, objtype *ob);

/*
=============================================================================

                               GP2X DEFINITIONS

=============================================================================
*/

#if defined(GP2X)

#if defined(GP2X_940)
void GP2X_MemoryInit(void);
void GP2X_Shutdown(void);
#endif
void GP2X_ButtonDown(int button);
void GP2X_ButtonUp(int button);

#endif


/*
=============================================================================

                             MISC DEFINITIONS

=============================================================================
*/

#define FP(x) ((x) << TILESHIFT)
#define FP_TRUNC(x) ((x) >> 16)
#define FP_ONE 0x10000l
#define FP_HALF 0x8000l
#define FP_SQ(x) fixedpt_mul(x,x)

#define TILE_IN_MAP(x, y) (((x) >= 0 && (x) < MAPSIZE) && ((y) >= 0 && (y) < MAPSIZE))

static inline fixed FixedMul(fixed a, fixed b)
{
	return (fixed)(((int64_t)a * b + 0x8000) >> 16);
}

static inline fixed FixedLerp(fixed a, fixed b, fixed t)
{
    return a == b ? a : fixedpt_mul(a, FP(1) - t) + fixedpt_mul(b, t);
}


#ifdef PLAYDEMOLIKEORIGINAL
    #define DEMOCHOOSE_ORIG_SDL(orig, sdl) ((demorecord || demoplayback) ? (orig) : (sdl))
    #define DEMOCOND_ORIG                  (demorecord || demoplayback)
    #define DEMOIF_SDL                     if(DEMOCOND_SDL)
#else
    #define DEMOCHOOSE_ORIG_SDL(orig, sdl) (sdl)
    #define DEMOCOND_ORIG                  false
    #define DEMOIF_SDL
#endif
#define DEMOCOND_SDL                   (!DEMOCOND_ORIG)

#define GetTicks(ticrate) ((SDL_GetTicks()*ticrate)/100)

#define ISPOINTER(x) ((((uintptr_t)(x)) & ~0xffff) != 0)

#define CHECKMALLOCRESULT(x) if(!(x)) Quit("Out of memory at %s:%i", __FILE__, __LINE__)

#ifdef _WIN32
    #define strcasecmp stricmp
    #define strncasecmp strnicmp
    #define snprintf _snprintf
#else
    static inline char* itoa(int value, char* string, int radix)
    {
	    sprintf(string, "%d", value);
	    return string;
    }

    static inline char* ltoa(long value, char* string, int radix)
    {
	    sprintf(string, "%ld", value);
	    return string;
    }
#endif

#define lengthof(x) (sizeof(x) / sizeof(*(x)))
#define endof(x)    ((x) + lengthof(x))

static inline word READWORD(byte *&ptr)
{
    word val = ptr[0] | ptr[1] << 8;
    ptr += 2;
    return val;
}

static inline longword READLONGWORD(byte *&ptr)
{
    longword val = ptr[0] | ptr[1] << 8 | ptr[2] << 16 | ptr[3] << 24;
    ptr += 4;
    return val;
}

#if 0
#define FRACINIT(x, argVal, argResidual, argNumerator, argDivider) \
do { \
    memset(&(x), 0, sizeof(x)); \
    (x).val = argVal; \
    (x).residual = argResidual; \
    (x).numerator = argNumerator; \
    (x).divider = argDivider; \
} while (0)

#define FRACINC(x) \
do { \
    (x).residual += (x).numerator; \
    while ((x).residual >= (x).divider) \
    { \
        (x).val++; \
        (x).residual -= (x).divider; \
    } \
} while (0)

typedef struct Frac_s
{
    int val;
    int residual;
    int numerator, divider;
} Frac_t;
#endif

/*
=============================================================================

                           FEATURE DEFINITIONS

=============================================================================
*/

#ifdef USE_FEATUREFLAGS
    // The currently available feature flags
    #define FF_STARSKY      0x0001
    #define FF_PARALLAXSKY  0x0002
    #define FF_CLOUDSKY     0x0004
    #define FF_RAIN         0x0010
    #define FF_SNOW         0x0020

    // The ffData... variables contain the 16-bit values of the according corners of the current level.
    // The corners are overwritten with adjacent tiles after initialization in SetupGameLevel
    // to avoid interpretation as e.g. doors.
    extern int ffDataTopLeft, ffDataTopRight, ffDataBottomLeft, ffDataBottomRight;

    /*************************************************************
     * Current usage of ffData... variables:
     * ffDataTopLeft:     lower 8-bit: ShadeDefID
     * ffDataTopRight:    FeatureFlags
     * ffDataBottomLeft:  CloudSkyDefID or ParallaxStartTexture
     * ffDataBottomRight: DoorStartTexture
     *************************************************************/

    // The feature flags are stored as a wall in the upper right corner of each level
    static inline word GetFeatureFlags()
    {
        return ffDataTopRight;
    }

#endif

#ifdef USE_FLOORCEILINGTEX
    void DrawFloorAndCeiling(VBuf_t vbuf, int min_wallheight);
#endif

#ifdef USE_PARALLAX
    void DrawParallax(VBuf_t vbuf);
#endif

#ifdef USE_DIR3DSPR
    void Scale3DShape(VBuf_t vbuf, statobj_t *ob, byte blend);
#endif

#ifdef PROFILE_RENDERER
#define SDL_Delay(x)
#endif

void ShapeToImage(int shapenum, byte *image, bool flip, 
    t_compshape *shape);
t_compshape *BuildSprite(SDL_Surface *surf);

#endif
