#!/bin/bash

while getopts "u" Option
do
	case $Option in
		u) uppercase=y ;; 
		*) echo "Option not implemented"; continue ;;
	esac
done

shift $(($OPTIND - 1))

for f in $*; do
	base=`basename $f`
	dir=`dirname $f`
	if [ "$uppercase" ]; then 
		newbase=`echo "$base" | tr 'a-z' 'A-Z'`
	else
		newbase=`echo "$base" | tr 'A-Z' 'a-z'`
	fi
	mv "$f" "$dir/$newbase"
done
