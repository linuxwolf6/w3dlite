Player Sounds
{
    HITWALLSND - Player walks into wall.
    NOWAYSND - Player cannot open locked door.
    PLAYERDEATHSND - Player dies.
    HEARTBEATSND - Player has low health.
    NOTENOUGHCOINSSND - Not enough coins to make purchase.
    ACTIVATENIGHTVISIONSND - Batman switches on his night vision goggles.

    [unused]
    NAZIHITPLAYERSND - Nazi player dies?
    TAKEDAMAGESND - Player takes damage.
    GAMEOVERSND - Run out of lives?
    DONOTHINGSND - Use key pressed in open area.
    SLURPIESND - Drink blood.
}

Enemy Sounds
{
    HALTSND - Armed Hobo sights player.
    DOGBARKSND - Knife Hobo sights player.
    DEATHSCREAM1SND - Random Armed/Knife Hobo death scream.
    DEATHSCREAM2SND - Random Armed/Knife Hobo death scream.
    DEATHSCREAM3SND - Random Armed/Knife Hobo death scream.
    DEATHSCREAM4SND - Random Armed/Knife Hobo death scream.
    DEATHSCREAM5SND - Random Armed/Knife Hobo death scream.
    DEATHSCREAM6SND - Random Armed/Knife Hobo death scream.
    AHHHGSND - Armed Thug death scream.
    LEBENSND - Mercenary death scream.
    NAZIFIRESND - Enemy fires pistol.
    NEINSOVASSND - Riddler Goon death scream.
    DOGATTACKSND - Knife Hobo attack.
    SSFIRESND - Mercenary machine gun attack.
    ELECTRICSHOCKSND - Sound played 8 times when enemy is electrocuted with the EMP gun.
    HOBOAWAKESND - Hobo sleeping on floor or park bench is alerted to Batman's presence.
    OFCSUSPICIONSND - Riddler Goon becomes suspicious.
    MUTSUSPICIONSND - Armed Thug becomes suspicious.
    SSSUSPICIONSND - Mercenary becomes suspicious.
    BMBGSUSPICIONSND - Black Mask Body Guard becomes suspicious.
    OFCPLAYERSUSPICIONSND - Riddler Goon suspects Batman is near.
    MUTPLAYERSUSPICIONSND - Armed Thug suspects Batman is near.
    SSPLAYERSUSPICIONSND - Mercenary suspects Batman is near.
    BMBGPLAYERSUSPICIONSND - Black Mask Body Guard suspects Batman is near.
    OFCALARMEDSND - Riddler Goon spots Batman and becomes alarmed.
    MUTALARMEDSND - Armed Thug spots Batman and becomes alarmed.
    SSALARMEDSND - Mercenary spots Batman and becomes alarmed.
    BMBGALARMEDSND - Black Mask Body Guard spots Batman and becomes alarmed.
    OFCLOSESIGHTSND - Riddler Goon loses sight of Batman.
    MUTLOSESIGHTSND - Armed Thug loses sight of Batman.
    SSLOSESIGHTSND - Mercenary loses sight of Batman.
    BMBGLOSESIGHTSND - Black Mask Body Guard loses sight of Batman.

    [unused]
    DOGDEATHSND - Dog death.
    WALK1SND - Enemy patrol walk?
    WALK2SND - Enemy patrol walk?
    DEATHSCREAM7SND - Random Wolf3D Guard death scream.
    DEATHSCREAM8SND - Random Wolf3D Guard death scream.
    DEATHSCREAM9SND - Random Wolf3D Guard death scream.
    HITENEMYSND - Enemy is struck by player attack.
    SCHUTZADSND - Enemy sights player.
    SPIONSND - Enemy sights player.
}

Weapon Sounds
{
    ATKGATLINGSND - Player fires Taser.
    ATKKNIFESND - Player throws Punch.
    ATKPISTOLSND - Player throws Leg Rope.
    ATKMACHINEGUNSND - Player throws Batarang.
    ATKTASERSND - Stealthy taser attack from behind enemy.
    BATARANGBREAKSND - Batarang hits a wall or floor and breaks apart.
    BATARANGKILLSND - Batarang hits and knocks out an unalerted enemy.
    BATARANGCATCHSND - Batarang is caught by Batman on its return journey.
    BATARANGHITSND - Batarang hits an enemy in attack mode.
    ATKEMPGUNSND - Batman fires the EMP gun.
    EMPHITSND - EMP blast from hitting wall, enemy or other solid object.
    EMPGUNLOWPOWERSND - Batman tries to shoot EMP gun while it is still recharging.
    EMPGUNRECHARGESND - EMP gun is fully recharged and can be fired again.
    LEGROPEHITSND - Leg rope projectile strikes an enemy.
    LEGROPEBREAKSND - Leg rope projectile strikes the wall or floor.
    LIDBREAKSND - The trash can lid breaks apart from gun fire.
    LIDTHROWSND - Batman throws the trash can lid at an enemy.
    LIDDAMAGESND1 - Trash can lid is struck by gun fire (variation #1).
    LIDDAMAGESND2 - Trash can lid is struck by gun fire (variation #2).
    LIDDAMAGESND3 - Trash can lid is struck by gun fire (variation #3).
    LIDDAMAGEHALFSND - Trash can lid reaches half way to being completely destroyed.
    TRASHCANLIDPROJHITSND - Trash can lid hits an enemy while flying through the air.
    TRASHCANLIDPROJBREAKSND - Trash can lid hits a wall while flying through the air.
    SEWERLIDDAMAGESND1 - Sewer lid is struck by gun fire (variation #1).
    SEWERLIDDAMAGESND2 - Sewer lid is struck by gun fire (variation #2).
    SEWERLIDDAMAGESND3 - Sewer lid is struck by gun fire (variation #3).
}

Boss Sounds
{
    GHOSTSIGHTSND - Clayface spawn sights player.
    MISSILEFIRESND - Boss missile shoot.
    GHOSTFADESND - Clayface Spawn death scream.
    ANGELSIGHTSND - Zsasz sights player.
    ANGELDEATHSND - Zsasz death scream.
    ANGELTIREDSND - Zsasz tired.
    ANGELFIRESND - Clayface Spawn launches projectile.
    TRANSSIGHTSND - Fire Fly sights player.
    TRANSDEATHSND - Fire Fly death scream.
    WILHELMSIGHTSND - Clock King sights player.
    WILHELMDEATHSND - Clock King death scream.
    UBERDEATHSND - Riddler death scream.
    KNIGHTSIGHTSND - Clayface sights player.
    KNIGHTDEATHSND - Clayface death scream.
    KNIGHTMISSILESND - Clayface launches projectile.
    STRANGESIGHTSND - Black Mask sights player.
    STRANGEDEATHSND - Black Mask death scream.
    STRANGETHROWSND - Black Mask launches projectile.
    TWOFACESIGHTSND - Two Face sights player.
    TWOFACEDEATHSND - Two Face death scream.
    TWOFACESHOOTSND - Two Face launches projectile.
    BMBGDEATHSND - Black Mask Body Guard is subdued by Batman.
    BMBGFIRESND - Black Mask Body Guard shoots his weapon.
    BMBGSIGHTSND - Black Mask Body Guard is alerted to Batman's presence.
    SCARECROWDEATHSND - Scare Crow boss is subdued by Batman.
    SCARECROWTHROWSND - Scare Crow boss fires his gas projectile weapon.
    SCARECROWSIGHTSND - Scare Crow is alerted to Batman's presence.
    MISSILEHITSND - Boss projectile collision.
    CLOCKKINGSHOOTSND - Clock King fires clock projectile.
    CLOCKPROJHITSND - Clock projectile hits wall.
    TIMEWARPFASTSND - Time warp is activated by Clock King.
    TIMEWARPSLOWSND - Clock King's time warp expires.
    FLAMEPROJBREAKSND - Fire Fly's flaming projectile hits wall and burns out.
    FIREFLYSHOOTSND - Fire Fly shoots a flaming projectile.
    FLAMEPROJHITSND - Fire Fly's flaming projectile hits and burns Batman.

    [unused]
    BOSSACTIVESND - Boss activated.
    BOSSFIRESND - Boss machine gun attack.
}

Item Sounds
{
    GETKEYSND - Key pickup.
    GETAMMOSND - Leg Rope ammo pickup.
    GETMACHINESND - Batarang weapon/ammo pickup.
    GETGATLINGSND - Taser weapon pickup.
    HEALTH1SND - Small health pickup or vending machine used.
    HEALTH2SND - First aid pickup.
    BONUS1SND - Batman treasure pickup.
    BONUS2SND - Clock King hat treasure pickup.
    BONUS3SND - Clock King spear treasure pickup.
    BONUS4SND - Clock King clock treasure pickup.
    BONUS1UPSND - Full heal pickup.
    GETAMMOBOXSND - Taser ammo pickup.
    GETSPEARSND - Spear of Destiny pickup. [unused]
    GETAMMOBELTSND - Ammo belt pickup.
    COLLECTCOINSSND - Batman is credited with coins.
    GETEMPGUNSND - Batman picks up the EMP gun.
    GETEMPAMMOSND - Batman picks up EMP gun emmo.
    GETARMORSND - Batman acquires armor.
    GETLIDSND - Batman picks up the trash can lid.

    [unused]
    NOITEMSND - No item to pickup?
}

Menu Sounds
{
    MOVEGUN2SND - Cursor movement.
    MOVEGUN1SND - Cursor movement.
    SHOOTSND - Menu selected.
    SHOOTDOORSND - Mouse/joystick controls confirmed.
    ESCPRESSEDSND - Menu back navigation.
    ENDBONUS1SND - Time bonus incremented in intermission screen.
    ENDBONUS2SND - End of time bonus in intermission screen.
    NOBONUSSND - No bonus awarded in intermission screen.
    PERCENT100SND - 100% ratio achieved in intermission screen.

    [unused]
    SELECTITEMSND - Select menu item?
}

World Sounds
{
    OPENDOORSND - Door open.
    CLOSEDOORSND - Door closed.
    PUSHWALLSND - Push wall activated.
    AMBIENTSND1 - Sewer ambient.
    AMBIENTSND2 - Subway Ambient.
    AMBIENTSND3 - Police Siren.
    AMBIENTSND4 - Riot Ambient.
    AMBIENTSND5 - Bane Chant.
    FRIDGEOPENSND - Fridge door open.
    FRIDGEEATSND - Player eats foot from fridge.
    TRASHCANDAMAGESND - TrashCan damaged but not destroyed yet. Applies to regular and flaming TrashCan.
    TRASHCANBREAKSND - TrashCan destroyed into pieces. Applies to regular, flaming and toxic TrashCan.
    BARRELBREAKSND - Barrel destroyed into pieces. Applies to explosive and toxic Barrels.
    TROLLEYBREAKSND - Trolley destroyed into pieces.
    PORTALSND - Player is teleported to another part of the map (e.g., from the streets to the inside of a shop).
    FOOTSTEPSND - Generic foot step sound.

    [unused]
    LEVELDONESND - Elevator switch.
}

Character Sounds
{
    SLUTSAUTHORIZEDSND - Pimp grants Batman access to talk to hookers.
    SEEMYPIMPSND - Batman tries to talk to hooker before seeing her Pimp.
    USEHOOKERSND - Batman pays hooker and she grants Batman a hint.
}
