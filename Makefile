CONFIG ?= config.default
-include $(CONFIG)

ifeq ($(WOLF4SDL_ARCH_X64),y)
CFLAGS ?= -g -m32 -O0
#LDFLAGS ?= -L/usr/lib -m32
LDFLAGS ?= -L/usr/lib32 -m32
else
CFLAGS += -g -O0
endif

ifeq ($(WOLF4SDL_RGB_LSB_FIRST),y)
CFLAGS += -DRGB_LSB_FIRST
endif

VERSION_WOLF3D := y
#VERSION_SPEAR := y
#VERSION_WOLF3D_SHAREWARE := y
#VERSION_WOLF3D_APOGEE := y
#VERSION_SPEAR_DEMO := y

ifeq ($(VERSION_WOLF3D_SHAREWARE),y)
GAME_VERSION_ID := wolf3d
CFLAGS += -DVERSION_WOLF3D_SHAREWARE
CHEATOPTION := --goobers
else
ifeq ($(VERSION_SPEAR),y)
GAME_VERSION_ID := sod
CFLAGS += -DVERSION_SPEAR
CHEATOPTION := --debugmode
else
ifeq ($(VERSION_WOLF3D_APOGEE),y)
GAME_VERSION_ID := wolf3d_apogee
CFLAGS += -DVERSION_WOLF3D_APOGEE
CHEATOPTION := --goobers
else
ifeq ($(VERSION_SPEAR_DEMO),y)
GAME_VERSION_ID := spear_demo
CFLAGS += -DVERSION_SPEAR_DEMO
CHEATOPTION := --debugmode
else
GAME_VERSION_ID := wolf3d_full
CFLAGS += -DVERSION_WOLF3D
CHEATOPTION := --goobers
endif
endif
endif
endif

BINARY    ?= wolf3d
PREFIX    ?= /usr/local
MANPREFIX ?= $(PREFIX)

INSTALL         ?= install
INSTALL_PROGRAM ?= $(INSTALL) -m 555 -s
INSTALL_MAN     ?= $(INSTALL) -m 444
INSTALL_DATA    ?= $(INSTALL) -m 444

SDL_CONFIG    ?= sdl-config
ifeq ($(WOLF4SDL_ARCH_X64),y)
CFLAGS_SDL    ?= $(shell $(SDL_CONFIG) --cflags)
LDFLAGS_SDL   ?= -L/usr/lib/i386-linux-gnu -lSDL-1.2 -lSDL_mixer-1.2
else
CFLAGS_SDL    ?= $(shell $(SDL_CONFIG) --cflags)
LDFLAGS_SDL   ?= $(shell $(SDL_CONFIG) --libs)
endif
ZIP_PACKAGE   ?= batsource.zip
RES           ?= --res 320 200
#RES           ?= --res 640 400
#RES           ?= --res 960 600
RUN_GAME_ARGS += --windowed#-mouse
RUN_GAME_ARGS += $(RES)
#RUN_GAME_ARGS += --windowed $(RES)
RUN_GAME_ARGS += $(CHEATOPTION)
RUN_GAME_ARGS += --joystick -1 
RUN_GAME_ARGS += #--nodblbuf
RUN_GAME_ARGS += --nowait --tedlevel 0 --normal
#RUN_GAME_ARGS += --nosound
#RUN_GAME_ARGS += --noenemies
RUN_GAME_ARGS += --nofade
#VALGRIND      := valgrind -v

#RUN_GAME_ARGS += --fakedblbuf
DROPBOX_DIR   ?= $(HOME)/Dropbox
#VBOX_PROJ     ?= /media/sf_Work/ron_wolf4sdl
VBOX_PROJ     ?= /media/USB\ DISK/ron_wolf4sdl

RUN_GAME_DIR  ?= games/$(GAME_VERSION_ID)
DROPBOX_GAME_DIR  ?= $(DROPBOX_DIR)/batman pack/Sequel 1/Latest GameFiles
DROPBOX_SRC_DIR   ?= $(DROPBOX_DIR)/batman pack/linuxwolf_srcs

CFLAGS += $(CFLAGS_SDL)

#CFLAGS += -Wall
#CFLAGS += -W
CFLAGS += -Wpointer-arith
CFLAGS += -Wreturn-type
CFLAGS += -Wwrite-strings
CFLAGS += -Wcast-align
CFLAGS += -DINJECT_TEST_OBJS
CFLAGS += -DLWLIB_LOGPRINTF_STDOUT
CFLAGS += -DAI_DEBUGGING

ifeq ($(WOLF4SDL_PROFILING),y)
CFLAGS += -pg
endif

CCFLAGS += $(CFLAGS)
CCFLAGS += -std=gnu99
CCFLAGS += -Werror-implicit-function-declaration
CCFLAGS += -Wimplicit-int
CCFLAGS += -Wsequence-point

CXXFLAGS += $(CFLAGS)

LDFLAGS += $(LDFLAGS_SDL)
ifeq ($(WOLF4SDL_ARCH_X64),y)
LDFLAGS += -L/usr/lib/i386-linux-gnu/mesa
else
LDFLAGS += -lSDL_mixer
endif

SRCS :=
SRCS += fmopl.cpp
SRCS += id_ca.cpp
SRCS += id_in.cpp
SRCS += id_pm.cpp
SRCS += id_sd.cpp
SRCS += id_us_1.cpp
SRCS += id_vh.cpp
SRCS += id_vl.cpp
SRCS += signon.cpp
SRCS += wl_act1.cpp
SRCS += wl_act2.cpp
SRCS += wl_agent.cpp
SRCS += wl_atmos.cpp
SRCS += wl_cloudsky.cpp
SRCS += wl_debug.cpp
SRCS += wl_draw.cpp
SRCS += wl_floorceiling.cpp
SRCS += wl_game.cpp
SRCS += wl_inter.cpp
SRCS += wl_main.cpp
SRCS += wl_menu.cpp
SRCS += wl_parallax.cpp
SRCS += wl_play.cpp
SRCS += wl_state.cpp
SRCS += wl_text.cpp
SRCS += wl_shade.cpp
SRCS += wl_dir3dspr.cpp
SRCS += wl_anyactor.cpp
ifeq ($(VERSION_BATMAN),y)
SRCS += wl_hooker.cpp
SRCS += wl_pimp.cpp
SRCS += wl_conartist.cpp
SRCS += wl_sleephobo.cpp
SRCS += wl_benchhobo.cpp
SRCS += wl_clockking.cpp
SRCS += wl_healthmeter.cpp
SRCS += wl_firefly.cpp
SRCS += wl_led.cpp
endif
SRCS += wl_physics.cpp
SRCS += wl_polygon.cpp
SRCS += wl_math.cpp
SRCS += wl_ai.cpp
SRCS += wl_ed.cpp
SRCS += wr_lightmap.cpp
SRCS += wr_rad.cpp
SRCS += wr_radmap.cpp
SRCS += wr_raycaster.cpp
SRCS += wr_room.cpp
SRCS += wr_scene.cpp
SRCS += wr_level.cpp
SRCS += wr_lightinfo.cpp
SRCS += wolfrad.cpp
SRCS += lw_ctx.cpp
SRCS += lw_protmsg.cpp
SRCS += lw_vec.cpp
SRCS += lw_misc.cpp
SRCS += lw_bres.cpp
SRCS += lw_maptool.cpp
SRCS += lw_fs.cpp
SRCS += lw_edit.cpp
SRCS += ai_enemy.cpp
SRCS += pwscan.cpp

DEPS = $(filter %.d, $(SRCS:.c=.d) $(SRCS:.cpp=.d))
OBJS = $(filter %.o, $(SRCS:.c=.o) $(SRCS:.cpp=.o))

.SUFFIXES:
.SUFFIXES: .c .cpp .d .o

Q ?= @

all: $(BINARY)

ifndef NO_DEPS
depend: $(DEPS)

ifeq ($(findstring $(MAKECMDGOALS), clean depend Data latest zip to_dropbox listsrc to_vboxshare from_vboxshare),)
-include $(DEPS)
endif
endif

$(BINARY): $(OBJS)
	@echo '===> LD $@'
	$(Q)$(CXX) $(CFLAGS) $(OBJS) $(LDFLAGS) -o $@

.c.o:
	@echo '===> CC $<'
	$(Q)$(CC) $(CCFLAGS) -c $< -o $@

.cpp.o:
	@echo '===> CXX $<'
	$(Q)$(CXX) $(CXXFLAGS) -c $< -o $@

.c.d:
	@echo '===> DEP $<'
	$(Q)$(CC) $(CCFLAGS) -MM $< | sed 's#^$(@F:%.d=%.o):#$@ $(@:%.d=%.o):#' > $@

.cpp.d:
	@echo '===> DEP $<'
	$(Q)$(CXX) $(CXXFLAGS) -MM $< | sed 's#^$(@F:%.d=%.o):#$@ $(@:%.d=%.o):#' > $@

clean distclean:
	@echo '===> CLEAN'
	$(Q)rm -fr $(DEPS) $(OBJS) $(BINARY)

install: $(BINARY)
	@echo '===> INSTALL'
	$(Q)$(INSTALL) -d $(PREFIX)/bin
	$(Q)$(INSTALL_PROGRAM) $(BINARY) $(PREFIX)/bin

run: $(BINARY)
	( cd $(RUN_GAME_DIR); $(VALGRIND) $(PWD)/wolf3d $(RUN_GAME_ARGS) )

debug-run: $(BINARY)
	( cd $(RUN_GAME_DIR); gdb --args $(PWD)/wolf3d $(RUN_GAME_ARGS) )

zip: clean
	rm -f "$(ZIP_PACKAGE)"
	zip -r "$(ZIP_PACKAGE)" *

latest:
	rm -rf latest
	cp -a "$(DROPBOX_GAME_DIR)" latest
	sh scripts/lcase.sh latest/*.SOD
	diff -rq $(RUN_GAME_DIR) latest

to_dropbox:
	cp $(RUN_GAME_DIR)/Wolf4SDL.exe "$(DROPBOX_GAME_DIR)"
	#$(MAKE) ZIP_PACKAGE="$(DROPBOX_SRC_DIR)/lwolf_batsource$(NUM).zip" zip

listsrc:
	ls "$(DROPBOX_SRC_DIR)"

profile:
	( cd $(RUN_GAME_DIR); gprof $(shell pwd)/wolf3d )

to_vboxshare:
	rm -rf $(VBOX_PROJ)
	mkdir -p $(VBOX_PROJ)
	cp -a * $(VBOX_PROJ)

from_vboxshare:
	cp $(VBOX_PROJ)/$(RUN_GAME_DIR)/Wolf4SDL.exe $(RUN_GAME_DIR)
	cp $(VBOX_PROJ)/Wolf4SDL.dev .
