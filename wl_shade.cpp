#include "version.h"

#ifdef USE_SHADING
#include "wl_def.h"
#include "wl_shade.h"
#include "fixedptc.h"
#include "lw_vec.h"

//============================================================

static LT_t lt =
{
    LT_DEFAULT_DISTANCE_ATTENUATION,     // distAttenuation
    40,                                  // distShadeLimit
    { 0 },                               // lights
    0,                                   // numLights
    0,                                   // lastLightIndex
    {
        { 0 },                           // LT_LIGHT_PREDEF_NONE
        { 
            {
                FP(7),                   // reach
                FP(1) / 6,               // strength
            },                           // cfg
        },                               // LT_LIGHT_PREDEF_MEDIUM
        { 
            {
                FP(7),                   // reach
                FP(1) / 5,               // strength
            },                           // cfg
        },                               // LT_LIGHT_PREDEF_HIGH
        { 
            {
                FP(9),                   // reach
                FP(1) / 3,               // strength
            },                           // cfg
        },                               // LT_LIGHT_PREDEF_EXTRAHIGH
        { 
            {
                FP(10),                  // reach
                -FP(1) / 2,              // strength
            },                           // cfg
        },                               // LT_LIGHT_PREDEF_EMPPROJ
        { 
            {
                FP(16),                  // reach
                FP(1) / 5,               // strength
                FP(1) / 5,               // ambient
                (LT_Light_AnonThink_t)LT_LightThinkFade, // think
            },                           // cfg
            {
                {
                    FP(16),              // reach
                    FP(1) / 25,          // strength
                    FP(0),               // ambient
                },                       // cfg
                15,                      // tics
                true,                    // destroyLight
            },                           // fade
        },                               // LT_LIGHT_PREDEF_MUZZLEFLASH
        { 
            {
                FP(4),                   // reach
                FP(1) / 10,              // strength
                FP(0),                   // ambient
                (LT_Light_AnonThink_t)LT_LightThinkJitter, // think
            },                           // cfg
            { 0 },                       // fade
            {
                {
                    FP(0),               // reach
                    FP(0),               // strength
                    FP(0),               // ambient
                    NULL,                // think
                    0,                   // cachedBrightnessTable
                    FP(1) / 8,           // x
                    FP(1) / 8,           // y
                },                       // cfg
                6,                       // tics
            },                           // jitter
        },                               // LT_LIGHT_PREDEF_BURNING_1
        { 
            {
                FP(5),                   // reach
                FP(1) / 10,              // strength
                FP(0),                   // ambient
                (LT_Light_AnonThink_t)LT_LightThinkJitter, // think
            },                           // cfg
            { 0 },                       // fade
            {
                {
                    FP(0),               // reach
                    FP(0),               // strength
                    FP(0),               // ambient
                    NULL,                // think
                    0,                   // cachedBrightnessTable
                    FP(1) / 15,          // x
                    FP(1) / 15,          // y
                },                       // cfg
                6,                       // tics
            },                           // jitter
        },                               // LT_LIGHT_PREDEF_BURNING_2
        { 
            {
                FP(8),                   // reach
                FP(1),                   // strength
                FP(0),                   // ambient
                (LT_Light_AnonThink_t)LT_LightThinkFade, // think
            },                           // cfg
            {
                {
                    FP(8),               // reach
                    FP(1) / 8,           // strength
                },                       // cfg
                35,                      // tics
                true,                    // destroyLight
            },                           // fade
        },                               // LT_LIGHT_PREDEF_EXPLODE
        { 
            {
                FP(8),                   // reach
                FP(2),                   // strength
                FP(0),                   // ambient
                (LT_Light_AnonThink_t)LT_LightThinkFade, // think
            },                           // cfg
            {
                {
                    FP(8),               // reach
                    FP(0),               // strength
                },                       // cfg
                35,                      // tics
                true,                    // destroyLight
            },                           // fade
        },                               // LT_LIGHT_PREDEF_FADE_DOWN
        { 
            {
                FP(6),                   // reach
                FP(1) / 3,               // strength
                FP(1) / 20,              // ambient
                (LT_Light_AnonThink_t)LT_LightThinkTaserFlash, // think
            },                           // cfg
        },                               // LT_LIGHT_PREDEF_TASERFLASH
        { 
            {
                FP(2),                   // reach
                FP(1) / 20,              // strength
                FP(0),                   // ambient
                (LT_Light_AnonThink_t)LT_LightThinkStrobe, // think
                LT_CACHED_BRIGHTNESS_TABLE_TREASURE, // cachedBrightnessTable
            },                           // cfg
            { 0 },                       // fade
            { 0 },                       // jitter
            {
                {
                    FP(0),               // reach
                    FP(1) / 8,           // strength
                },                       // cfg
                40,                      // tics
            },                           // strobe
        },                               // LT_LIGHT_PREDEF_TREASURE
    },                                   // predefCfgs
    false,                               // blackOutElectricLights
	false,								 // blackOutGlobalAmbient
	0,									 // ambient
};

LT_t *LT(void)
{
    return &lt;
}

void LT_Startup(void)
{
    int i;
    fixed x, y;
    fixed D, invD;
    int lighttilex, lighttiley;
    LT_Light_PredefCfg_t *predefCfg;
    LT_DistTable_t *distTable;

    distTable = &lt.cachedBrightnessTables[0];
    distTable->cells = lwlib_CallocMany(fixed, LT_DISTTABLE_SIZE);
    distTable->width = LT_DISTTABLE_WIDTH;
    distTable->widthShift = lwlib_Log2(LT_DISTTABLE_WIDTH);
    distTable->size = LT_DISTTABLE_SIZE;

    for (lighttilex = 0; lighttilex < LT_DISTTABLE_WIDTH; lighttilex++)
    {
        for (lighttiley = 0; lighttiley < lighttilex + 1; lighttiley++)
        {
            x = LIGHTTILE_CENTER(lighttilex);
            y = LIGHTTILE_CENTER(lighttiley);

            D = FP_SQ(x) + FP_SQ(y);

            invD = fixedpt_div(FP(1), D);
            invD = lwlib_MIN(invD, FP(256));

            LT_DISTTABLE_ACCESS(distTable, lighttilex, lighttiley) = 
                LT_DISTTABLE_ACCESS(distTable, lighttiley, lighttilex) =
                invD;
        }
    }

    for (i = 0; i < lwlib_CountArray(lt.predefCfgs); i++)
    {
        predefCfg = &lt.predefCfgs[i];
        predefCfg->cfg.halfReachSq = 
            FP_SQ(predefCfg->cfg.reach >> 1);
        predefCfg->cfg.invHalfReachSq = 
            fixedpt_div_checked(FP(1), predefCfg->cfg.halfReachSq);
    }
}

void LT_Shutdown(void)
{
    lwlib_IntMapClear(&lt.litSpriteMap);
}

void LT_ResetLights(void)
{
    int i;

    memset(lt.lights, 0, sizeof(lt.lights));
    lt.numLights = 0;
    lt.lastLightIndex = 0;
    lt.ambient = 0;

    for (i = 0; i < lwlib_CountArray(lt.tiles); i++)
    {
        vec_free(lt.tiles[i].lightNums, int);
    }
    lwlib_Zero(lt.tiles);
}

int LT_NewLight(void)
{
    int i;
    LT_Light_t *light;
    LT_Light_t *prev;

    if (lt.numLights >= LT_MAX_LIGHTS)
    {
        return 0;
    }
    lt.numLights++;

    i = lt.lastLightIndex;
    while (lt.lights[i].id != 0)
    {
        i++;
        if (i >= LT_MAX_LIGHTS)
        {
            i = 0;
        }
    }

    light = &lt.lights[i];
    memset(light, 0, sizeof(LT_Light_t));
    light->id = i + 1;

    if (lt.numLights > 1)
    {
        prev = &lt.lights[lt.lastLightIndex];
        prev->next = light;
        light->prev = prev;
    }

    lt.lastLightIndex = i;
    return light->id;
}

LT_Light_t *LT_GetLightById(int lightId)
{
    if (lightId <= 0 || lightId - 1 >= LT_MAX_LIGHTS)
    {
        return NULL;
    }
    return &lt.lights[lightId - 1];
}

void LT_DeleteLightEx(int lightId, bool markOnly)
{
    LT_Light_t *light;
    LT_Light_State_t *state;
    LT_Light_Cfg_t *cfg;

    light = LT_GetLightById(lightId);
    if (light == NULL)
    {
        return;
    }

    state = &light->state;
    cfg = &state->cfg;

    if (markOnly)
    {
        state->deleteMark = true;
        return;
    }

    if (cfg->cachedBrightnessTable == 0 && 
        light->brightnessTable != NULL)
    {
        if (light->brightnessTable->cells != NULL)
        {
            free(light->brightnessTable->cells);
        }
        free(light->brightnessTable);
    }

    if (light->prev != NULL)
    {
        if (lightId - 1 == lt.lastLightIndex)
        {
            lt.lastLightIndex = light->prev->id - 1;
        }

        light->prev->next = light->next;
        if (light->next != NULL)
        {
            light->next->prev = light->prev;
        }
    }
    else // removing head node
    {
        if (light->next != NULL)
        {
            light->next->prev = NULL;
        }
    }
    memset(light, 0, sizeof(LT_Light_t));

    assert(lt.numLights > 0);
    lt.numLights--;
}

LT_Spectra_t LT_ComputeLightTileIntensity(int lighttilex, int lighttiley)
{
    int i;
    int tilex, tiley;
    int distShade;
    fixed I, invD;
    LT_Light_t *light;
    LT_Light_Cfg_t *cfg;
    LT_Tile_t *tile;
    int ltx, lty;

    I = 0;

    assert(LIGHTTILE_IN_MAP(lighttilex, lighttiley));
    tilex = LIGHTTILE_TO_TILE(lighttilex);
    tiley = LIGHTTILE_TO_TILE(lighttiley);

    tile = &lt.tiles[(tiley << mapshift) + tilex];
    for (i = 0; i < vec_size(tile->lightNums); i++)
    {
        light = &lt.lights[vec_at_unchecked(tile->lightNums, i)];

        cfg = &light->state.cfg;
        if (cfg->disabled)
        {
            continue; 
        }

        ltx = abs((cfg->x >> LIGHTTILESHIFT) - lighttilex);
        if (ltx >= light->brightnessTable->width)
        {
            continue;
        }

        lty = abs((cfg->y >> LIGHTTILESHIFT) - lighttiley);
        if (lty >= light->brightnessTable->width)
        {
            continue;
        }

        I += LT_DISTTABLE_ACCESS(light->brightnessTable, ltx, lty) +
            cfg->ambient;
    }

    return I;
}

int32_t LT_SaveTheGame(FILE *file, int32_t checksum)
{
    int i;
    int prev, next;
    LT_Light_t *light;

    for (i = 0; i < LT_MAX_LIGHTS; i++)
    {
        light = &lt.lights[i];
        SAVE_FIELD(light->id);
        SAVE_FIELD(light->state);
        prev = (light->prev != NULL ? (light->prev - &lt.lights[0]) + 1 : 0);
        SAVE_FIELD(prev);
        next = (light->next != NULL ? (light->next - &lt.lights[0]) + 1 : 0);
        SAVE_FIELD(next);
    }
    SAVE_FIELD(lt.numLights);
    SAVE_FIELD(lt.lastLightIndex);
    SAVE_FIELD(lt.blackOutElectricLights);
    SAVE_FIELD(lt.blackOutGlobalAmbient);

    return checksum;
}

int32_t LT_LoadTheGame(FILE *file, int32_t checksum)
{
    int i;
    int prev, next;
    LT_Light_t *light;

    for (i = 0; i < LT_MAX_LIGHTS; i++)
    {
        light = &lt.lights[i];
        LOAD_FIELD(light->id);
        LOAD_FIELD(light->state);
        LOAD_FIELD(prev);
        light->prev = (prev != 0 ? &lt.lights[0] + (prev - 1) : NULL);
        LOAD_FIELD(next);
        light->next = (next != 0 ? &lt.lights[0] + (next - 1) : NULL);
    }
    LOAD_FIELD(lt.numLights);
    LOAD_FIELD(lt.lastLightIndex);
    LOAD_FIELD(lt.blackOutElectricLights);
    LOAD_FIELD(lt.blackOutGlobalAmbient);

    return checksum;
}

int LT_SpawnLightPredef(fixed x, fixed y, LT_Light_Predef_t predef)
{
    int lightId;
    LT_Light_t *light;

    lightId = LT_NewLight();
    light = LT_GetLightById(lightId);
    light->state.predefCfg = lt.predefCfgs[predef];
    light->state.cfg = light->state.predefCfg.cfg;
    light->state.cfg.x += x;
    light->state.cfg.y += y;
    light->state.predef = predef;

    return lightId;
}

LT_Spectra_t LT_GlobalAmbient(void)
{
    LT_Spectra_t amb = LT_GLOBAL_AMBIENT + lt.ambient;
    return amb;
}

void LT_LightThinkFade(LT_Light_t *light)
{
    fixed t;
    LT_Light_State_t *state;
    LT_Light_Cfg_t *cfg;
    LT_Light_PredefCfg_t *predefCfg;

    state = &light->state;
    cfg = &state->cfg;
    predefCfg = &state->predefCfg;

    state->fade.ticsElapsed += tics;
    state->fade.ticsElapsed = MIN(state->fade.ticsElapsed, predefCfg->fade.tics);

    t = fixedpt_fromint(state->fade.ticsElapsed) / predefCfg->fade.tics;
    cfg->strength = FixedLerp(predefCfg->cfg.strength, 
        predefCfg->fade.cfg.strength, t);
    cfg->ambient = FixedLerp(predefCfg->cfg.ambient, 
        predefCfg->fade.cfg.ambient, t);

    if (light->brightnessTable != NULL)
    {
        light->brightnessTable->ready = false;
    }

    if (t == FP(1))
    {
        cfg->disabled = true;
        if (predefCfg->fade.destroyLight)
        {
            LT_DeleteLight(light->id);
			return;
        }
    }
}

void LT_LightThinkJitter(LT_Light_t *light)
{
    int rnd;
    fixed t;
    fixed x, y;
    LT_Light_State_t *state;
    LT_Light_Cfg_t *cfg;
    LT_Light_PredefCfg_t *predefCfg;

    state = &light->state;
    cfg = &state->cfg;
    predefCfg = &state->predefCfg;

    if (!state->jitter.start)
    {
        state->jitter.start = true;
        state->jitter.cfg = *cfg;
        state->jitter.ticsElapsed = US_RndT() % predefCfg->jitter.tics;
    }

    state->jitter.ticsElapsed += tics;
    if (state->jitter.ticsElapsed >= predefCfg->jitter.tics)
    {
        state->jitter.ticsElapsed %= predefCfg->jitter.tics;

        #define JITTER_RANDOMLY(target, unready) \
        do { \
            int rnd; \
            fixed oldval; \
            if (predefCfg->jitter.cfg.target != FP(0)) \
            { \
                oldval = cfg->target; \
                rnd = US_RndT(); \
                if (rnd < 96) \
                { \
                    cfg->target = state->jitter.cfg.target - predefCfg->jitter.cfg.target; \
                } \
                else if (rnd < 192) \
                { \
                    cfg->target = state->jitter.cfg.target; \
                } \
                else \
                { \
                    cfg->target = state->jitter.cfg.target + predefCfg->jitter.cfg.target; \
                } \
                if (unready && cfg->target != oldval && \
                    light->brightnessTable != NULL) \
                { \
                    light->brightnessTable->ready = false; \
                } \
            } \
        } while (0)

        JITTER_RANDOMLY(strength, true);
        JITTER_RANDOMLY(x, false);
        JITTER_RANDOMLY(y, false);

        #undef JITTER_RANDOMLY
    }
}

void LT_LightThinkStrobe(LT_Light_t *light)
{
    fixed t;
    fixed x, y;
    LT_Light_State_t *state;
    LT_Light_Cfg_t *cfg;
    LT_Light_PredefCfg_t *predefCfg;

    state = &light->state;
    cfg = &state->cfg;
    predefCfg = &state->predefCfg;

    if (!state->strobe.start)
    {
        state->strobe.start = true;
        state->strobe.cfg = *cfg;
    }

    state->strobe.ticsElapsed += tics;
    t = fixedpt_fromint(state->strobe.ticsElapsed % 
        (predefCfg->strobe.tics * 2)) / predefCfg->strobe.tics;
    if (t >= FP(1))
    {
        t = FP(2) - t;
    }

#define STROBE(target, unready) \
do { \
    if (predefCfg->strobe.cfg.target != FP(0)) \
    { \
        cfg->target = FixedLerp(state->strobe.cfg.target, \
            predefCfg->strobe.cfg.target, t); \
        if (unready && light->brightnessTable != NULL) \
        { \
            light->brightnessTable->ready = false; \
        } \
    } \
} while (0)

    STROBE(strength, true);
    STROBE(x, false);
    STROBE(y, false);

#undef STROBE
}

void LT_LightThinkLifeTimed(LT_Light_t *light)
{
    LT_Light_State_t *state;
    LT_Light_Cfg_t *cfg;

    state = &light->state;
    cfg = &state->cfg;

    state->lifeTimed.ticsElapsed += tics;
    if (state->lifeTimed.ticsElapsed >= cfg->lifeTics)
    {
        cfg->disabled = true;
        LT_DeleteLight(light->id);
        return;
    }
}

void LT_LightThinkTaserFlash(LT_Light_t *light)
{
    LT_Light_State_t *state;
    LT_Light_Cfg_t *cfg;

    state = &light->state;
    cfg = &state->cfg;

    if (gamestate.weapon == wp_chaingun && 
        (gamestate.weaponframe >= 2 && gamestate.weaponframe <= 4))
    {
        cfg->x = player->x;
        cfg->y = player->y;
    }
    else
    {
        cfg->disabled = true;
        LT_DeleteLight(light->id);
    }
}

static LT_Tile_t *LT_GetTile(int x, int y)
{
    if (!TILE_IN_MAP(x, y))
    {
        return NULL;
    }
    return &lt.tiles[(y << mapshift) + x];
}

void LT_UpdateLights(void)
{
    int i;
    int w;
    int x, y;
    int tilex, tiley;
    LT_Light_t *light, *prevLight;
    LT_Light_Think_t think;
    LT_Light_State_t *state;
    LT_Light_Cfg_t *cfg;
    LT_Tile_t *tile;

    for (i = 0; i < lwlib_CountArray(lt.tiles); i++)
    {
        vec_free(lt.tiles[i].lightNums, int);
    }
    lwlib_Zero(lt.tiles);

    if (lt.numLights <= 0)
    {
        return;
    }

    assert(lt.lastLightIndex >= 0 && 
        lt.lastLightIndex < LT_MAX_LIGHTS);

    for (light = &lt.lights[lt.lastLightIndex]; 
        light != NULL; light = light->prev)
    {
        state = &light->state;
        cfg = &state->cfg;

        if (cfg->disabled)
        {
            continue; 
        }

        think = (LT_Light_Think_t)cfg->think;
        if (think != NULL)
        {
            think(light);
        }
    }

    light = &lt.lights[lt.lastLightIndex];
    while (light != NULL)
    {
        state = &light->state;

        if (state->deleteMark)
        {
            prevLight = light->prev;
            LT_DeleteLightEx((light - lt.lights) + 1, false);
            light = prevLight;
        }
        else
        {
            light = light->prev;
        }
    }

    for (light = &lt.lights[lt.lastLightIndex]; 
        light != NULL; light = light->prev)
    {
        state = &light->state;
        cfg = &state->cfg;

        LT_RefreshBrightnessTable(light);

        w = (cfg->reach + FP(1)) >> TILESHIFT;
        tilex = (cfg->x - (cfg->reach >> 1)) >> TILESHIFT;
        tiley = (cfg->y - (cfg->reach >> 1)) >> TILESHIFT;

        for (x = 0; x < w; x++)
        {
            for (y = 0; y < w; y++)
            {
                tile = LT_GetTile(x + tilex, y + tiley);
                if (tile != NULL)
                {
                    vec_push(tile->lightNums, int, light - lt.lights);
                }
            }
        }
    }
}

#if 0
// The lower 8-bit of the upper left tile of every map determine
// the used shading definition of shadeDefs.
static inline int GetShadeDefID()
{
    int shadeID = ffDataTopLeft & 0x00ff;
    return shadeID;
}
#endif

uint32_t LT_GetShade(int lighttilex, int lighttiley, uint32_t col,
    uint8_t palCol)
{
    int c;
    int max;
    int shade;
    LT_Spectra_t I;
    uint32_t palRgbaScaled;
    int r, g, b;
    byte palRgbaArr[4];
    
    if (palCol == 0xff)
    {
        return col;
    }

    I = LT_ComputeLightTileIntensity(lighttilex, lighttiley);

    shade = fixedpt_toint(I * 255);
    if (shade < 0)
    {
        shade = 0;
    }
    else if (shade > LT_SHADE_RANGE - 1)
    {
        shade = LT_SHADE_RANGE - 1;
    }

    memcpy(palRgbaArr, VBufLightTable[shade][palCol], 4);

    col = LT_ClipPack(LT_RED(col) + LT_PALRED(palRgbaArr),
        LT_GREEN(col) + LT_PALGREEN(palRgbaArr),
        LT_BLUE(col) + LT_PALBLUE(palRgbaArr));

    return col;
}

void LT_SetDistAttenuation(int distAttenuation)
{
    lt.distAttenuation = distAttenuation;
}

uint32_t LT_SpriteColor(byte col, lwlib_TPoint3f spriteLitLevel)
{
    uint32_t spriteColor;

    memcpy(&spriteColor, VBufLightTable[255][col], sizeof(uint32_t));

    spriteColor = LT_ClipPack(
        LT_RED(spriteColor) * lwlib_X(spriteLitLevel), 
        LT_GREEN(spriteColor) * lwlib_Y(spriteLitLevel), 
        LT_BLUE(spriteColor) * lwlib_Z(spriteLitLevel)
        );

    return spriteColor;
}

lwlib_TPoint3f LT_GetShadedSpriteLitLevel(fixed x, fixed y)
{
    int i;
    fixed I;
    double ltIntensity, whiteFlash, redFlash;
    int lighttilex, lighttiley;
    lwlib_TPoint3f spriteLitLevel;

    lighttilex = LIGHTTILE_COORD(x);
    lighttiley = LIGHTTILE_COORD(y);

    I = LT_ComputeLightTileIntensity(lighttilex, lighttiley);

    ltIntensity = fixedpt_todouble(I);
    ltIntensity = lwlib_MIN(ltIntensity, 1.0);

    whiteFlash = (flash_white ? (double)flash_white / 
        BONUSFLASH_PEAK : 0.0);
    redFlash = (flash_red ? (double)flash_red / 
        damagepeak : 0.0);

    spriteLitLevel = SpriteLitLevel(x, y);
    for (i = 0; i < 3; i++)
    {
        lwlib_C(spriteLitLevel, i) += ltIntensity + whiteFlash;
    }

    lwlib_X(spriteLitLevel) += redFlash;
    return spriteLitLevel;
}

bool LT_ShapeNumHasLitVersion(int shapenum)
{
    return lwlib_IntMapContains(&lt.litSpriteMap, shapenum);
}

int LT_GetLitShapeNum(int shapenum)
{
    return (int)lwlib_IntMapElem(&lt.litSpriteMap, shapenum);
}

void LT_RefreshBrightnessTable(LT_Light_t *light)
{
    fixed invD;
    int ltx, lty;
    LT_DistTable_t *invDistTable, *brightnessTable;
    LT_Light_State_t *state;
    LT_Light_Cfg_t *cfg;

    state = &light->state;
    cfg = &state->cfg;

    if (light->brightnessTable == NULL)
    {
        if (cfg->cachedBrightnessTable == LT_CACHED_BRIGHTNESS_TABLE_NONE)
        {
            light->brightnessTable = lwlib_CallocSingle(LT_DistTable_t);
        }
        else
        {
            light->brightnessTable = &lt.cachedBrightnessTables[
                cfg->cachedBrightnessTable];
        }
    }

    brightnessTable = light->brightnessTable;

    if (brightnessTable->cells == NULL || !brightnessTable->ready)
    {
        invDistTable = &lt.cachedBrightnessTables[0];

        if (brightnessTable->cells == NULL)
        {
            brightnessTable->width = 
                lwlib_NextPow2(fixedpt_toint(cfg->reach * LIGHTTILE_RES) + 
                FP_ONE);
            brightnessTable->width = lwlib_MIN(brightnessTable->width,
                LT_DISTTABLE_WIDTH);
            brightnessTable->widthShift = lwlib_Log2(brightnessTable->width);
            brightnessTable->size = lwlib_Sq(brightnessTable->width);
            brightnessTable->cells = lwlib_CallocMany(fixed,
                brightnessTable->size);
        }
        else
        {
            memset(brightnessTable->cells, 0,
                brightnessTable->size * sizeof(fixed));
        }

        for (lty = 0; lty < brightnessTable->width; lty++)
        {
            invD = LT_DISTTABLE_ACCESS(invDistTable, 0, lty);
            if (invD < cfg->invHalfReachSq)
            {
                break;
            }

            for (ltx = 0; ltx < lty + 1; ltx++)
            {
                invD = LT_DISTTABLE_ACCESS(invDistTable, ltx, lty);
                if (invD < cfg->invHalfReachSq)
                {
                    break;
                }

                LT_DISTTABLE_ACCESS(brightnessTable, ltx, lty) = 
                    LT_DISTTABLE_ACCESS(brightnessTable, lty, ltx) =
                    fixedpt_mul(cfg->strength, invD);
            }
        }

        brightnessTable->ready = true;
    }
}

#endif
