#include "version.h"

#ifdef USE_PARALLAX

#include "wl_def.h"

#ifdef USE_FEATUREFLAGS

// The lower left tile of every map determines the start texture of the parallax sky.
static int GetParallaxStartTexture()
{
    int startTex = ffDataBottomLeft;
    assert(startTex >= 0 && startTex < PMSpriteStart);
    return startTex;
}

#else

static int GetParallaxStartTexture()
{
    int startTex;
    switch(gamestate.episode * 10 + mapon)
    {
        case  0: startTex = 20; break;
        default: startTex =  0; break;
    }
    assert(startTex >= 0 && startTex < PMSpriteStart);
    return startTex;
}

#endif

void DrawParallax(VBuf_t vbuf)
{
    int startpage = GetParallaxStartTexture();
    int midangle = player->angle * (FINEANGLES / ANGLES);
    int skyheight = viewheight >> 1;
    int curtex = -1;
    byte *skytex;

    startpage += USE_PARALLAX - 1;

    for(int x = 0; x < viewwidth; x++)
    {
        int curang = pixelangle[x] + midangle;
        if(curang < 0) curang += FINEANGLES;
        else if(curang >= FINEANGLES) curang -= FINEANGLES;
        int xtex = curang * USE_PARALLAX * TEXTURESIZE / FINEANGLES;
        int newtex = xtex >> TEXTURESHIFT;
        if(newtex != curtex)
        {
            curtex = newtex;
            skytex = PM_GetTexture(startpage - curtex);
        }
        int texoffs = TEXTUREMASK - ((xtex & (TEXTURESIZE - 1)) << TEXTURESHIFT);
#ifdef HORIZON
        int yend = skyheight; // - (wallheight[x] >> 3);
#else
        int yend = skyheight - (wallheight[x] >> 3);
#endif
        if(yend <= 0) continue;

		fixed u = 0;
		fixed uinc = FP(TEXTURESIZE) / skyheight;
		int y, offs;
#ifdef HORIZON
		int botoffs = screen.pitch * (viewheight-1) + x;
#endif
        for(y = 0, offs = x; y < yend; y++, offs += screen.pitch)
        {
            VBufWritePix(vbuf, offs, 255, 
                skytex[texoffs + (u >> TILESHIFT)]);
#ifdef HORIZON
			// y-mirroring
            VBufWritePix(vbuf, botoffs, 255, 
                skytex[texoffs + (u >> TILESHIFT)]);
			botoffs -= screen.pitch;
#endif
			u += uinc;
        }
    }
}

#endif
