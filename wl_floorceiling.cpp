#include "version.h"

#ifdef USE_FLOORCEILINGTEX

#include "wl_def.h"
#include "wl_shade.h"

static inline bool IsBoundaryUV(int u, int v, bool isDefault)
{
    static const int error = 3;

    if (!isDefault)
    {
        return false;
    }

    u = u & ((TEXTURESIZE / 2) - 1);
    v = v & ((TEXTURESIZE / 2) - 1);
    return u < error || u > (TEXTURESIZE / 2) - 1 - error || 
        v < error || v > (TEXTURESIZE / 2) - 1 - error;
}

// Textured Floor and Ceiling by DarkOne
// With multi-textured floors and ceilings stored in lower and upper bytes of
// according tile in third mapplane, respectively.
void DrawFloorAndCeiling(VBuf_t vbuf, int min_wallheight)
{
    int spot;
    int ceiltex;
    fixed dist;                                // distance to row projection
    fixed tex_step;                            // global step per one screen pixel
    fixed gu, gv, du, dv;                      // global texture coordinates
    int u, v;                                  // local texture coordinates
    uint32_t *top_lmsource, *bot_lmsource;
    unsigned lasttoptex = 0xffffffff, lastbottex = 0xffffffff;
#ifdef USE_SHADING
    //LT_Shade_t shade;
#endif
    uint32_t col, shadedCol;
    uint8_t palCol;
    bool isDefault;
    int curtex;
    int curx, cury;
    int i;
    fixed hu, hv;
    int lighttilex, lighttiley;
    static const int k = (2 << (TILESHIFT - TEXTURESHIFT));
    static const int a[9][2] =
    {
        { -k, k }, { 0, k }, { k, k },
        { -k, 0 }, { 0, 0 }, { k, 0 },
        { -k, -k }, { 0, -k }, { k, -k },
    };

    int halfheight = viewheight >> 1;
    int y0 = min_wallheight >> 3;              // starting y value
    if(y0 > halfheight)
        return;                                // view obscured by walls
    if(!y0) y0 = 1;                            // don't let division by zero
    unsigned bot_offset0 = screen.pitch * (halfheight + y0);
    unsigned top_offset0 = screen.pitch * (halfheight - y0 - 1);

    // draw horizontal lines
    for(int y = y0, bot_offset = bot_offset0, top_offset = top_offset0;
        y < halfheight; y++, bot_offset += screen.pitch, top_offset -= screen.pitch)
    {
        dist = (heightnumerator / (y + 1)) << 5;
        gu =  viewx + FixedMul(dist, viewcos);
        gv = -viewy + FixedMul(dist, viewsin);
        tex_step = (dist << 8) / viewwidth / 175;
        du =  FixedMul(tex_step, viewsin);
        dv = -FixedMul(tex_step, viewcos);
        gu -= (viewwidth >> 1) * du;
        gv -= (viewwidth >> 1) * dv; // starting point (leftmost)

        int tsh = TEXTURESHIFT;
        int tmsk = TEXTURESIZE-1;
        int tmsku = tmsk << tsh;

        for(int x = 0, bot_add = bot_offset, top_add = top_offset;
            x < viewwidth; x++, bot_add++, top_add++)
        {
            if(wallheight[x] >> 3 <= y)
            {
                lighttilex = LIGHTTILE_COORD(gu);
                lighttiley = (-LIGHTTILE_COORD(gv) - 1);

                curx = (gu >> TILESHIFT) & (MAPSIZE - 1);
                cury = (gv >> TILESHIFT) & (MAPSIZE - 1);
                u = (gu >> (TILESHIFT - tsh)) & tmsk;
                v = (gv >> (TILESHIFT - tsh)) & tmsk;

                top_lmsource = GetLightmap(LMFACE_CEILING, u, v,
                    curx, cury, NULL);
                if (top_lmsource != NULL)
                {
                    col = *top_lmsource;
                    palCol = LT_ALPHA(col);
                    if (palCol != 0xff)
                    {
                        shadedCol = LT_GetShade(lighttilex, 
                            lighttiley, col, palCol);
                        VBufWriteColor(vbuf, top_add, shadedCol);
                    }
                }

                bot_lmsource = GetLightmap(LMFACE_FLOOR, u, v, 
                    curx, cury, NULL);
                if (bot_lmsource != NULL)
                {
                    col = *bot_lmsource;
                    palCol = LT_ALPHA(col);
                    if (palCol != 0xff)
                    {
                        shadedCol = LT_GetShade(lighttilex, 
                            lighttiley, col, palCol);
                        VBufWriteColor(vbuf, bot_add, shadedCol);
                    }
                }
            }
            gu += du;
            gv += dv;
        }
    }
}

#endif
